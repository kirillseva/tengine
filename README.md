**Tiny multiplatform 2d engine for android/ios/win32/nix/kolibrios/web(emscripten)**
--------------------------------------------------------------------------------

маленькая мультиплатформенная 2d поделка для создания игровых приложений под android/ios/win32/nix/kolibrios/web(emscripten)

Включает в себя редактор уровней, анимации, логических свойств и состояний (MapEditor) а также конвертер ресурсов (bmpcvtr)

c, lib, tengine, engine, game, homebrew, indie, gamedev, gui, nuklear, verlet, box2d, openAL, openSL ES, openGL, win32, NDK, IOS, freeBSD, NIX, emscripten, kolibriOS, multiplatform, level editor, converter, samples, API