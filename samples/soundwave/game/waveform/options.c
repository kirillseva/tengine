#include "options.h"

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

void wfInitOpionsByDefault(struct WFOptions* options)
{
	// if samples_per_pixel = -1 and pixels_per_second = -1 -- autosize pixels_per_second to image_width
	options->samples_per_pixel = -1; // 256
	options->pixels_per_second = -1; // 100
	options->image_width = 800;
	options->image_height = 250;
	options->waveform_color = GX_RGBA(0, 0, 31, 1);
}
//-------------------------------------------------------------------------------------------
