#include "generator.h"
#include "scalefactor.h"
#include "buffer.h"
#include "../console.h"

#define WBUF_SIZE 128

static struct WFGenerator
{
	s32 min;
	s32 max;
	s32 count;
	s32 channels;
	s32 samples_per_pixel;
	struct WFBuffer* buffer;
	struct WFScaleFactor* scale_factor;
}gsGenerator = {0};

const static s32 MAX_SAMPLE = +32767;
const static s32 MIN_SAMPLE = -32768;

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

struct WFGenerator* wfInitGenerator(struct WFBuffer* input_buffer, struct WFScaleFactor* scale_factor)
{
	gsGenerator.buffer = input_buffer;
	gsGenerator.scale_factor = scale_factor,
	gsGenerator.channels = 0;
	gsGenerator.samples_per_pixel = 0;
	wfGeneratorReset(&gsGenerator);
	return &gsGenerator;
}
//-------------------------------------------------------------------------------------------

BOOL wfGeneratorSetup(struct WFGenerator* generator, s32 sample_rate, s32 channels, struct TxtConsole *con)
{
	SDK_NULL_ASSERT(generator);
	if (channels < 1 || channels > 2)
	{
		SDK_ASSERT(0); // Can only generate waveform data from mono or stereo input files
		return FALSE;
	}
	wfBufferClear(generator->buffer);
	generator->channels = channels;
	generator->samples_per_pixel = wfScaleFactor_GetSamplesPerPixel(generator->scale_factor, sample_rate);
	if (generator->samples_per_pixel < 2)
	{
		if(con != NULL)
		{
			wchar wbuf[WBUF_SIZE];
			txbConvertAnsiToUnicode("Invalid zoom: minimum 2", wbuf, WBUF_SIZE);
			txbConsoleAdd(con, wbuf);
		}
		return FALSE;
	}
	wfBufferSetSamplesPerPixel(generator->buffer, generator->samples_per_pixel);
	wfBufferSetSampleRate(generator->buffer, sample_rate);
	if(con != NULL)
	{
		wchar wbuf[WBUF_SIZE];
		wchar wfbuf[WBUF_SIZE];
		txbConvertAnsiToUnicode("Generating waveform data...", wbuf, WBUF_SIZE);
		txbConsoleAdd(con, wbuf);
		txbConvertAnsiToUnicode("Samples per pixel : %d", wfbuf, WBUF_SIZE);
		STD_WSnprintf(wbuf, WBUF_SIZE, wfbuf, generator->samples_per_pixel);
		txbConsoleAdd(con, wbuf);
		//txbConvertAnsiToUnicode("Input channels : %d", wfbuf, WBUF_SIZE);
		//STD_WSprintf(wbuf, wfbuf, generator->channels);
		//txbConsoleAdd(con, wbuf);
	}
	return TRUE;
}
//-------------------------------------------------------------------------------------------

void wfGeneratorReset(struct WFGenerator* generator)
{
	SDK_NULL_ASSERT(generator);
	generator->min = MAX_SAMPLE;
	generator->max = MIN_SAMPLE;
	generator->count = 0;
}
//-------------------------------------------------------------------------------------------

s32 wfGeneratorGetSamplesPerPixel(struct WFGenerator* generator)
{
	SDK_NULL_ASSERT(generator);
	return generator->samples_per_pixel;
}
//-------------------------------------------------------------------------------------------

BOOL wfGeneratorProcess(struct WFGenerator* generator, const s16* input_buffer, s32 input_frame_count)
{
	SDK_NULL_ASSERT(generator);
	SDK_NULL_ASSERT(input_buffer);
	for (s32 i = 0; i < input_frame_count; ++i)
	{
		const s32 index = i * generator->channels;
		// Sum samples from each input channel to make a single (mono) waveform
		s32 sample = 0;
		for (s32 j = 0; j < generator->channels; ++j)
		{
			sample += input_buffer[index + j];
		}
		sample /= generator->channels;
		// Avoid numeric overflow when converting to short
		if (sample > MAX_SAMPLE)
		{
			sample = MAX_SAMPLE;
		}
		else if (sample < MIN_SAMPLE)
		{
			sample = MIN_SAMPLE;
		}
		if (sample < generator->min)
		{
			generator->min = sample;
		}
		if (sample > generator->max)
		{
			generator->max = sample;
		}
		if (++generator->count == generator->samples_per_pixel)
		{
			wfBufferAppendSamples(generator->buffer, (s16)generator->min, (s16)generator->max);
			wfGeneratorReset(generator);
		}
	}
	return TRUE;
}
//-------------------------------------------------------------------------------------------

void wfGeneratorProcessDone(struct WFGenerator* generator)
{
	SDK_NULL_ASSERT(generator);
	if (generator->count > 0)
	{
		wfBufferAppendSamples(generator->buffer, (s16)generator->min, (s16)generator->max);
		wfGeneratorReset(generator);
	}
}
//-------------------------------------------------------------------------------------------

void wfGeneratorDumpResult(struct WFGenerator* generator, struct TxtConsole *con)
{
	wchar wbuf[WBUF_SIZE];
	wchar wfbuf[WBUF_SIZE];
	SDK_NULL_ASSERT(generator);
	SDK_NULL_ASSERT(con);
	txbConvertAnsiToUnicode("Generated %d points", wfbuf, WBUF_SIZE);
	STD_WSnprintf(wbuf, WBUF_SIZE, wfbuf, wfBufferGetSize(generator->buffer));
	txbConsoleAdd(con, wbuf);
}
//-------------------------------------------------------------------------------------------
