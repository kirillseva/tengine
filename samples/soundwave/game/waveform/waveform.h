#ifndef WAVEFORM_H
#define WAVEFORM_H

/*
this code based on
Audio Waveform Image Generator by Chris Needham, chris.needham at bbc.co.uk.
https://github.com/bbcrd/audiowaveform
*/

#include "system.h"

#ifdef __cplusplus
extern "C" {
#endif

struct WFOptions;
struct TxtConsole;

BOOL wfInitWaveformFromFile(const char* filename, const struct WFOptions* options, struct TxtConsole *con);
BOOL wfInitWaveformFromBuffer(const u8* buffer, const struct WFOptions* options, struct TxtConsole *con);
void wfRenderWaveform(void);
void wfReleaseWaveform(void);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif