#include "filereader.h"
#include "buffer.h"
#include "generator.h"
#include "loadhelpers.h"
#include "memory.h"
#include "texts.h"
#include "../console.h"

#define WBUF_SIZE 128

const static s32 MAX_SHORT = +32767;
const static s32 MIN_CHAR = +127;

struct WFWavFileFmtStruct
{
	u8 chunkID[4];
	u32 chunkSize;
	s16 formatTag;
	u16 channels;
	u32 samplesPerSec;
	u32 avgBytesPerSec;
	u16 blockAlign;
	u16 bitsPerSample;
};

static struct WFFileReader
{
	u8* data;
	const u8* buffer_data;
	u32 data_size;
	u32 wavedata_size;
	char filename[MAX_PATH];
	struct WFWavFileFmtStruct wavinfo;
}gsFileReader = {0};

static const u8* _readWavInfo(const u8* data, struct WFWavFileFmtStruct **wh, u32 *wavedata_size);
static void _checkBPS(struct TxtConsole *err_con);

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

struct WFFileReader* wfInitFileReader(const char* input_filename, struct TxtConsole *err_con)
{
	u8* hdata;
	struct WFWavFileFmtStruct *wh;
	SDK_NULL_ASSERT(input_filename);
	SDK_ASSERT(gsFileReader.data == NULL); // call wfReleaseFileReader() before
	SDK_ASSERT(gsFileReader.data_size == 0); // call wfReleaseFileReader() before
	SDK_ASSERT(gsFileReader.buffer_data == NULL); // call wfReleaseFileReader() before
	
	gsFileReader.data_size = FileSize(input_filename);
	if (gsFileReader.data_size > 0)
	{
		STD_StrCpy(gsFileReader.filename, input_filename);
		hdata = (u8*)MALLOC(44, "wfInitFileReader:hdata");
		LoadFileToSpecificMemory(input_filename, hdata, 44);
		_readWavInfo(hdata, &wh, &gsFileReader.wavedata_size);
		gsFileReader.wavinfo = *wh;
		FREE(hdata);
		_checkBPS(err_con);
	}
	else
	{
		MI_CpuClear8(&gsFileReader.wavinfo, sizeof(struct WFWavFileFmtStruct));
		if(err_con != NULL)
		{
			wchar wfbuf[WBUF_SIZE];
			wchar wsbuf[MAX_PATH];
			wchar wbuf[WBUF_SIZE + MAX_PATH];
			txbConvertAnsiToUnicode("Error reading file : %s", wfbuf, WBUF_SIZE);
			txbConvertAnsiToUnicode(input_filename, wsbuf, MAX_PATH);
			STD_WSnprintf(wbuf, WBUF_SIZE + MAX_PATH, wfbuf, wsbuf);
			txbConsoleAdd(err_con, wbuf);
		}
	}
	return &gsFileReader;
}
//-------------------------------------------------------------------------------------------

struct WFFileReader* wfInitFileReaderFromBuffer(const u8* buffer, struct TxtConsole *err_con)
{
	struct WFWavFileFmtStruct *wh;
	SDK_NULL_ASSERT(buffer);
	SDK_ASSERT(gsFileReader.data == NULL); // call wfReleaseFileReader() before
	SDK_ASSERT(gsFileReader.data_size == 0); // call wfReleaseFileReader() before
	SDK_ASSERT(gsFileReader.buffer_data == NULL); // call wfReleaseFileReader() before

	gsFileReader.filename[0] = L'\0';
	if(buffer[0] == 'R' && buffer[1] == 'I' &&  buffer[2] == 'F' && buffer[3] == 'F')
	{
		_readWavInfo(buffer, &wh, &gsFileReader.wavedata_size);
		gsFileReader.wavinfo = *wh;
		gsFileReader.data_size = gsFileReader.wavedata_size + 44;
		gsFileReader.buffer_data = buffer;
		_checkBPS(err_con);
	}
	else
	{
		gsFileReader.data_size = 0;
		gsFileReader.buffer_data = NULL;
		MI_CpuClear8(&gsFileReader.wavinfo, sizeof(struct WFWavFileFmtStruct));
		if(err_con != NULL)
		{
			wchar wbuf[WBUF_SIZE];
			txbConvertAnsiToUnicode("Wrong buffer data", wbuf, WBUF_SIZE);
			txbConsoleAdd(err_con, wbuf);
		}
	}
	return &gsFileReader;
}
//-------------------------------------------------------------------------------------------

void _checkBPS(struct TxtConsole *err_con)
{
	if(gsFileReader.wavinfo.bitsPerSample != 8 && gsFileReader.wavinfo.bitsPerSample != 16)
	{
		if(err_con != NULL)
		{
			wchar wbuf[WBUF_SIZE];
			txbConvertAnsiToUnicode("Invalid BPS: only 8 or 16 BPS alloved", wbuf, WBUF_SIZE);
			txbConsoleAdd(err_con, wbuf);
		}
		MI_CpuClear8(&gsFileReader.wavinfo, sizeof(struct WFWavFileFmtStruct));
	}
}
//-------------------------------------------------------------------------------------------

void wfFileReaderDumpFileInfo(struct WFFileReader* file_reader, struct TxtConsole *con)
{
	wchar wfbuf[WBUF_SIZE];
	wchar wsbuf[MAX_PATH];
	wchar wbuf[WBUF_SIZE + MAX_PATH];
	SDK_NULL_ASSERT(file_reader);
	SDK_NULL_ASSERT(con);
	if(file_reader->data_size > 0)
	{
		if(file_reader->filename[0] != L'\0')
		{
			txbConvertAnsiToUnicode("File : %s", wfbuf, WBUF_SIZE);
			txbConvertAnsiToUnicode(file_reader->filename, wsbuf, MAX_PATH);
			STD_WSnprintf(wbuf, WBUF_SIZE + MAX_PATH, wfbuf, wsbuf);
			txbConsoleAdd(con, wbuf);
		}

		txbConvertAnsiToUnicode("- channels : %d", wfbuf, WBUF_SIZE);
		STD_WSnprintf(wbuf, WBUF_SIZE + MAX_PATH, wfbuf, gsFileReader.wavinfo.channels);
		txbConsoleAdd(con, wbuf);

		txbConvertAnsiToUnicode("- samples per second : %d", wfbuf, WBUF_SIZE);
		STD_WSnprintf(wbuf, WBUF_SIZE + MAX_PATH, wfbuf, gsFileReader.wavinfo.samplesPerSec);
		txbConsoleAdd(con, wbuf);

		txbConvertAnsiToUnicode("- sample rate : %d", wfbuf, WBUF_SIZE);
		STD_WSnprintf(wbuf, WBUF_SIZE + MAX_PATH, wfbuf, gsFileReader.wavinfo.bitsPerSample);
		txbConsoleAdd(con, wbuf);

		{
			const u32 length_in_seconds = gsFileReader.wavedata_size / gsFileReader.wavinfo.avgBytesPerSec; // 212.68800354
			const u32 m = length_in_seconds / 60;
			const u32 s = length_in_seconds % 60;
			txbConvertAnsiToUnicode("- duration : %02d:%02d min", wfbuf, WBUF_SIZE);
			STD_WSnprintf(wbuf, WBUF_SIZE + MAX_PATH, wfbuf, m, s);
			txbConsoleAdd(con, wbuf);
		}
	}
}
//-------------------------------------------------------------------------------------------

void wfReleaseFileReader(struct WFFileReader* file_reader)
{
	SDK_NULL_ASSERT(file_reader);
	if (file_reader->data != NULL)
	{
		FREE(file_reader->data);
	}
	file_reader->data = NULL;
	file_reader->buffer_data = NULL;
	file_reader->data_size = 0;
	file_reader->filename[0] = L'\0';
}
//-------------------------------------------------------------------------------------------

u32 wfFileReaderGetFileSize(struct WFFileReader* file_reader)
{
	SDK_NULL_ASSERT(file_reader);
	return file_reader->data_size;
}
//-------------------------------------------------------------------------------------------

u32 wfFileReaderGetWavDataDataSize(struct WFFileReader* file_reader)
{
	SDK_NULL_ASSERT(file_reader);
	return file_reader->wavedata_size;
}
//-------------------------------------------------------------------------------------------

u16 wfFileReaderGetWavBitsPerSample(struct WFFileReader* file_reader)
{
	SDK_NULL_ASSERT(file_reader);
	return file_reader->wavinfo.bitsPerSample;
}
//-------------------------------------------------------------------------------------------

u16 wfFileReaderGetWavChannels(struct WFFileReader* file_reader)
{
	SDK_NULL_ASSERT(file_reader);
	return file_reader->wavinfo.channels;
}
//-------------------------------------------------------------------------------------------

u32 wfFileReaderGetWavSamplesPerSec(struct WFFileReader* file_reader)
{
	SDK_NULL_ASSERT(file_reader);
	return file_reader->wavinfo.samplesPerSec;
}
//-------------------------------------------------------------------------------------------

BOOL wfFileReaderLoad(struct WFFileReader* file_reader)
{
	SDK_NULL_ASSERT(file_reader);
	if(file_reader->filename[0] == L'\0')
	{
		return TRUE;
	}
	SDK_ASSERT(file_reader->data == NULL); // call wfReleaseFileReader() before
	if (file_reader->data_size != 0)
	{
		file_reader->data = (u8*)MALLOC(file_reader->data_size, "wfFileReaderOpen:file_reader->data");
		LoadFileToSpecificMemory(file_reader->filename, file_reader->data, file_reader->data_size);
	}
	return file_reader->data_size != 0;
}
//-------------------------------------------------------------------------------------------

const u8* _readWavInfo(const u8* p, struct WFWavFileFmtStruct **wh, u32 *wavedata_size)
{ 
	if (!(p[0] == 'R' && p[1] == 'I' &&  p[2] == 'F' && p[3] == 'F'))
	{
		SDK_ASSERT(0); // wrong format!
		return FALSE;
	}
	*wh = (struct WFWavFileFmtStruct*)p;
	while (!((*wh)->chunkID[0] == 'f' && (*wh)->chunkID[1] == 'm' && (*wh)->chunkID[2] == 't' && (*wh)->chunkID[3] == ' '))
	{
		p++;
		*wh = (struct WFWavFileFmtStruct*)p;
	}
	p += sizeof(struct WFWavFileFmtStruct);
	while (!(p[0] == 'd' && p[1] == 'a' && p[2] == 't' && p[3] == 'a'))
	{
		p++;
	}
	p += 4;
	*wavedata_size = p[3] << 24;
	*wavedata_size |= p[2] << 16;
	*wavedata_size |= p[1] << 8;
	*wavedata_size |= p[0];
	p += 4;
	return p;
}
//-------------------------------------------------------------------------------------------

#define BUFFER_SIZE 16384
BOOL wfFileReaderRun(struct WFFileReader* file_reader, struct WFGenerator* processor, struct TxtConsole *con)
{
	const u8* p;
	struct WFWavFileFmtStruct *wh;
	u32 waveformDataSize;
	BOOL success;
	SDK_NULL_ASSERT(file_reader);
	
	p = file_reader->data != NULL ? file_reader->data : file_reader->buffer_data;
	if(p == NULL)
	{
		return FALSE;
	}
	p = _readWavInfo(p, &wh, &waveformDataSize);

	success = wfGeneratorSetup(processor, wh->samplesPerSec, wh->channels, con);
	if (success) 
	{
		const s32 frames_to_read = BUFFER_SIZE / wh->channels;
		s32 frames_read = frames_to_read;
		u32 frames_read_bytes = BUFFER_SIZE * wh->bitsPerSample / 8;
		while (success && frames_read == frames_to_read)
		{
			if (wh->bitsPerSample == 8)
			{
				s16 tbuffer[BUFFER_SIZE];
				s32 i, tbuffer_count;
				if(frames_read_bytes >= waveformDataSize)
				{
					frames_read = waveformDataSize / (wh->bitsPerSample / 8) / wh->channels;
					frames_read_bytes = waveformDataSize;
				}
				tbuffer_count = frames_read * wh->channels;
				for(i = 0; i < tbuffer_count; ++i)
				{
					tbuffer[i] = (s16)((((s32)p[i] - MIN_CHAR) * MAX_SHORT) / MIN_CHAR);
				}
				success = wfGeneratorProcess(processor, tbuffer, frames_read);
				p += frames_read_bytes;
				waveformDataSize -= frames_read_bytes;
			}
			else if(wh->bitsPerSample == 16)
			{
				if (frames_read_bytes >= waveformDataSize)
				{
					frames_read = waveformDataSize / (wh->bitsPerSample / 8) / wh->channels;
					frames_read_bytes = waveformDataSize;
				}
				success = wfGeneratorProcess(processor, (const s16*)p, frames_read);
				p += frames_read_bytes;
				waveformDataSize -= frames_read_bytes;
			}
		}
		SDK_ASSERT(waveformDataSize == 0);
		wfGeneratorProcessDone(processor);
	}
	return success;
}
//-------------------------------------------------------------------------------------------
