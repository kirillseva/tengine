#ifndef OAL_MICCAPTURE_H
#define OAL_MICCAPTURE_H

#include "system.h"
#include "texts.h"

#ifdef __cplusplus
extern "C" {
#endif

const u8* oalMicCaptureInit(u32 buffer_in_seconds);
void oalMicCaptureRelease(void);

void oalMicCaptureUpdate(void);
BOOL oalMicCaptureStart(void);
void oalMicCaptureStop(void);

BOOL oalMicCaptureDataExist(const u8 *data);
float* oalMicCreateFromDataFloatBuf(const u8 *data, u32 *onumSamples);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif