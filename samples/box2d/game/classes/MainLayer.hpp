#pragma once

#include "classes/GameLayer.hpp"
#include <Box2D/Box2D.h>

#define B2D_DEBUG_DRAW

#ifdef B2D_DEBUG_DRAW
class B2DDebugDraw;
#endif

#define MAX_DYNAMICS_BODIES 5
#define MAX_STATIC_SHAPES 2

class MainLayer : public GameLayer
{
 public:
	MainLayer(Game* game);
	virtual ~MainLayer();

	virtual void OnLayerRelease();
	virtual void OnLayerLoad();
	virtual void OnBeginUpdate(const s32& ms);
	virtual void OnDrawBackgroundTiles(const s32& ms);
	virtual void OnDrawObject(const s32& ms, const s32& id, BOOL* const opDraw);

private:

	void DeleteB2DObjects();

#ifdef B2D_DEBUG_DRAW
	B2DDebugDraw* mpDebugDraw;
#endif
	fxVec2 mSwitchCameraPos;
	BOOL mSwitchCamera;
	s32* mpCameraObj;
	b2Vec2 mObjPos0[MAX_DYNAMICS_BODIES];
	b2World *mpWorld;
	b2Body* mpStaticBody1;
	b2Body* mpBody[MAX_DYNAMICS_BODIES];
	b2Body* mpStaticShape[MAX_STATIC_SHAPES];
};