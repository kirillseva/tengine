#pragma once

#include "tengine.h"
#include "level_id.h"

class GameLayer;

class Game
{
 public:
	Game();

	void Init();
	void Release();

	void Update(const s32& ms); 

	void OnLayerRelease(const GameLayers& layer);
	void OnLayerLoad(const GameLayers& layer);
	void OnBeginUpdate(const GameLayers& layer);
	void OnDrawBackgroundTiles(const GameLayers& layer);
	void OnDrawObject(const GameLayers& layer, const u32& iId, BOOL* const opDraw);

 private:

	GameLayer* mLayer[LAYER_COUNT];
	s32 mCurrentMS;
};