#pragma once

#include "classes/GameLayer.hpp"

class HudLayer : public GameLayer
{
 public:
	HudLayer(Game* game);

	virtual void OnLayerRelease();
	virtual void OnLayerLoad();
	virtual void OnBeginUpdate(const s32& ms);
	virtual void OnDrawBackgroundTiles(const s32& ms);
	virtual void OnDrawObject(const s32& ms, const s32& id, BOOL* const opDraw);

private:

	wchar aaa[16];
	wchar bbb[16];
	s32 mCurrentFPS;
};