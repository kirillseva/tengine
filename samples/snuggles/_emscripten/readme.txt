��� �������� js/html ������ ���������� � ������� emscripten (1.27.0+)

- ���������� Emscripten-SDK https://github.com/kripken/emscripten/wiki/Emscripten-SDK
- ��������������� �������, ��������� 0_make_res.bat � ������� �����
- (windows) ���������� ���������� ���������� ����� EMSCRIPTEN_HOME (�������� EMSCRIPTEN_HOME=d:\Emscripten)
- (windows) ��������� 1_make_app.bat � ������� �����
- (linux) ��������� make � ������� �����

��������� �� ie11/chrome/safari (� safari ����� �������� develop/enable webgl)