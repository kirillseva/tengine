#ifndef LEVEL_ID_H_
#define LEVEL_ID_H_

// level files

enum
{
	LEVEL_IDX_LOADING = 0,
	LEVEL_IDX_HUD,
	LEVEL_IDX_SCENE1,
	LEVEL_IDX_SCENE2,

	LEVEL_IDX_COUNT
};

#endif
