#ifndef _GAMELEVEL_H_
#define _GAMELEVEL_H_

#include "tengine.h"
#include "gamefield.h"
#include "gui.h"

void onGUIEvent(const GUIEvent* event);

class GameFieldLevel
{
 public:

	GameFieldLevel();
	virtual ~GameFieldLevel();

	s32 getLevelFileIdx() const;

	virtual void init(u32 layerIdx);
	virtual void release();
	
	void setVisible(BOOL val);

	virtual void afterDataLoad(s32 level);
	virtual void gameStep(s32 ms);
	virtual void onDrawObject(s32 iId, BOOL* const oDraw);

	virtual void onEngineEvent(const struct EventCallbackData *pData);
	virtual void OnGUIEvent(const GUIEvent* event);

 protected:
	// mLevelFileIdx map file from mapeditor 
	s32 mLevelFileIdx;

 private:
	// assigned layer
	u32 mLayerIdx;
};

#endif //_GAMELEVEL_H_

