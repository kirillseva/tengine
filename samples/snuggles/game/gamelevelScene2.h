#ifndef _GAMELEVEL_SCENE2_H_
#define _GAMELEVEL_SCENE2_H_

#include "gamelevel.h"

class GameFieldLevelScene2 : public GameFieldLevel
{
 public:

	virtual ~GameFieldLevelScene2(){};

	virtual void init(u32 layerIdx);
	virtual void release();
};

#endif //_GAMELEVEL_SCENE2_H_
