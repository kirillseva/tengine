
#include "gamelevelLoading.h"

#include "level_id.h"

void GameFieldLevelLoading::init(u32 layerIdx)
{
	GameFieldLevel::init(layerIdx);
	mLevelFileIdx = LEVEL_IDX_LOADING;
}
//------------------------------------------------------------------------------------
