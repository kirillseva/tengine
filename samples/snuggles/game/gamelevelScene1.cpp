
#include "gamelevelScene1.h" 
#include "constants.h"
#include "level_id.h"
#include "touchpad.h"

//------------------------------------------------------------------------------------

extern TouchPadData gTPData;

//------------------------------------------------------------------------------------

void GameFieldLevelScene1::init(u32 layerIdx)
{
	GameFieldLevel::init(layerIdx);
	mLevelFileIdx = LEVEL_IDX_SCENE1;
}
//------------------------------------------------------------------------------------

void GameFieldLevelScene1::release()
{
	GameFieldLevel::release();
}
//------------------------------------------------------------------------------------

void GameFieldLevelScene1::onDrawObject(s32 iId, BOOL* const oDraw)
{
	(void)oDraw;
	switch(iId)
	{
		case M2_ORANGE:
		{
			fxVec2 pos;
			pos = prGetPosition(M2_ORANGE);
			pos.x += FX32(0.01f); //<-- fx32 operations 
			pos.y += FX32(0.02f);
			pos.x = FX_Mul(pos.x, pos.y);// == x * y;
			pos.x = FX_Div(pos.x, pos.y);// == x / y;
			// also
			pos.x = pos.y * FX32(3.0f);
			pos.y = FX32(100.0f) / FX32(5.0f);

			// getCurrentDeltaTimeScale -- timescale multiplier
			pos.x = FX_Mul(FX32(0.5f), getCurrentDeltaTimeScale());
			pos.y = FX_Mul(FX32(-0.7f), getCurrentDeltaTimeScale());
			prAddXYToPosition(M2_ORANGE, pos.x, pos.y);
		}
		break;

		case M2_FURSNUGGLE_BALL:
		{
			if(gTPData.point[0].mTouch)
			{
				if(prGetState(M2_FURSNUGGLE_BALL) == ST_TRANS_TO_IDLE)
				{
					prSetState(M2_FURSNUGGLE_BALL, ST_TRANS_TO_ACTIVE, SBM_FROM_END_ANIMATION);
				}
				else
				{
					if(prGetState(M2_FURSNUGGLE_BALL) != ST_TRANS_TO_ACTIVE)
					{
						prSetState(M2_FURSNUGGLE_BALL, ST_TRANS_TO_ACTIVE, SBM_NONE);
					}
				}
			}
			else
			{
				if(prGetState(M2_FURSNUGGLE_BALL) == ST_TRANS_TO_ACTIVE)
				{
					prSetState(M2_FURSNUGGLE_BALL, ST_TRANS_TO_IDLE, SBM_FROM_END_ANIMATION);
				}
			}
		}
		break;

		default:
			if(prGetProperty(iId, PRP_ACTIVE) > 0)
			{
				// alpha collide map, see 2_make_res.bat, 'create alpha collide map' section
				if(gTPData.point[0].mTouch && prHasAlphaCollideMap(iId) == TRUE)
				{
					struct fxVec2 pos;
					pos.x = FX32(gTPData.point[0].mX);
					pos.y = FX32(gTPData.point[0].mY);
					if(prGetAlphaCollideMapValue(iId, pos) == TRUE)
					{
						prSetProperty(iId, PRP_ACTIVE, 0);
					}
				}
			}
			else
			{
				fxVec2 pos;
				fx32 speed = prGetProperty(iId, PRP_SPEED);
				pos = prGetPosition(iId);
				speed += FX_Mul(FX32(2.7f), getCurrentDeltaTimeScale());
				pos.y -= speed;
				prSetPosition(iId, pos);
				prSetProperty(iId, PRP_SPEED, speed);
				if(pos.y < -FX32(100))
				{
					prSetProperty(iId, PRP_ENABLE, 0);
				}
			}
		break;
	}
}
//----------------------------------------------------------------------------------

void GameFieldLevelScene1::afterDataLoad(s32 level)
{
	if(level == getLevelFileIdx())
	{
		prSetState(M2_CACTUS_29, ST_ACTIVATED, SBM_NONE);
		prSetState(M2_CACTUS_30, ST_IDLE, SBM_NONE);
		prSetState(M2_CACTUS_31, ST_IDLE, SBM_NONE);
		prSetState(M2_BEDCOVER_3_61, ST_ACTIVATED, SBM_NONE);
	}
}
//----------------------------------------------------------------------------------

void GameFieldLevelScene1::onEngineEvent(const struct EventCallbackData *pData)
{
	switch(pData->eventType)
	{
		case EVENT_TYPE_ON_ANIMATION:
			if(pData->eventId == EVENT_ENDANIM)
			{
				switch(pData->ownerId)
				{
					case M2_CACTUS_29:
						if(prGetState(M2_CACTUS_29) == ST_ACTIVATED)
						{
							prSetState(M2_CACTUS_29, ST_IDLE, SBM_NONE);
							prSetState(M2_CACTUS_30, ST_ACTIVATED, SBM_NONE);
							prSetState(M2_CACTUS_31, ST_IDLE, SBM_NONE);
						}
					break;
					case M2_CACTUS_30:
						if(prGetState(M2_CACTUS_30) == ST_ACTIVATED)
						{
							prSetState(M2_CACTUS_29, ST_IDLE, SBM_NONE);
							prSetState(M2_CACTUS_30, ST_IDLE, SBM_NONE);
							prSetState(M2_CACTUS_31, ST_ACTIVATED, SBM_NONE);
						}
					break;
					case M2_CACTUS_31:
						if(prGetState(M2_CACTUS_31) == ST_ACTIVATED)
						{
							prSetState(M2_CACTUS_29, ST_ACTIVATED, SBM_NONE);
							prSetState(M2_CACTUS_30, ST_IDLE, SBM_NONE);
							prSetState(M2_CACTUS_31, ST_IDLE, SBM_NONE);
						}
					break;
					default:
					break;
				}
			}
			break;
		default:
		break;
	}
}
//----------------------------------------------------------------------------------
