
#include "texts.h"
#include "gamefield.h"
#include "gamelevelHud.h"
#include "constants.h"
#include "level_id.h"
#include "touchpad.h"

//debug
#include "lib/jobs.h"

//-------------------------------------------------------------------------------------------
// debug fps
static s32 fps_timer = 0;
static s32 ltc_timer = 0;
static s32 tct_counter = 0;
//-------------------------------------------------------------------------------------------

extern TouchPadData gTPData;

//-------------------------------------------------------------------------------------------

GameFieldLevelHud::GameFieldLevelHud()
{
	MI_CpuClear8(&mGUIContainer, sizeof(GUIContainer));
}
//-------------------------------------------------------------------------------------------

void GameFieldLevelHud::init(u32 layerIdx)
{
	GameFieldLevel::init(layerIdx);
	mLevelFileIdx = LEVEL_IDX_HUD;
}
//------------------------------------------------------------------------------------

void GameFieldLevelHud::release()
{
	// to avoid memory fragmentation, release all in opposite side 
	GUIFactory_Reset();
	GUIContainer_FreeAll(&mGUIContainer);
	GameFieldLevel::release();
}
//------------------------------------------------------------------------------------

void GameFieldLevelHud::gameStep(s32 ms)
{
	fps_timer += ms;
	ltc_timer += ms;
#ifdef SDK_DEBUG
	tct_counter = jobGetTextureCounter();
	jobResetTextureCounter();
#endif
	GUIContainer_ProcessInput(&mGUIContainer, &gTPData);
}
//------------------------------------------------------------------------------------

void GameFieldLevelHud::afterDataLoad(s32 level)
{
	s32 i;
	GUIButtonInitParameters bp;
	// see constants.h, "Properties of game object:" section for below id
	bp.baseInitParameters.enabledPrpId = PRP_ENABLE;
	// see constants.h, "Game object types" section for id
	bp.baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE] =  BUTTON_IDLE;
	bp.baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_PRESSED] =  BUTTON_IDLE;
	bp.baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_FOCUSED] = -1;
	bp.baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_TEXT] = TEXTBOX;
	// tab order
	bp.tabOrderPrpId = -1;

	GUIContainer_Init(&mGUIContainer, getLevelFileIdx(), 1); // 1 - maximum gui objects
	GUIContainer_SetGUIEventHandler(&mGUIContainer, onGUIEvent); // OnEvent() registration

	GUIFactory_Init(&mGUIContainer);
	GUIFactory_AddGUIButtonType(&bp); // register button type

	// parsing array with id's and create gui objects
	// based on registred types
	GUIFactory_ParseMapObjects();

	for(i = 0; i < GUIContainer_GetObjectsCount(&mGUIContainer); i++)
	{
		wchar txt[10];
		txt[0] = L'\0';
		GUIObject2D* o = GUIContainer_GetObject(&mGUIContainer, i);
		if(GUIObject2D_GetType(o) == GUI_TYPE_BUTTON)
		{
			GUIButton* b = (GUIButton*)(o);
			switch(GUIButton_GetMapObjId(b))
			{
				case M1_BUTTON1:
					if(level == LEVEL_IDX_SCENE1)
					{
						txt[0] = L'-';
						txt[1] = L'-';
						txt[2] = L'>';
						txt[3] = L'\0';
					}
					else
					{
						txt[0] = L'<';
						txt[1] = L'-';
						txt[2] = L'-';
						txt[3] = L'\0';
					}
					//GUIButton_SetPosition(b, getScreenWidth() / 2, BUTTON_SIZE / 2);
			}
			GUIButton_SetText(b, txt);
		}
	}
}
//------------------------------------------------------------------------------------

void GameFieldLevelHud::onDrawObject(s32 iId, BOOL* const oDraw)
{
	(void)oDraw;
	switch(iId)
	{
		case M1_TXTBOX_FPS:
		{
			wchar aaa[16];
			wchar bbb[16];
			if(ltc_timer >= 60)
			{
				bbb[0] = L'l';
				bbb[1] = L't';
				bbb[2] = L'c';
				bbb[3] = L'=';
				bbb[4] = L'%';
				bbb[5] = L'd';
				bbb[6] = L'\0';
				STD_WSnprintf(aaa, 16, bbb, tct_counter);
				txbSetDynamicTextLine(M1_TXTBOX_FPS, 1, aaa);
				ltc_timer -= 60;
			}
			if(fps_timer >= 1000)
			{
				bbb[0] = L'f';
				bbb[1] = L'p';
				bbb[2] = L's';
				bbb[3] = L'=';
				bbb[4] = L'%';
				bbb[5] = L'd';
				bbb[6] = L'\0';
				STD_WSnprintf(aaa, 16, bbb, getFPS());
				txbSetDynamicTextLine(M1_TXTBOX_FPS, 0, aaa);
				fps_timer -= 1000;

#if defined SDK_DEBUG && defined FRAME_ALLOCATOR
				{
					u32 sz = GetTotalMALLOCSizeDbg();
					bbb[0] = L'd';
					bbb[1] = L'm';
					bbb[2] = L'e';
					bbb[3] = L'm';
					bbb[4] = L'=';
					bbb[5] = L'%';
					bbb[6] = L'd';
					bbb[7] = L'\0';
					STD_WSnprintf(aaa, 16, bbb, sz);
					txbSetDynamicTextLine(M1_TXTBOX_FPS, 2, aaa);
				}
#endif
			}
		}
	}
}
//----------------------------------------------------------------------------------
