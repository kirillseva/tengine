#ifndef _GAMELEVEL_LOADING_H_
#define _GAMELEVEL_LOADING_H_

#include "gamelevel.h"

class GameFieldLevelLoading : public GameFieldLevel
{
 public:
	
	virtual ~GameFieldLevelLoading(){};

	virtual void init(u32 layerIdx);
};

#endif //_GAMELEVEL_LOADING_H_
