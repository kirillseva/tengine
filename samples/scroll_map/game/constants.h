#ifndef _GE_CONSTANTS_H_
#define _GE_CONSTANTS_H_

enum GEConstants
{

// Languages:
 LANGUAGE_ENG = 0,
 LANGUAGE_RUS = 1,

// Text strings:
 TXT_STRING1 = 0,
 TXT_STRING2 = 1,
 TXT_STRING3 = 2,

// Game object types:
 TRIGGER = 0,
 DIRECTOR = 1,
 TEXTBOX = 2,
 CAMERA = 5,
 BUTTON_IDLE = 6,
 BUTTON_FOCUS = 7,
 BUTTON_PRESSED = 8,
 OBJ_WITH_STATES = 9,

// Properties of game object:
 PRP_ENABLE = 0,
 PRP_SPEED = 1,

// Map0 fonts list
 M0_FONT_ARIAL = 0,

// Map0 objects list
 M0_TXTBOX_4 = 0,
 M0_POOL_OBJECT_8_0_POOL_FIRST = 1,
 M0_POOL_CAMERA_21 = 1,
 M0_POOL_CAMERA_22 = 2,
 M0_POOL_CAMERA_23 = 3,
 M0_POOL_CAMERA_24 = 4,
 M0_POOL_CAMERA_25 = 5,
 M0_POOL_CAMERA_26 = 6,
 M0_POOL_CAMERA_27 = 7,
 M0_POOL_CAMERA_28 = 8,
 M0_POOL_CAMERA_29 = 9,
 M0_POOL_CAMERA_30 = 10,
 M0_POOL_OBJECT_8_0_POOL_COUNT = 10,
 M0_POOL_OBJECT_8_1_POOL_FIRST = 11,
 M0_POOL_BUTTON_PRESSED_31 = 11,
 M0_POOL_BUTTON_PRESSED_32 = 12,
 M0_POOL_BUTTON_PRESSED_33 = 13,
 M0_POOL_BUTTON_PRESSED_34 = 14,
 M0_POOL_BUTTON_PRESSED_35 = 15,
 M0_POOL_OBJECT_8_1_POOL_COUNT = 5,
 M0_CAMERA_1 = 16,
 M0_CAMERA_3 = 17,
 M0_CAMERA_5 = 18,
 M0_TXTBOX_6 = 19,
 M0_OBJ_WITH_STATES = 20,

// Map0 pathnodes list
 M0_NODE_1 = 0,
 M0_NODE_2 = 1,
 M0_NODE_3 = 2,
 M0_NODE_4 = 3,
 M0_NODE_5 = 4,
 M0_NODE_6 = 5,
 M0_NODE_7 = 6,
 M0_NODE_L = 7,
 M0_NODE_R = 8,

// Map0 sound list
 M0_SND_BG_MUSIC = 0, //BGM
 M0_SND_DING = 0, //SFX

// Map1 fonts list
 M1_FONT_ARIAL = 0,

// Map1 objects list
 M1_CAMERA_1 = 0,
 M1_CAMERA_2 = 1,
 M1_CAMERA_3 = 2,
 M1_TXTBOX_FPS = 3,
 M1_BUTTON_1 = 4,
 M1_BUTTON_FOCUS_1 = 5,
 M1_TXTBOX_BT_1 = 6,
 M1_BUTTON_2 = 7,
 M1_BUTTON_FOCUS_2 = 8,
 M1_TXTBOX_BT_2 = 9,
 M1_BUTTON_3 = 10,
 M1_BUTTON_FOCUS_3 = 11,
 M1_TXTBOX_BT_3 = 12,
 M1_BUTTON_4 = 13,
 M1_BUTTON_FOCUS_4 = 14,
 M1_TXTBOX_BT_4 = 15,
 M1_CAMERA_18 = 16,

// Map2 fonts list
 M2_FONT_ARIAL = 0,

// Map2 objects list
 M2_TXTBOX_2 = 0,
 M2_BUTTON = 1,
 M2_TXTBOX_BT = 2,

// Animations: (for drawSingleFrame() function only)
 ANIM_IDLE = 0,
 ANIM_RIGHT = 1,
 ANIM_LEFT = 2,

// Events:
 EVENT_ON_OBJ_WITH_STATE_ON_NODE_R = 0,
 EVENT_ON_OBJ_WITH_STATE_ON_NODE_L = 1,

// States:
 STATE_IDLE = 0,
 STATE_RIGHT = 1,
 STATE_LEFT = 2,

// Movement :
 DIR_IDLE = 0,
 DIR_UP = 2,
 DIR_LEFT = 4,
 DIR_RIGHT = 6,
 DIR_DOWN = 8,
 DIR_COUNT = 5 

};

#endif
