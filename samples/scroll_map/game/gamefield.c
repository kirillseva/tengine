// demo:
// - tengine init (optional with USE_STATIC_MEMORY pool)
// - renderplane init
// - 2 different levels created with mapeditor uses to render on one renderplane (BGSELECT_SUB3)
//     layer 1 - GAME_LAYER based on level map l0 (mapeditor)
//     layer 2 - HUD_LAYER based on level map l1 (mapeditor)
//     (also layer 2 uses as "low memory" message layer (level map l2))
// - gui init and using
// - FRAME_ALLOCATOR principle functionality memory using
// - "low memory" signal
// - sounds and sfx
// - wchar
// - text objects: static (M0_TXTBOX_4), dynamic (M0_TXTBOX_6), TextPerPixelAccuracy on/off mode 
// - touchpad and keypad
// - colors and lines
// - obj_pool (mapeditor) demo
// - render list order manipulations (rlGetGameObjectIndex() and rlMoveGameObject() functions)
// - getCurrentDeltaTimeScale(), framerate scale value

#include "tengine.h"
#include "texts.h"
#include "touchpad.h"
#include "gamepad.h"
#include "gamefield.h"
#include "level_id.h"
#include "sound.h"
#ifdef USE_STATIC_MEMORY
#include "static_allocator.h"
#endif

#include "gui.h"

#include "constants.h"

#define DEMO_SOUND

#ifdef NITRO_SDK
#include "sound.sadl"
#endif

#ifdef USE_NO_SOUND
#ifdef DEMO_SOUND
#undef DEMO_SOUND
#endif
#endif

//-------------------------------------------------------------------------------------------

enum KeyCodes
{
    UP = 1,
    DOWN,
    RIGHT,
    LEFT,
    FIRE,
    SELECT,
    START,
    ANYKEY
};

enum GameStates
{
	gstNONE = 0,
	gstFORCEDISPLAYINIT,
	gstDISPLAYINIT,
	gstPREPARE_LOADING,
	gstLOADING,
	gstGAME
};

enum LoadingStates
{
	lstGAME = 0,
	lstLOWMEMORYMESSAGE
};

static const s32 TIME_TO_PARTICLE_EFFECT = 2500;

static s32 gameState = gstNONE;
static s32 loadingState = lstGAME;
static fx32 hero_x;
static fx32 hero_y;
static s32 key_action;
static s32 prepare_loading_draw_frame_count = 0;
static fx32 button_angle = 0;
static fx32 face_angle = 0;
static fx32 button_alpha_fx = FX32(31);
static s32 particle_effect_time = 0;
static BOOL need_rearrangeSpeedButtons = FALSE;
static struct fxVec2 camera_pos;
static fx32 direction_power[4];
static s32 pool_obj_timer = 0;
static s32 last_fps = 0;
static s32 resizeW = 0;
static s32 resizeH = 0;
static u16 top_most_renderobj_index;
static TouchPadData tp_data;
static struct SoundHandle* soundBGMHandle;
static struct GUIContainer gGUIContainer;

enum ControlDirections
{
	CD_UP = 0,
	CD_DOWN,
	CD_LEFT,
	CD_RIGHT
};

enum LoadMarkers
{
	LM_INIT = 0,
	LM_LOAD
};

enum GameLayers
{
	GAME_LAYER = 0,
	HUD_LAYER = 1,
	LAYER_COUNT
};
 
 #ifdef NITRO_SDK
 static const u32 SOUND_HEAD_SIZE = 233000;
 static const char* SOUND_BANK_NAME = "data/sound.sdat";
 #endif
 
#ifdef USE_STATIC_MEMORY
 // MAIN_STATIC_ALLOCATOR_SIZE = 50Mb 
#define MAIN_STATIC_ALLOCATOR_SIZE (1024 * 1024 * 50)
static u8 sgMainStaticHeap[MAIN_STATIC_ALLOCATOR_SIZE];
static struct StaticAllocator sgMainStaticAllocator;
#endif

//-------------------------------------------------------------------------------------------

static void onEngineEvent(const struct EventCallbackData *pData);

static void onBeginUpdate(u32 layer);
static void onDrawObject(u32 layer, s32 iId, BOOL* const oDraw);
static void onDrawBackgroundTiles(u32 layer);

static void releaseTengineData(void);
static void releaseGameResources(void);

static void load(void);

static void updateExampleWithObjPool(void);

static void doResize(void);

static void onLevelMapLoad(void);
static void onLevelHudLoad(void);
static void rearrangeSpeedButtons(void);

static void processInput(void);
static void onGUIEvent(const struct GUIEvent* event);

static void toggleBGMusic(void);

//-------------------------------------------------------------------------------------------

void tfgInitMemory()
{
	MI_CpuClear8(&gGUIContainer, sizeof(struct GUIContainer));
#ifdef USE_STATIC_MEMORY
	StaticAllocator_Init(&sgMainStaticAllocator, sgMainStaticHeap, MAIN_STATIC_ALLOCATOR_SIZE);
	InitMemoryAllocator(&sgMainStaticAllocator);
#else
	InitMemoryAllocator();
#endif
}
//-------------------------------------------------------------------------------------------

void tfgInit()
{
    InitGamePad();
	InitTouchPad();
#ifdef DEMO_SOUND
#ifndef NITRO_SDK
	sndInitSoundSystem();
#else
	sndInitSoundSystem(SOUND_HEAD_SIZE, SOUND_BANK_NAME);
#endif
#endif
	gameState = gstNONE;
}
//-------------------------------------------------------------------------------------------

void tfgRelease()
{
	releaseTengineData();
	sndReleaseSoundSystem();
}
//-------------------------------------------------------------------------------------------

void tfgSetWindowModeSize(s32 *const w, s32 *const h)
{
	*w = 800;
	*h = 600;
}
//-------------------------------------------------------------------------------------------

void tfgResize(s32 w, s32 h)
{
	gameState = gstDISPLAYINIT;
	particle_effect_time = 0;
	resizeW = w;
	resizeH = h;
}
//-------------------------------------------------------------------------------------------

static void doResize()
{
#ifdef USE_OPENGL_RENDER
	const s32 MAX_RENDER_OBJ_ON_SCENE = 128;
#endif
	const s32 MAX_RENDER_PARTICLES_ON_SCENE = 1000;

	struct RenderPlaneInitParams mp1;

	initRenderPlaneParametersWithDefaultValues(&mp1);
	mp1.mBGType = BGSELECT_SUB3;
	mp1.mSizes.mViewWidth = resizeW; 
	mp1.mSizes.mViewHeight = resizeH;
#ifndef NITRO_SDK
	
 #ifdef USE_OPENGL_RENDER
	if(loadingState == lstGAME)
	{
		// max objs estimation on scene: 128 obj and 1000 particles
		mp1.mMaxRenderObjectsOnPlane = MAX_RENDER_OBJ_ON_SCENE + MAX_RENDER_PARTICLES_ON_SCENE;
	}
	else //lstLOWMEMORYMESSAGE
	{
		mp1.mMaxRenderObjectsOnPlane = MAX_RENDER_OBJ_ON_SCENE / 2; // "low memory" level uses only text and one button (see mapeditor)
	}
 #endif

 #ifdef USE_CUSTOM_RENDER
	mp1.mColorType = BMP_TYPE_DC16;
	{
		const s32 h8 = ((resizeH + 7) / 8) * 8; // must be multiple of 8
		s32 w8 = ((resizeW + 7) / 8) * 8;
  #ifdef FRAME_ALLOCATOR
		// when the screen orientation changes, it is necessary to re-create a buffer (resize)
		// if defined FRAME_ALLOCATOR, we cannot recreate new buffer for easygraphics module because of assertion)
		// so the buffer must be the same for the larger size of the screen
		w8 = (w8 > h8) ? w8 : h8; 
		mp1.mSizes.mFrameBufferWidth8 = (u16)w8;
		mp1.mSizes.mFrameBufferHeight8 = (u16)w8;
  #else //FRAME_ALLOCATOR
		mp1.mSizes.mFrameBufferWidth8 = w8;
		mp1.mSizes.mFrameBufferHeight8 = h8;
  #endif //FRAME_ALLOCATOR
	}
#endif //USE_CUSTOM_RENDER
#else //NITRO_SDK
	mp1.mColorType = BMP_TYPE_DC16;
	mp1.mBGPriority = 0;
	mp1.mScreenBase = GX_BG_BMPSCRBASE_0x00000;
	mp1.mSizes.mFrameBufferWidth8 = (u16)resizeW;
	mp1.mSizes.mFrameBufferHeight8 = (u16)resizeH; 
#endif //NITRO_SDK
	mp1.mX = 0;
	mp1.mY = 0;

#ifdef NITRO_SDK
	//GX_SetBankForBG(GX_VRAM_BG_128_A);
    //GX_SetGraphicsMode(GX_DISPMODE_GRAPHICS, GX_BGMODE_5, GX_BG0_AS_2D);
	GX_SetBankForSubBG(GX_VRAM_SUB_BG_128_C);
    GXS_SetGraphicsMode(GX_BGMODE_5); 
    GXS_DispOn();
#endif

	if(!isRenderPlaneInit(GAME_LAYER)) // any active plane, whatever
	{
		struct InitEngineParameters params;

		releaseTengineData();

		initEngineParametersWithDefaultValues(&params);
		params.layersCount = LAYER_COUNT;
		if(loadingState == lstGAME)
		{
			params.particlesPoolSize = MAX_RENDER_PARTICLES_ON_SCENE;
		}
		else //lstLOWMEMORYMESSAGE
		{
			params.particlesPoolSize = 0;
		}

		initEngine(&params);
		initRenderPlane(&mp1);
		
		assignLayerWithRenderPlane(GAME_LAYER, BGSELECT_SUB3, TRUE);
		assignLayerWithRenderPlane(HUD_LAYER, BGSELECT_SUB3, FALSE);

		//setVisible(HUD_LAYER, FALSE);
		//setVisible(GAME_LAYER, FALSE);

        //register callbacks
		setOnEvent(onEngineEvent);
		setOnDrawGameObject(onDrawObject);
		setOnDrawBackgroundTiles(onDrawBackgroundTiles);
		setOnBeginUpdate(onBeginUpdate);

		//begin load task
		//parameter will return with iData.initiatorId in onEngineEvent() function ( EVENT_TYPE_ON_END_ASYNH_LOAD_DATA)
		beginLoadListAsynh(LM_INIT);
		{
			//load common data
			addToLoadListCommonData();
			//load text data for certain language
			// to avoid memory fragmentation load it once after common data
			addToLoadListLanguageData(LANGUAGE_ENG);
		}
		//end load task
		endLoadListAsynh(FALSE);
	
		camera_pos.x = hero_x = 0;
		camera_pos.y = hero_y = 0;
		key_action = 0;
		particle_effect_time = 0;

		//wait for EVENT_TYPE_ON_END_ASYNH_LOAD_DATA in onEngineEvent() function
		gameState = gstNONE;
	}
	else
	{
		resizeRenderPlane(GAME_LAYER, &mp1.mSizes);
		need_rearrangeSpeedButtons = TRUE;
		gameState = gstGAME;
	}
}
//-------------------------------------------------------------------------------------------

void releaseTengineData()
{
	// to avoid memory fragmentation, releaseMapData in opposite side
	releaseGameResources();
	releaseTextData();
	releaseCommonData();
	releaseRenderPlane(BGSELECT_SUB3);
	releaseEngine();
}
//-------------------------------------------------------------------------------------------

void releaseGameResources()
{
	releaseResources();

	GUIFactory_Reset();
	GUIContainer_FreeAll(&gGUIContainer);	

	releaseMapData(HUD_LAYER);
	releaseMapData(GAME_LAYER);

	ResetTouchPad();
}
//-------------------------------------------------------------------------------------------

void load()
{
#ifdef NITRO_SDK
    GX_SetVisiblePlane(GX_PLANEMASK_NONE);
#endif
	
	// to avoid memory fragmentation, releaseMapData in opposite side

	//setVisible(HUD_LAYER, FALSE);
	//setVisible(GAME_LAYER, FALSE);

	releaseGameResources();
	OS_Printf("mem on newGame = %u\n", GetFreeMemorySize());

	beginLoadListAsynh(LM_LOAD);
	{
		if(loadingState == lstGAME)
		{
			//layer LAYER_GAME, load current level
			addToLoadListMap(GAME_LAYER, 0); // i.e. l0 map file in assets/data folder
			//layer LAYER_HUD, load or reload hud
			addToLoadListMap(HUD_LAYER, 1); // i.e. l1 map file in assets/data folder
		}
		else //lstLOWMEMORYMESSAGE
		{
			//layer LAYER_HUD, load or reload memory warning message
			addToLoadListMap(HUD_LAYER, 2); // i.e. l2 map file in assets/data folder
		}
	}
	endLoadListAsynh(TRUE);
	
	//wait for  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA and EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES in onEngineEvent() function
	gameState = gstNONE;
}
//-------------------------------------------------------------------------------------------

// file index 1, layer GAME_LAYER
void onLevelMapLoad()
{
	struct fxVec2 pos;
	
	particle_effect_time = 0;
	soundBGMHandle = NULL;

	resetParticles();

	// only on onLevelMapLoad (called from onEngineEvent function)
	// you can work with M0_* objects, because current active layer is GAME_LAYER 
	pos = prGetPosition(M0_CAMERA_1); // constants.h (gererated by MapEditor3)
	hero_x = camera_pos.x = pos.x;
	hero_y = camera_pos.y = pos.y;
	direction_power[CD_UP] = 0;
	direction_power[CD_DOWN]= 0;
	direction_power[CD_LEFT]= 0;
	direction_power[CD_RIGHT]= 0;

	txbSetTextPerPixelAccuracy(M0_TXTBOX_4, FALSE, GAME_LAYER);

	// define tomost_renderobj_index in M0_POOL_OBJECT_8_1_POOL pool
	{
		const u32 zone = 0; // this level has 3 zones, but only first zone is used (see mpf file with Mapeditor)
		static const s32 tomost_poolobj_index = M0_POOL_OBJECT_8_1_POOL_FIRST + M0_POOL_OBJECT_8_1_POOL_COUNT - 1; // pool object with topmost Z
		SDK_ASSERT(rlIsAvailable());
		rlGetGameObjectIndex(tomost_poolobj_index, GAME_LAYER, zone, NULL, &top_most_renderobj_index);
	}

	setCamera(camera_pos);
}
//-------------------------------------------------------------------------------------------

void onLevelHudLoad()
{
	// only on onLevelHudLoad (called from onEngineEvent function)
	// you can work with M1_* objects, because current active layer is HUD_LAYER

	s32 i;
	struct GUISpeedButtonInitParameters bp;
	bp.baseInitParameters.enabledPrpId = PRP_ENABLE;
	// id's from constants.h, "Game object types" section
	bp.baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE] = BUTTON_IDLE;
	bp.baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_PRESSED] = BUTTON_PRESSED;
	bp.baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_FOCUSED] = -1;
	bp.baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_TEXT] = TEXTBOX;

	GUIContainer_Init(&gGUIContainer, HUD_LAYER, 4); // 4 - maximum gui objects
	GUIContainer_SetGUIEventHandler(&gGUIContainer, onGUIEvent); // OnGuiEvent() registration

	GUIFactory_Init(&gGUIContainer);
	GUIFactory_AddGUISpeedButtonType(&bp); // register button type

	// parsing array with id's and create gui objects
	// based on registred types
	GUIFactory_ParseMapObjects();

	//create gui from HUD_LAYER data 
	if(loadingState == lstGAME)
	{
		for(i = 0; i < GUIContainer_GetObjectsCount(&gGUIContainer); i++)
		{
			struct GUIObject2D *o; 
			wchar txt[10];
			txt[0] = L'\0';
			o = GUIContainer_GetObject(&gGUIContainer, i);
			if(GUIObject2D_GetType(o) == GUI_TYPE_SPEEDBUTTON)
			{
				struct GUISpeedButton* b = (struct GUISpeedButton*)o;
				switch(GUISpeedButton_GetMapObjId(b))
				{
					case M1_BUTTON_1:
						txt[0] = L'U';
						txt[1] = L'p';
						txt[3] = L'\0';
						GUISpeedButton_SetHotkey(b, TYPE_UP);
                    break;
					case M1_BUTTON_2:
						txt[0] = L'D';
						txt[1] = L'o';
						txt[2] = L'w';
						txt[3] = L'n';
						txt[4] = L'\0';
						GUISpeedButton_SetHotkey(b, TYPE_DOWN);
					break;
					case M1_BUTTON_3:
						txt[0] = L'L';
						txt[1] = L'e';
						txt[2] = L'f';
						txt[3] = L't';
						txt[4] = L'\0';
						GUISpeedButton_SetHotkey(b, TYPE_LEFT);
					break;
					case M1_BUTTON_4:
						txt[0] = L'R';
						txt[1] = L'i';
						txt[2] = L'g';
						txt[3] = L'h';
						txt[4] = L't';
						txt[5] = L'\0';
						GUISpeedButton_SetHotkey(b, TYPE_RIGHT);
				}
				GUISpeedButton_SetText(b, txt);
			}
		}

		rearrangeSpeedButtons();

		prSetCustomRotation(M1_BUTTON_3, FX32(45));
	}
	else //lstLOWMEMORYMESSAGE
	{
		// nope
	}

#ifdef NITRO_SDK
    GXS_SetVisiblePlane(GX_PLANEMASK_BG3);
#endif

	last_fps = 0;

    gameState = gstGAME;
}
//------------------------------------------------------------------------------------

void rearrangeSpeedButtons()
{
	s32 i;
	for(i = 0; i < GUIContainer_GetObjectsCount(&gGUIContainer); i++)
	{
		const fx32 BUTTON_SIZE = FX32(64);
		struct GUIObject2D *o; 
		o = GUIContainer_GetObject(&gGUIContainer, i);
		if(GUIObject2D_GetType(o) == GUI_TYPE_SPEEDBUTTON)
		{
			struct GUISpeedButton* b = (struct GUISpeedButton*)o;
			switch(GUISpeedButton_GetMapObjId(b))
			{
				case M1_BUTTON_1:
					GUISpeedButton_SetPosition(b, FX32(getViewWidth(HUD_LAYER)) / 2, BUTTON_SIZE / 2);
                break;
				case M1_BUTTON_2:
					GUISpeedButton_SetPosition(b, FX32(getViewWidth(HUD_LAYER)) / 2, FX32(getViewHeight(HUD_LAYER)) - BUTTON_SIZE / 2);
				break;
				case M1_BUTTON_3:
					GUISpeedButton_SetPosition(b, BUTTON_SIZE / 2, FX32(getViewHeight(HUD_LAYER)) / 2);
				break;
				case M1_BUTTON_4:
					GUISpeedButton_SetPosition(b, FX32(getViewWidth(HUD_LAYER)) - BUTTON_SIZE / 2, FX32(getViewHeight(HUD_LAYER)) / 2);
				break;
				default:
				break;
			}
		}
	}
}
//-------------------------------------------------------------------------------------------

void tfgLostRenderDevice()
{
}
//-------------------------------------------------------------------------------------------

void tfgRestoreRenderDevice()
{
}
//------------------------------------------------------------------------------------

void tfgLowMemory()
{	
	// now load "level" with warning message
	// note: this level must be as small as possible, without BG elements and large textures
	loadingState = lstLOWMEMORYMESSAGE;

	// todo: save current gamestate

	// delete all data and tengine instance
	releaseTengineData();

	// reinit engine with minimal memory consumption
	gameState = gstDISPLAYINIT;
}
//-------------------------------------------------------------------------------------------

void tfgTick(s32 ms)
{
	switch(gameState)
	{
		case gstNONE:
		break;

		case gstFORCEDISPLAYINIT:
			releaseTengineData();
		case gstDISPLAYINIT:
			doResize();
		break;

		case gstPREPARE_LOADING:
			// To avoid flickering then loading: 
			//   call drawWithoutInternalLogicUpdateMode() function
			//   and skip one or two ticks
			drawWithoutInternalLogicUpdateMode();
			if(++prepare_loading_draw_frame_count > 2)
			{
				gameState = gstLOADING;
			}
		break;

		case gstLOADING:
			prepare_loading_draw_frame_count = 0;
			load();
		break;

		case gstGAME:
			pool_obj_timer += ms;
			particle_effect_time -= ms;
	}
}
//-------------------------------------------------------------------------------------------

void onGUIEvent(const struct GUIEvent* event)
{
	if(GUIObject2D_GetType(event->mpSender) == GUI_TYPE_SPEEDBUTTON)
	{
		if(event->mState == ST_PRESSED)
		{
			const struct GUISpeedButton* b = (struct GUISpeedButton*)event->mpSender;
			const fx32 face_ds = FX_Mul(FX32(0.5f), getCurrentDeltaTimeScale());
			const fx32 power_ds = FX_Mul(FX32(2.5f), getCurrentDeltaTimeScale());
			
			// in case of low memory this is message button
			if(lstLOWMEMORYMESSAGE == loadingState)
			{
				loadingState = lstGAME;
				gameState = gstFORCEDISPLAYINIT;
				return;
			}
			
			switch(GUISpeedButton_GetHotkey(b))
			{
				case TYPE_LEFT:
					key_action = TYPE_LEFT;
					direction_power[CD_LEFT] += power_ds;
					prSetCustomRotation(M1_CAMERA_18, face_angle);
					face_angle += face_ds;
				break;
				case TYPE_RIGHT:
					key_action = TYPE_RIGHT;
					direction_power[CD_RIGHT] += power_ds;

					prSetCustomRotation(M1_BUTTON_1, button_angle);
					face_angle = prGetCustomRotation(M1_CAMERA_18);
					button_angle += face_ds;
				break;
				case TYPE_UP:
					key_action = TYPE_UP;
					direction_power[CD_UP] += power_ds;
				break;
				case TYPE_DOWN:
					key_action = TYPE_DOWN;
					direction_power[CD_DOWN] += power_ds;
                break;
				default:
                break;
			}
		}
	}
}
//----------------------------------------------------------------------------------

// work with OBJPOOL objects
// to use it place object with group type OBJPOOL on the map, go to property and set objpools 
void updateExampleWithObjPool()
{
	s32 i;
	struct fxVec2 startPosition;

	const s32 POOL_OBJECT_EMIT_TIME = 1000;
	const s32 POOL_OBJECT_SPIN_RADIUS = 100;
	const s32 POOL_OBJECT_SPIN_COUNT = 3;
	const fx32 FX_360 = FX32(360);
	const fx32 OBJ_LINEAR_SPEED = FX32(10);
	const fx32 OBJ_RADIAL_SPEED = FX32(2);

	const fx32 currentDeltaTimeScale = getCurrentDeltaTimeScale();

	startPosition.x = 0;
	startPosition.y = FX32(getMapHeight() / 3);
	for(i = M0_POOL_OBJECT_8_1_POOL_FIRST; i < M0_POOL_OBJECT_8_1_POOL_FIRST + M0_POOL_OBJECT_8_1_POOL_COUNT; i++)
	{
		if(pool_obj_timer >= POOL_OBJECT_EMIT_TIME)
		{
			if(prGetProperty(i, PRP_ENABLE) == 0)
			{
				prSetProperty(i, PRP_ENABLE, 1);
				prSetProperty(i, PRP_SPEED, 0);
				prSetPosition(i, startPosition);
				pool_obj_timer = 0;
			}
		}
		if(prGetProperty(i, PRP_ENABLE) != 0)
		{
			struct fxVec2 pos = prGetPosition(i);
			if(pos.x < FX32(getMapWidth()))
			{
				if (prGetProperty(i, PRP_SPEED) <= 0)
				{
					pos.x += FX_Mul(OBJ_LINEAR_SPEED, currentDeltaTimeScale);
					prSetProperty(i, PRP_ENABLE, pos.x); // save position for spin phase
					if (prGetProperty(i, PRP_SPEED) == 0 && pos.x >= FX32(getMapWidth() / 3))
					{
						prSetProperty(i, PRP_SPEED, FX32(270)); // spin phase marker, rotation angle
					}
				}
				else
				{
					fx32 savedPosX, angle, cosA;
					savedPosX = prGetProperty(i, PRP_ENABLE);
					angle = prGetProperty(i, PRP_SPEED);
					cosA = fxCos((FX_Mul(angle, FX_PI)) / 180);
					pos.x = savedPosX + POOL_OBJECT_SPIN_RADIUS * cosA; // 200(int) * cosA(fx32) = fx32  
					angle += FX_Mul(OBJ_RADIAL_SPEED, currentDeltaTimeScale);
					if (prGetProperty(i, PRP_SPEED) / FX_360 != angle / FX_360)
					{
						u16 renderlist_index; // renderlist index
						u16 renderobj_index; // render object index in renderlist
						const u32 zone = 0; // this level has 3 zones, but only first zone is used (see mpf file with Mapeditor)
						SDK_ASSERT(rlIsAvailable());
						// get renderlist_index and renderobj_index values
						rlGetGameObjectIndex(i, GAME_LAYER, zone, &renderlist_index, &renderobj_index);
						// move object in renderlist
						rlMoveGameObject(GAME_LAYER, zone, renderlist_index, renderobj_index, top_most_renderobj_index);
					}
					//scale effect
					{
						struct fxVec2 scale;
						const fx32 sinA = fxSin((FX_Mul(angle, FX_PI)) / 180);
						scale.x = FX32_ONE + sinA / 10;
						scale.y = scale.x;
						prSetCustomScale(i, scale);
					}
					if (angle >= FX_360 * POOL_OBJECT_SPIN_COUNT)
					{
						angle = -1; // break spin phase
					}
					prSetProperty(i, PRP_SPEED, angle);
				}
				prSetPosition(i, pos);
			}
			else
			{
				prSetProperty(i, PRP_ENABLE, 0);
			}
		}
	}
}
//----------------------------------------------------------------------------------

void toggleBGMusic()
{
#ifdef DEMO_SOUND
	if(sndIsBGMPlaying() == TRUE)
	{
		sndStop(soundBGMHandle);
		soundBGMHandle = NULL;
	}
	else
	{
		struct SoundData sd;
		sd.Id = M0_SND_BG_MUSIC;
		sd.Looped = TRUE;
		sd.Type = SOUND_TYPE_BGM2;
		sd.Volume = FX32(0.5f);
		soundBGMHandle = sndPlay(&sd);
		// set volume with handle
		if(soundBGMHandle != NULL)
		{
			sndSetVolume(soundBGMHandle, FX32(0.51f));
		}
	}
#endif
}
//----------------------------------------------------------------------------------

void onBeginUpdate(u32 layer)
{
	switch(layer)
	{
		case GAME_LAYER:

			setCamera(camera_pos);

			updateExampleWithObjPool();

			break;

		case HUD_LAYER:
			if(need_rearrangeSpeedButtons)
			{
				need_rearrangeSpeedButtons = FALSE;
				rearrangeSpeedButtons();
			}

			processInput();

		default:
			break;
	}
}
//----------------------------------------------------------------------------------

void onDrawBackgroundTiles(u32 layer)
{
	switch(layer)
	{
		case GAME_LAYER:
			// draw primitives example
			setColor(GX_RGBA(31, 0, 0, 1));
			drawLine(FX32(100), FX32(100), FX32(100), FX32(300));
			setColor(GX_RGBA(0, 0, 31, 1));
			drawLine(FX32(110), FX32(100), FX32(110), FX32(300));

		default:
			break;
	}
}
//----------------------------------------------------------------------------------

void processInput()
{
	const fx32 MAX_SPEED = FX32(60);		
	
	key_action = 0;

	UpdateGamePad();
	
	//nintendo lotcheck
	if(IsButtonPress(TYPE_L) && IsButtonPress(TYPE_R) && IsButtonPress(TYPE_SELECT) && IsButtonPress(TYPE_START))
	{
		OS_ResetSystem(0);
		return;
	}

	ReadTouchPadData(&tp_data);

	GUIContainer_ProcessInput(&gGUIContainer, &tp_data);

	if(IsButtonDown(TYPE_START))
	{
		key_action = START;
		gameState = gstPREPARE_LOADING;
	}

	if(IsButtonDown(TYPE_A))
	{
		key_action = 0;
		toggleBGMusic();
	}

	if(direction_power[CD_UP] > MAX_SPEED)
	{
		direction_power[CD_UP] = MAX_SPEED;
	}
	if(direction_power[CD_DOWN] > MAX_SPEED)
	{
		direction_power[CD_DOWN] = MAX_SPEED;
	}
	if(direction_power[CD_LEFT] > MAX_SPEED)
	{
		direction_power[CD_LEFT] = MAX_SPEED;
	}
	if(direction_power[CD_RIGHT] > MAX_SPEED)
	{
		direction_power[CD_RIGHT] = MAX_SPEED;
	}

	{
		const fx32 ds = FX_Mul(FX32(0.5f), getCurrentDeltaTimeScale());
		direction_power[CD_UP] -= ds;
		direction_power[CD_DOWN] -= ds;
		direction_power[CD_LEFT] -= ds;
		direction_power[CD_RIGHT] -= ds;
	}

	if(direction_power[CD_UP] < 0)
	{
		direction_power[CD_UP] = 0;
	}
	if(direction_power[CD_DOWN] < 0)
	{
		direction_power[CD_DOWN] = 0;
	}
	if(direction_power[CD_LEFT] < 0)
	{
		direction_power[CD_LEFT] = 0;
	}
	if(direction_power[CD_RIGHT] < 0)
	{
		direction_power[CD_RIGHT] = 0;
	}
}
//----------------------------------------------------------------------------------

void onDrawObject(u32 layer, s32 iId, BOOL* const oDraw)
{
	(void)oDraw;
	switch(layer)
	{
		case GAME_LAYER:
			switch(iId)
			{
				case M0_CAMERA_1: // constants.h (gererated by MapEditor3)
				{
					struct fxVec2 pos;
					fx32 forceX, forceY, xxx, ival;

					pos = prGetPosition(M0_CAMERA_1);
					hero_x = pos.x;
					hero_y = pos.y;

					if(particle_effect_time < 0)
					{
						particle_effect_time = TIME_TO_PARTICLE_EFFECT;
						createParticles(M0_CAMERA_1, 3, 3, FX32(0.5), FX32(0.2), 3000, 1500, 300, 0, 0);
						createSimpleParticles(5, GX_RGBA(31,0,0,1), hero_x, hero_y, 10, 200, FX32(1.1f), FX32(0), 3000, 1500, 300, NULL, NULL);
						createExplode(hero_x, hero_y, 20, 10, 1000, M0_CAMERA_1, STATE_RIGHT);
#ifdef DEMO_SOUND
						{
							struct SoundHandle *sh;
							struct SoundData sd;
							sd.Id = M0_SND_DING;
							sd.Looped = FALSE;
							sd.Type = SOUND_TYPE_SFX;
							sd.Volume = FX32(0.0f);
							sh = sndPlay(&sd);
							if(sh)
							{
								sndSetVolume(sh, FX32(0.8f));
							}
						}
#endif
					}

					forceX = FX_Div((direction_power[CD_RIGHT] - direction_power[CD_LEFT]), FX32(5));
					forceY = FX_Div((direction_power[CD_DOWN] - direction_power[CD_UP]), FX32(5));
					camera_pos.y += forceY;
					camera_pos.x += forceX;
					xxx = getViewHeight(HUD_LAYER) / 2;
					ival = fx2int(camera_pos.y);
					if(ival < xxx)
					{
						camera_pos.y = FX32(xxx);
						direction_power[CD_UP] = 0;
						direction_power[CD_DOWN]= 0;
						direction_power[CD_LEFT]= 0;
						direction_power[CD_RIGHT]= 0;
					}
					xxx = getViewWidth(HUD_LAYER) / 2;
					ival = fx2int(camera_pos.x);
					if(ival < xxx)
					{
						camera_pos.x = FX32(xxx);
						direction_power[CD_UP] = 0;
						direction_power[CD_DOWN]= 0;
						direction_power[CD_LEFT]= 0;
						direction_power[CD_RIGHT]= 0;
					}
					xxx = getMapHeight() - getViewHeight(HUD_LAYER) / 2;
					ival = fx2int(camera_pos.y);
					if(ival > xxx)
					{
						camera_pos.y = FX32(xxx);
						direction_power[CD_UP] = 0;
						direction_power[CD_DOWN]= 0;
						direction_power[CD_LEFT]= 0;
						direction_power[CD_RIGHT]= 0;
					}
					xxx = getMapWidth() - getViewWidth(HUD_LAYER) / 2;
					ival = fx2int(camera_pos.x);
					if(ival > xxx)
					{
						camera_pos.x = FX32(xxx);
						direction_power[CD_UP] = 0;
						direction_power[CD_DOWN]= 0;
						direction_power[CD_LEFT]= 0;
						direction_power[CD_RIGHT]= 0;
					}
				}
				break;	

				case M0_TXTBOX_6:
				{
					wchar aaa[16];
					wchar bbb[16];
					bbb[0] = L'x';
					bbb[1] = L'=';
					bbb[2] = L'%';
					bbb[3] = L'd';
					bbb[4] = L'\0';
					STD_WSnprintf(aaa, 16, bbb, tp_data.point[0].mX);
					txbSetDynamicTextLine(M0_TXTBOX_6, 0, aaa);
					bbb[0] = L'y';
					STD_WSnprintf(aaa, 16, bbb, tp_data.point[0].mY);
					txbSetDynamicTextLine(M0_TXTBOX_6, 1, aaa);
					txbSetDynamicTextLine(M0_TXTBOX_6, 2, getStaticText(TXT_STRING1, 0));
					txbSetDynamicTextLine(M0_TXTBOX_6, 3, getStaticText(TXT_STRING2, 0));
				}

				default:
					break;
			}
		break;
		
		case HUD_LAYER:
		{
			switch(iId)
			{
				case M1_BUTTON_2:
					{
						struct fxVec2 scale;
						prSetCustomAlpha(iId, (u8)(fx2int(button_alpha_fx)));
						button_alpha_fx -= FX_Mul(FX32(1), getCurrentDeltaTimeScale());
						if(button_alpha_fx < FX32(15))
						{
							button_alpha_fx = FX32(31);
						}
						scale.x = FX32_ONE + FX_Div(FX32(31) - button_alpha_fx, FX32(31));
						scale.y = scale.x;
						prSetCustomScale(iId, scale);
					}
				break;

				case M1_TXTBOX_FPS: // (hack) or text field on low memory warning button, same ID
					if(last_fps != getFPS())
					{
						wchar aaa[16];
						wchar bbb[16];
						bbb[0] = L'f';
						bbb[1] = L'p';
						bbb[2] = L's';
						bbb[3] = L'=';
						bbb[4] = L'%';
						bbb[5] = L'd';
						bbb[6] = L'\0';
						last_fps = getFPS();
						STD_WSnprintf(aaa, 16, bbb, last_fps);
						txbSetDynamicTextLine(M1_TXTBOX_FPS, 0, aaa);
					}
				
				default:
					break;
			}
		}

		default:
			break;
	}
}
//----------------------------------------------------------------------------------

//CALLBACK FUNCTIONS

//----------------------------------------------------------------------------------

static void onEngineEvent(const struct EventCallbackData *pData)
{	
	switch(pData->eventType)
	{
		case EVENT_TYPE_ON_SOUND:
		{
			/*
			const struct SoundHandle *sh = pData->eventData.mpSoundHandle;
			switch(pData->eventId)
			{
				case SOUND_TYPE_PLAY:
					OS_Printf("sound play, type=%d, id=%d\n", sndGetSoundType(sh), sndGetSoundId(sh));
				break;
				case SOUND_TYPE_PAUSE:
					OS_Printf("sound pause, type=%d, id=%d\n", sndGetSoundType(sh), sndGetSoundId(sh));
				break;
				case SOUND_TYPE_STOP:
					OS_Printf("sound stop, type=%d, id=%d\n", sndGetSoundType(sh), sndGetSoundId(sh));
				break;
			}
			*/
		}
		break;

		case EVENT_TYPE_ON_ANIMATION:
		case EVENT_TYPE_ON_DIRECTOR:
        break;
    
		case  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA:
			switch(pData->eventId)
			{
				case LOAD_TYPE_ENDLOADDATATASK:
					if(pData->initiatorId == LM_INIT)
					{
						gameState = gstPREPARE_LOADING;
					}
				break;
				case LOAD_TYPE_MAPDATA:
					if(pData->initiatorId == LM_LOAD)
					{
						switch(pData->layer)
						{
							case HUD_LAYER:
								onLevelHudLoad();
							break;
							case GAME_LAYER:
								onLevelMapLoad();
						}
					}
				default:
					break;
			}
		break;
		
		case EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES:
			//setVisible(HUD_LAYER, TRUE);
			//setVisible(GAME_LAYER, TRUE);
			gameState = gstGAME;
			toggleBGMusic();

		default:
			break;
	}
}
//----------------------------------------------------------------------------------
