@echo on

set CURRENT_DIR=%~dp0

if EXIST %CURRENT_DIR%\win32app\Debug\OpenAL32.dll (
goto END
)

copy ..\..\..\lib\win32\openAL\OpenAL32.dll %CURRENT_DIR%\win32app\Debug\
copy ..\..\..\lib\win32\openAL\wrap_oal.dll %CURRENT_DIR%\win32app\Debug\

:END