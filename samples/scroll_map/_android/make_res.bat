@echo off

echo ------------------------------
echo Generate map (MapEditor) files
echo Please verify correct pathes in file ..\mapeditor\editor.ini
echo By default editor.ini contain paths from C disk
echo ------------------------------

set CURRENT_DIR=%~dp0
if EXIST %CURRENT_DIR%\app\src\main\assets\data\o del %CURRENT_DIR%\app\src\main\assets\data\o

rem wchar size for android 4
..\..\..\tools\editor\MapEditor3.exe -wchar_size4 -p%CURRENT_DIR%\..\mapeditor\map_cpx_obj24d3.mpf -i%CURRENT_DIR%\..\mapeditor\ -o%CURRENT_DIR%\app\src\main\assets\data\ -l%CURRENT_DIR%\log.txt 

echo MapEditor report %CURRENT_DIR%\log.txt
type %CURRENT_DIR%\log.txt

if NOT EXIST %CURRENT_DIR%\app\src\main\assets\data\o (
echo .
echo ------------------------------
echo Error: generated files not found
echo Please check pathes in file ..\mapeditor\editor.ini
echo and generate map-files with MapEditor:
echo - open ..\mapeditor\*.mpf file with ..\..\..\tools\editor\MapEditor3.exe
echo - go to main menu [File/Save map] and browse to folder assets\data
echo ------------------------------
pause
goto END
)

echo .
echo ------------------------------
echo convert graphics res 
echo ------------------------------

@echo on
for %%i in (..\\rawres\*.bmp) do ..\..\..\tools\bmpcvtr.exe %%i -8888 -2n -fnearest -oapp/src/main/assets/data
@echo off

copy ..\rawres\arial.fnt app\src\main\assets\data\
copy ..\rawres\*.wav app\src\main\assets\data\

copy ..\rawres\color.vsh app\src\main\assets\data\
copy ..\rawres\lit.vsh app\src\main\assets\data\
copy ..\rawres\unlit.vsh app\src\main\assets\data\
copy ..\rawres\color.fsh app\src\main\assets\data\
copy ..\rawres\lit.fsh app\src\main\assets\data\
copy ..\rawres\unlit.fsh app\src\main\assets\data\

if NOT EXIST %CURRENT_DIR%\app\src\main\assets\data\color.vsh (
echo .
echo ------------------------------
echo Please copy *.fsh and *.vsh files from tengine\src\_default_shaders to ..\rawres
echo ------------------------------
pause
goto END
)

:END