#pragma once

#include "tengine.h"
#include <Box2D/Box2D.h>

#define COLOR_TO_5BCOLOR(c) ((u8)((c) * 31.0f))

const b2Vec2& b2GetGameObjectPosition(const s32& id);

void b2SetGameObjectPosition(const s32& id, const b2Vec2& b2pos);

void b2SetGameObjectRotation(const s32& id, const float& angle_rad);

void b2SetCameraPosition(const b2Vec2& b2pos);

const fxVec2& b2MetersToPixels(const b2Vec2& b2pos);

const b2Vec2& b2PixelsToMeters(const fxVec2& fxpos);

float32 b2PixelsToMeters(const float32& val);

float32 b2MetersToPixels(const float32& val);
