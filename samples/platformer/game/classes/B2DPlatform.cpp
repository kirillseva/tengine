#include "classes/B2DPlatform.hpp"
#include "classes/B2DHelper.hpp"
#include "level_id.h"

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

void B2BPlatformManager::Init(b2World* world)
{
	s32 i;

	mpWorld = world;

	const s32 staticShapeCount = cldShapesCount(GAME_LAYER);
	for (i = 0; i < staticShapeCount; i++)
	{
		B2BPlatform* p = new B2BPlatform();

		const struct fxVec2* arr = cldShapeGetNodesArray(GAME_LAYER, i);
		const s32 chainShapesNodesCount = cldShapeNodesCount(GAME_LAYER, i);
		b2Vec2 *b2arr = new b2Vec2[chainShapesNodesCount];
		for (s32 j = 0; j < chainShapesNodesCount; j++)
		{
			b2arr[j] = b2PixelsToMeters(arr[j]);
		}

		b2ChainShape chainShape;
		chainShape.CreateChain(b2arr, chainShapesNodesCount);
		delete[] b2arr;

		b2FixtureDef fixtureDef;
		fixtureDef.shape = &chainShape;
		fixtureDef.density = 1.0f;
		fixtureDef.friction = 0.8f;

		b2BodyDef staticShapeBodyDef;
		staticShapeBodyDef.type = b2_staticBody;

		p->mpBody = mpWorld->CreateBody(&staticShapeBodyDef);
		p->mpFixture = p->mpBody->CreateFixture(&fixtureDef);

		if (i == 0)
		{
			p->SetType(B2BObject::Type::OT_Undef);
		}

		p->mpFixture->SetUserData(p); // mark fixture as platform

		chainShape.Clear();

		mMap[i] = p;
	}
}

void B2BPlatformManager::OnDraw(const s32& id) const
{
	/*const B2BCharacter& c = *mMap.at(id);
	const b2Vec2& position = c.mpBodyB->GetPosition();
	const float32& angle_rad = c.mpBodyB->GetAngle();
	b2SetGameObjectPosition(id, position + c.mTexureOffset);
	b2SetGameObjectRotation(id, angle_rad);*/
}
