#pragma once

#include <Box2D/Box2D.h>

class B2BWorld: public b2ContactListener
{
public:

	B2BWorld();

	void Init(b2World* world);
	void Release();

private:

	virtual void BeginContact(b2Contact* contact)  override;
	virtual void EndContact(b2Contact* contact)  override;
	virtual void PreSolve(b2Contact* contact, const b2Manifold* oldManifold) override;

	b2World* mpWorld;
};