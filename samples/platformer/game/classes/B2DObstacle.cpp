#include "classes/B2DObstacle.hpp"
#include "classes/B2DHelper.hpp"
#include "level_id.h"

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

void B2BObstacleManager::Init(const s32* arrId, const s32& maxObjects, b2World* world)
{
	s32 i;

	mpWorld = world;

	for (i = 0; i < maxObjects; i++)
	{
		mMap[arrId[i]] = new B2BObstacle();
		Create(arrId[i]);
	}
}

void B2BObstacleManager::OnDraw(const s32& id) const
{
	const B2BObstacle& o = Get(id);
	const b2Vec2& position = o.mpBodyB->GetPosition();
	const float32& angle_rad = o.mpBodyB->GetAngle();
	b2SetGameObjectPosition(id, position + o.mTexureOffset);
	b2SetGameObjectRotation(id, angle_rad);
}

void B2BObstacleManager::Create(const s32& id)
{
	B2BObstacle* c = mMap.at(id);

	c->mId = id;

	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.fixedRotation = true;

	fx32 rect[RECT_SIZE];
	fxVec2 fxsize;
	prGetCollideRect(id, rect);
	fxsize.x = rect[RECT_RIGHT_TOP_X] - rect[RECT_LEFT_TOP_X];
	fxsize.y = rect[RECT_LEFT_BOTTOM_Y] - rect[RECT_LEFT_TOP_Y];
	fxsize.y -= FX_Div(fxsize.x, FX32(2));
	if (fxsize.y <= 0)
	{
		fxsize.y = FX32(4);
	}
	if (fxsize.x <= 0)
	{
		fxsize.x = FX32(4);
	}
	const b2Vec2 b2size = b2PixelsToMeters(fxsize);

	b2Vec2 pos = b2GetGameObjectPosition(id);
	bodyDef.position.Set(pos.x, pos.y - b2size.y); // center

	c->mTexureOffset = b2Vec2(pos.x - bodyDef.position.x, pos.y - bodyDef.position.y);

	// body (main)
	b2PolygonShape polygonShape;
	polygonShape.SetAsBox(b2size.x * 0.5f, b2size.y * 0.5f);
	c->mpBodyB = mpWorld->CreateBody(&bodyDef);
	c->mpPhysicsFixtureB = c->mpBodyB->CreateFixture(&polygonShape, 1.0f);
	c->mpPhysicsFixtureB->SetUserData(c); // mark fixture

	// body circle (feet)
	b2CircleShape circleShape;
	circleShape.m_radius = b2size.x * 0.5f;
	bodyDef.position.Set(pos.x, pos.y - b2size.y * 0.5f);  // center
	c->mpBodyC = mpWorld->CreateBody(&bodyDef);

	b2FixtureDef circleFixtureDef;
	circleFixtureDef.shape = &circleShape;
	circleFixtureDef.density = 1.0f;
	c->mpPhysicsFixtureC = c->mpBodyC->CreateFixture(&circleFixtureDef);
	c->mpPhysicsFixtureC->SetUserData(c); // mark fixture

	// sensor
	circleFixtureDef.isSensor = true;
	c->mpSensorFixture = c->mpBodyC->CreateFixture(&circleFixtureDef);
	c->mpSensorFixture->SetUserData(c); // mark fixture

	b2RevoluteJointDef jointDef;
	jointDef.bodyA = c->mpBodyB;
	jointDef.bodyB = c->mpBodyC;
	jointDef.localAnchorA.Set(0.0f, b2size.y * 0.5f);
	jointDef.localAnchorB.Set(0.0f, 0.0f);
	c->mpJoint = mpWorld->CreateJoint(&jointDef);
}

