#pragma once

#include "tengine.h"
#include "classes/Game.hpp"

class GameLayer
{
 public:
	GameLayer(Game* game);
	virtual ~GameLayer();

	Game* GetGame();

	virtual void OnLayerRelease() = 0;
	virtual void OnLayerLoad() = 0;
	virtual void OnBeginUpdate(const s32& ms) = 0;
	virtual void OnDrawBackgroundTiles(const s32& ms) = 0;
	virtual void OnDrawObject(const s32& ms, const s32& id, BOOL* const opDraw) = 0;
 
 private:

	Game* mpGame;
};