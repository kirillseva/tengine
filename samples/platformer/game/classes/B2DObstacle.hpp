#pragma once

#include "classes/B2DCharacter.hpp"

class B2BObstacle : public B2BMovable
{
	friend class B2BObstacleManager;
	friend class B2BWorld;

public:

	B2BObstacle()
	{
		mType = OT_Obstacle;
		mPassEachOther = false;
	}
};


class B2BObstacleManager : public B2BObjectManager<B2BObstacle>
{
public:

	void Init(const s32* arrId, const s32& size, b2World* world);

	virtual void OnDraw(const s32& id) const override;

private:

	void Create(const s32& id);

	s32 mSize;
};