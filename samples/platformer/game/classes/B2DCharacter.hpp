#pragma once

#include "classes/B2DObject.hpp"
#include <vector>


class B2BMovable : public B2BObject
{
	friend class B2BWorld;

public:
	B2BMovable()
		: mpBodyB(nullptr)
		, mpBodyC(nullptr)
		, mId(-1)
		, mPlatformContacts(0)
	{
		mType = OT_Undef;
	}

	virtual void Release(b2World* world) override
	{
		mContacts.clear();
		mPlatformContacts = 0;

		SDK_ASSERT(world != nullptr);
		if (mpJoint != nullptr)
		{
			world->DestroyJoint(mpJoint);
			mpJoint = nullptr;
		}
		if (mpBodyC != nullptr)
		{
			world->DestroyBody(mpBodyC);
			mpBodyC = nullptr;
		}
		if (mpBodyB != nullptr)
		{
			world->DestroyBody(mpBodyB);
			mpBodyB = nullptr;
		}
	}

protected:

	s32 mId;
	b2Vec2 mTexureOffset;
	b2Fixture* mpPhysicsFixtureB;
	b2Fixture* mpPhysicsFixtureC;
	b2Fixture* mpSensorFixture;
	b2Body* mpBodyB;
	b2Body* mpBodyC;
	b2Joint* mpJoint;
	std::vector<b2Contact*> mContacts;
	s32 mPlatformContacts;
};


class B2BCharacter : public B2BMovable
{
	friend class B2BCharacterManager;
	friend class B2BWorld;

public:

	B2BCharacter()
		: mFootContacts(0)
		, mAllContacts(0)
	{
		mType = OT_NPC;
	}

	virtual void Release(b2World* world) override
	{
		mFootContacts = 0;
		mAllContacts = 0;

		B2BMovable::Release(world);
	}

	bool IsStayOnSomething()
	{
		return (mFootContacts > 0 && mPlatformContacts == 0) ||
			(mPlatformContacts > 0 && mContacts.empty());
	}

private:

	s32 mFootContacts;
	s32 mAllContacts;
};


class B2BCharacterManager : public B2BObjectManager<B2BCharacter>
{
public:

	enum Command
	{
		CC_IDLE = 0x0,
		CC_LEFT = 0x1,
		CC_RIGHT = 0x2,
		CC_JUMP = 0x4
	};

	void Init(const s32* arrId, const s32& size, b2World* world);

	void Update(const s32& ms);

	void SetHero(const s32& id);
	
	void SetCommand(s32 state, const s32& id);

	virtual void OnDraw(const s32& id) const override;

private:

	void Create(const s32& id);

	const float32 CalculateVerticalVelocityForHeight(const float32& desiredHeight) const;

	s32 mSize;
	s32 mMs;
};