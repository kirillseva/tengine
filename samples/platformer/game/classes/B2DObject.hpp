#pragma once

#include <Box2D/Box2D.h>
#include "tengine.h"
#include <map>

class B2BObject
{
public:

	enum Type
	{
		OT_Undef,
		OT_Hero,
		OT_NPC,
		OT_Platform,
		OT_Obstacle
	};

	B2BObject() : mType(OT_Undef), mPassEachOther(true)
	{
	}

	const bool CanStayOnPlatform() const
	{
		return mType == OT_Hero || mType == OT_NPC || mType == OT_Obstacle;
	}

	Type GetType() const
	{
		return mType;
	}

	bool PassEachOther() const
	{
		return mPassEachOther;
	}

	virtual void Release(b2World* world) = 0;

protected:

	void SetType(B2BObject::Type type)
	{
		mType = type;
	}

	Type mType;
	bool mPassEachOther;
};


template <class T>
class B2BObjectManager
{
public:

	void Release()
	{
		if (mMap.size() == 0)
		{
			for (auto& it : mMap)
			{
				//it.second->Release(mpWorld);
				delete it.second;
			}
			mMap.clear();
		}

		mpWorld = nullptr;
	}

	B2BObjectManager() : mpWorld(nullptr)
	{
	}

	virtual void OnDraw(const s32& id) const = 0;

	const T& Get(const s32& id) const
	{
		return *mMap.at(id);
	}

	bool Has(const s32& id) const
	{
		return mMap.find(id) != mMap.end();
	}

protected:

	b2World* mpWorld;
	std::map<s32, T*> mMap;
};
