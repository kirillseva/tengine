attribute highp vec2 a_Position;
attribute mediump vec2 a_TextureCoordinates;
attribute lowp vec4 a_Color;

uniform highp mat4 u_MVP;

varying mediump vec2 v_TextureCoordinates;
varying lowp vec4 v_Color;

void main()
{
    v_TextureCoordinates = a_TextureCoordinates;
    gl_Position = u_MVP * vec4(a_Position.x, a_Position.y, 0.0, 1.0);
	v_Color = a_Color;
}