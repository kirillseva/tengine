#include "tengine.h"
#include "gamefield.h"
#include "level_id.h"
#include "sound.h"
#include "constants.h"

#include "games/game_loader.h"

//-------------------------------------------------------------------------------------------

enum LoadMarkers
{
	LM_INIT = 0,
	LM_LOAD
};

static GameLoader gameLoader;

static void _onEngineEvent(const struct EventCallbackData *pData);
static void _onBeginUpdate(u32 layer);
static void _onDrawBackgroundTiles(u32 layer);
static void _onDrawObject(u32 layer, s32 iId, BOOL* const oDraw);
static void _releaseTengineData(void);
static void _load(void);
static void _startPrepareForLoad(void);
static void _prepareForLoad(void);
static void _doResize(void);
static void _onGameStart(void);
static void _loadGameLayer(GameLayer layer);
static void _exit();

void tfgInitMemory()
{
	InitMemoryAllocator();
}

void tfgInit()
{
	//gameLoader.init();
	gameLoader.setGameStateCallback(GameLoader::GLS_DISPLAYINIT, _doResize);
	gameLoader.setGameStateCallback(GameLoader::GLS_PREPARE_FOR_LOAD, _prepareForLoad);
	gameLoader.setGameStateCallback(GameLoader::GLS_LOADING, _load);
	gameLoader.setGameStateCallback(GameLoader::GLS_EXIT, _exit);
	
	gameLoader.setState(GameLoader::GLS_DISPLAYINIT);
}

void tfgRelease()
{
	_releaseTengineData();
	sndReleaseSoundSystem();
	//gameLoader.setActive(false);
}

void _exit()
{
	//TODO: android
#ifdef WINDOWS_APP
	_releaseTengineData();
	exit(0);
#endif
}

void _releaseTengineData()
{
	gameLoader.release();

	releaseResources();
	releaseMapData(LAYER_HUD);
	releaseMapData(LAYER_GAME);
	releaseMapData(LAYER_LOADING);
	releaseTextData();
	releaseCommonData();
	releaseRenderPlane(BGSELECT_SUB3);
	releaseRenderPlane(BGSELECT_SUB2);
	releaseEngine();
}

void tfgSetWindowModeSize(s32 *const w, s32 *const h)
{
	// this code runs on WINDOWS_APP only
	*w = gameLoader.renderScreenData().viewWidth;
	*h = gameLoader.renderScreenData().viewHeight;
}

void tfgResize(s32 w, s32 h)
{
	gameLoader.setState(GameLoader::GLS_DISPLAYINIT);
	gameLoader.setRenderScreenData(w, h);
}

void _doResize()
{
	tfgRelease();

	struct RenderPlaneInitParams mp1;
	struct RenderPlaneInitParams mp2;
	mp1.mBGType = BGSELECT_SUB3;
	mp1.mSizes.mViewWidth = gameLoader.renderScreenData().viewWidth;
	mp1.mSizes.mViewHeight = gameLoader.renderScreenData().viewHeight;
	mp1.mX = 0;
	mp1.mY = 0;

#ifdef USE_OPENGL_RENDER
	mp1.mMaxRenderObjectsOnPlane = 1200;
#endif
	mp2 = mp1;
	mp2.mBGType = BGSELECT_SUB2;
#ifdef USE_OPENGL_RENDER
	// loading screen has only one object (see mapeditor)
	mp2.mMaxRenderObjectsOnPlane = 2;
#endif

#ifdef USE_CUSTOM_RENDER
	mp1.mColorType = BMP_TYPE_DC16;
	{
		const s32 h8 = ((gameLoader.renderScreenData().viewHeight + 31) / 32) * 32; // must be multiple of 32 (see mapeditor tile size)
		s32 w8 = ((gameLoader.renderScreenData().viewWidth + 31) / 32) * 32;
#ifdef FRAME_ALLOCATOR
		// when the screen orientation changes, it is necessary to re-create a buffer (resize)
		// if defined FRAME_ALLOCATOR, we cannot recreate new buffer for easygraphics module because of assertion)
		// so the buffer must be the same for the larger size of the screen
		w8 = (w8 > h8) ? w8 : h8;
		mp1.mSizes.mFrameBufferWidth8 = (u16)w8;
		mp1.mSizes.mFrameBufferHeight8 = (u16)w8;
#else //FRAME_ALLOCATOR
		mp1.mSizes.mFrameBufferWidth8 = w8;
		mp1.mSizes.mFrameBufferHeight8 = h8;
#endif //FRAME_ALLOCATOR
}
#endif //USE_CUSTOM_RENDER

	if (!isRenderPlaneInit(LAYER_GAME)) // any active plane, whatever
	{
		_releaseTengineData();

		struct InitEngineParameters initparams;
		initparams.layersCount = LAYER_COUNT;
		initparams.particlesPoolSize = 1000;

		initEngine(&initparams);
		initRenderPlane(&mp1);
		initRenderPlane(&mp2);

		assignLayerWithRenderPlane(LAYER_GAME, BGSELECT_SUB3, TRUE);
		assignLayerWithRenderPlane(LAYER_HUD, BGSELECT_SUB3, FALSE);

		assignLayerWithRenderPlane(LAYER_LOADING, BGSELECT_SUB2, TRUE);

		//register callbacks
		setOnEvent(_onEngineEvent);
		setOnBeginUpdate(_onBeginUpdate);
		setOnDrawGameObject(_onDrawObject);
		setOnDrawBackgroundTiles(_onDrawBackgroundTiles);

		gameLoader.init();

		//begin load task
		//parameter will return with iData.initiatorId in onEngineEvent() function ( EVENT_TYPE_ON_END_ASYNH_LOAD_DATA)
		beginLoadListAsynh(LM_INIT);
		{
			//load common data
			addToLoadListCommonData();
			//load text data for certain language
			// to avoid memory fragmentation load it once after common data
			addToLoadListLanguageData(LANGUAGE_ENG);

			addToLoadListMap(LAYER_LOADING, GAMELEVEL_IND_LOADING);
		}
		//end load task
		endLoadListAsynh(FALSE);

		//wait for  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA in onEngineEvent() function
		gameLoader.setState(GameLoader::GLS_NONE);
	}
	else
	{
		resizeRenderPlane(LAYER_GAME, &mp1.mSizes);
		gameLoader.setState(GameLoader::GLS_GAME);
	}

	gameLoader.setRenderPlaneScale();
}

void _load()
{
	gameLoader.beginLoad();

	// to avoid memory fragmentation, releaseMapData in opposite side
	releaseResources();
	releaseMapData(LAYER_HUD);
	releaseMapData(LAYER_GAME);
	OS_Printf("mem on newGame = %u\n", GetFreeMemorySize());
	beginLoadListAsynh(LM_LOAD);
	{
		//layer LAYER_GAME, load current level
		addToLoadListMap(LAYER_GAME, GAMELEVEL_IND_GAME);
		//layer LAYER_HUD, load or reload hud
		addToLoadListMap(LAYER_HUD, GAMELEVEL_IND_HUD);
	}
	endLoadListAsynh(TRUE);
	//wait for  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA and EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES in onEngineEvent() function
	gameLoader.setState(GameLoader::GLS_NONE);
}

void _startPrepareForLoad()
{
	gameLoader.setState(GameLoader::GLS_PREPARE_FOR_LOAD);
}

void _prepareForLoad()
{
	drawWithoutInternalLogicUpdateMode();
	gameLoader.setState(GameLoader::GLS_LOADING);
}

void _onGameStart()
{
	gameLoader.setLayerVisibleGame(TRUE);
	gameLoader.setState(GameLoader::GLS_GAME);
}

void _loadGameLayer(GameLayer layer)
{
	(void)layer;
	//gameLoader.layerLoad(layer);
}

void tfgLostRenderDevice()
{
}

void tfgRestoreRenderDevice()
{
}

void tfgLowMemory()
{
}

void tfgTick(s32 ms)
{
	gameLoader.update(ms);
}

void _onBeginUpdate(u32 layer)
{
	gameLoader.onBeginUpdate(static_cast<GameLayer>(layer));
}

void _onDrawBackgroundTiles(u32 layer)
{
	gameLoader.onDrawBackgroundTiles(static_cast<GameLayer>(layer));
}

void _onDrawObject(u32 layer, s32 iId, BOOL* const oDraw)
{
	gameLoader.onDrawObject(static_cast<GameLayer>(layer), iId, oDraw);
}

void _onEngineEvent(const struct EventCallbackData *pData)
{
	switch(pData->eventType)
	{
	case EVENT_TYPE_ON_END_ASYNH_LOAD_DATA:
		switch(pData->eventId)
		{
		case LOAD_TYPE_ENDLOADDATATASK:
			if(pData->initiatorId == LM_INIT)
			{
				_startPrepareForLoad();
			}
			break;
		case LOAD_TYPE_MAPDATA:
			if(pData->initiatorId == LM_LOAD)
			{
				_loadGameLayer(static_cast<GameLayer>(pData->layer));				
			}
			break;

		default:
			break;
		}
		break;

	case EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES:
		_onGameStart();
		break;

	default:
		break;
	}
}
