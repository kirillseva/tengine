#ifndef NK_TENGINE_H_
#define NK_TENGINE_H_

#ifdef NK_TENGINE_IMPLEMENTATION

#include "external.h"

#ifdef __cplusplus
extern "C" {
#endif

extern struct nk_context* nk_tengine_init(struct nk_user_font* pfont);
extern void nk_tengine_render(u32 layer, enum nk_anti_aliasing);
extern void nk_tengine_shutdown(void);

struct nk_tengine_device
{
    struct nk_buffer cmds;
    struct nk_draw_null_texture null;
    s32 font_id;
};

static struct nk_tengine
{
    struct nk_tengine_device ogl;
    struct nk_context ctx;
	BOOL init;
} _tengine = {0};

static void* nk_custom_draw_vertex(void *draw_list, const struct nk_convert_config *config, struct nk_vec2 pos, struct nk_vec2 uv, struct nk_colorf color)
{
	struct TRGLColorVertexData cvd;
	SDK_NULL_ASSERT(draw_list);
	(void)uv;
	(void)config;
	ExD_SetActiveClipRect((const struct ExDRect*)&((struct nk_draw_list *)draw_list)->clip_rect);
	MI_CpuCopy8(&color.r, &cvd.color[0], sizeof(float) * 4);
	MI_CpuCopy8(&pos.x, &cvd.pos[0], sizeof(float) * 2);
	ExD_AddRenderExternalPosColorData(&cvd);
	return NULL;
}

static void nk_custom_draw_list_add_text(struct nk_draw_list* draw_list, const struct nk_user_font* font, struct nk_rect rect,
											const nk_tchar *text, int len, float font_height, struct nk_color color)
{
	const GXRgba bg_color = 0;
	struct ExDRect txt_rect;
	SDK_NULL_ASSERT(draw_list);
	SDK_NULL_ASSERT(font);
	MI_CpuCopy8(&rect.x, &txt_rect.x, sizeof(float) * 4);
	txt_rect.h = font_height;
	ExD_DrawText(font->userdata.id, (const wchar*)text, (u32)len, (const struct ExDRect*)&txt_rect,
					(const struct ExDRect*)&((struct nk_draw_list *)draw_list)->clip_rect,
					COLOR888TO1555(color.r, color.g, color.b), bg_color, FALSE);
}

static void nk_tengine_clipbard_paste(nk_handle usr, struct nk_text_edit *edit)
{
	(void)usr;
	(void)edit;
}

static void nk_tengine_clipbard_copy(nk_handle usr, const nk_tchar* text, int len)
{
	(void)usr;
	(void)text;
	(void)len;
}

static float nk_custom_font_get_text_width(nk_handle handle, float height, const nk_tchar *text, int len)
{
	(void)height;
	return (float)ExD_GetTextWidth(handle.id, (const wchar*)text, (u32)len);
}

// EXTERN Functions

extern void nk_tengine_render(u32 layer, enum nk_anti_aliasing AA)
{
	struct nk_buffer vbuf;
	struct nk_convert_config config;
	struct nk_tengine_device *dev = &_tengine.ogl;
	static const struct nk_draw_vertex_layout_element vertex_layout[] = {
		{NK_VERTEX_POSITION, NK_FORMAT_FLOAT, NK_OFFSETOF(struct TRGLColorVertexData, pos)},
		// {NK_VERTEX_TEXCOORD, NK_FORMAT_FLOAT, NK_OFFSETOF(struct nk_temp_vertex, uv)},
		{NK_VERTEX_COLOR, NK_FORMAT_FLOAT, NK_OFFSETOF(struct TRGLColorVertexData, color)},
		{NK_VERTEX_LAYOUT_END}
	};
#ifdef SDK_DEBUG
	SDK_ASSERT(getVisible(layer) == TRUE);
#else
	(void)layer;
#endif
	NK_MEMSET(&config, 0, sizeof(config));
	config.vertex_layout = vertex_layout;
	config.vertex_size = sizeof(struct TRGLColorVertexData);
	config.vertex_alignment = NK_ALIGNOF(struct TRGLColorVertexData);
	config.null = dev->null;
	config.circle_segment_count = 22;
	config.curve_segment_count = 22;
	config.arc_segment_count = 22;
	config.global_alpha = 1.0f;
	config.shape_AA = AA;
	config.line_AA = AA;

	nk_buffer_init_default(&vbuf);
	nk_convert(&_tengine.ctx, &dev->cmds, &vbuf, NULL, &config);

	nk_clear(&_tengine.ctx);
	nk_buffer_free(&vbuf);
}

extern BOOL nk_is_tengine_init(void)
{
	return _tengine.init;
}

extern struct nk_context* nk_tengine_init(struct nk_user_font* pfont)
{
	SDK_ASSERT(_tengine.init == FALSE);
	if(_tengine.init == FALSE)
	{
		struct nk_user_font *font = pfont;
		SDK_NULL_ASSERT(pfont);
		_tengine.init = TRUE;
		font->height = (float)Font_GetSize(ExD_GetFont(pfont->userdata.id));
		font->width = nk_custom_font_get_text_width;
		nk_init_default(&_tengine.ctx, font);
		_tengine.ctx.draw_list.draw_vertex = nk_custom_draw_vertex;
		_tengine.ctx.draw_list.draw_text = nk_custom_draw_list_add_text;
		_tengine.ctx.clip.copy = nk_tengine_clipbard_copy;
		_tengine.ctx.clip.paste = nk_tengine_clipbard_paste;
		_tengine.ctx.clip.userdata = nk_handle_ptr(0);
		nk_buffer_init_default(&_tengine.ogl.cmds);
		return &_tengine.ctx;
	}
	return NULL;
}

extern void nk_tengine_shutdown(void)
{
	if(_tengine.init == TRUE)
	{
		struct nk_tengine_device *dev = &_tengine.ogl;
		nk_free(&_tengine.ctx);
		nk_buffer_free(&dev->cmds);
		_tengine.init = FALSE;
	}
}

#ifdef __cplusplus
} /* extern "C" */
#endif
#endif
#endif
