#pragma once

#include "utils.h"

class StateMachine
{
public:
	StateMachine();
	virtual ~StateMachine() {}

	void update(Int ms);

	void setActive(bool act);
	bool getActive() const { return active; }

	void setState(Int newState);
	Int getState() const { return state; }

protected:
	virtual void onActiveChangedBase() {}
	virtual void onStateUpdateBase(Int ms) { (void)ms; }
	virtual void onStateEnterBase() {}
	virtual void onStateFinishBase() {}

private:
	bool active;
	Int state;
};