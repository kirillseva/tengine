#include "state_machine.h"

StateMachine::StateMachine()
	: active(false)
	, state(-1)
{

}

void StateMachine::setActive(bool act)
{
	active = act;
	onActiveChangedBase();
}

void StateMachine::setState(Int newState)
{
	if (state >= 0)
	{
		onStateFinishBase();
	}

	state = newState;

	onStateEnterBase();
}

void StateMachine::update(Int ms)
{
	if (getActive())
	{
		onStateUpdateBase(ms);
	}
}

