#include "units_pool.h"
#include "touchpad.h"
#include "level_id.h"
#include "units/units.h"
#include "games/level.h"


UnitsPool::UnitsPool()
	: unitsCount(0)
{
	for (Int i = 0; i < UNITS_COUNT; ++i)
	{
		units[i] = NULL;
	}

	addUnits(UnitClass::U_UNIT, 0, 0);
	//addUnits(UnitClass::U_ZOMBIE, U_ZOMBIE_ID, U_ZOMBIE_COUNT);
}

UnitsPool::~UnitsPool()
{
	for (Int i = 0; i < unitsCount; ++i)
	{
		delete units[i];
		units[i] = NULL;
	}
}

void UnitsPool::addUnit(Unit* unit)
{
	units[unitsCount++] = unit;
}

Unit* UnitsPool::newUnit(UnitClass unitClass, Int id)
{
	switch (unitClass)
	{
	//case UnitClass::U_ZOMBIE:
	//	return new Zombie(id);
	//	break;
	default:
		SDK_ASSERT(false);
		break;
	}

	return NULL;
}

void UnitsPool::addUnits(UnitClass unitClass, Int firstId, Int count)
{
	unitsInd[(Int)unitClass] = { getUnitsCount(), count };
	for (Int i = firstId; i < firstId + count; ++i)
	{
		addUnit(newUnit(unitClass, i));
	}
}

Unit* UnitsPool::getUnitFromPool(UnitClass unitClass)
{
	Int indFrom = unitsInd[(Int)unitClass].startIndex;
	Int indTo = indFrom + unitsInd[(Int)unitClass].count;
	for (Int i = indFrom; i < indTo; ++i)
	{
		if (units[i]->getClass() == unitClass)
		{
			if (!units[i]->getActive())
			{
				return units[i];
			}
		}
	}
	SDK_ASSERT(false);
	return NULL;
}

void UnitsPool::updateUnits(Int ms)
{
	for (Int i = 0; i < getUnitsCount(); ++i)
	{
		units[i]->update(ms);
	}
}

void UnitsPool::reset()
{
	for (Int i = 0; i < getUnitsCount(); ++i)
	{
		if (units[i]->getActive())
		{
			units[i]->setActive(false);
		}
	}
}
