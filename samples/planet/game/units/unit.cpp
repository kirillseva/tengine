#include "unit.h"
#include "constants.h"
#include "games/level.h"
#include "games/level_game.h"

Unit::Unit(Int unitid, UnitClass uClass)
	: StateMachine()
	, id(unitid)
	, unitClass(uClass)
	, updateProp(P_NONE)
	, position({ 0, 0 })
	, direction({ 1, 0 })
	, speed(0)
	, health(20)
	, level(NULL)
{
	reset();
}

Unit::~Unit()
{
}

void Unit::setLevel(Level* lev)
{ 
	level = dynamic_cast<LevelGame*>(lev); 
	SDK_ASSERT(level);
}

void Unit::reset()
{
	updateProp = P_NONE;
	position = { 0, 0 };
	direction = { 1, 0 };
	speed = 0;
	health = 20;
}

void Unit::onActiveChangedBase()
{
	prSetProperty(getId(), PRP_ENABLE, getActive());

	onActiveChanged();
	if (!getActive())
	{
		reset();
	}
}

void Unit::onStateUpdateBase(Int ms)
{
	updateHealth();
	updateState();
	updateMove(ms);

	onStateUpdate(ms);
}

void Unit::onStateEnterBase()
{
	setNeedUpdate(Prop::P_STATE);
}

void Unit::onStateFinishBase()
{
	onStateFinish();
}

bool Unit::isAlive() const
{
	return getActive() && !isDead();
}

bool Unit::isIdle() const
{
	return getState() == STATE_IDLE;
}

bool Unit::isDead() const
{
	return false;
}

bool Unit::isHit() const
{
	return false;
}

bool Unit::isAttack() const
{
	return false;
}

bool Unit::needUpdate(Unit::Prop prop) const
{
	return updateProp & (Int)prop;
}

void Unit::setNeedUpdate(Unit::Prop prop)
{
	updateProp |= (Int)prop;
}

void Unit::resetNeedUpdate(Unit::Prop prop)
{
	updateProp &= ~(Int)prop;
}

void Unit::updateHealth()
{
	if (needUpdate(Prop::P_HEALTH))
	{
		onHit();

		resetNeedUpdate(P_HEALTH);
	}
}

void Unit::updateState()
{
	if (needUpdate(Prop::P_STATE))
	{
		//prSetState(id, getState(), SBM_FROM_END_ANIMATION);
		prSetState(id, getState(), SBM_NONE);
		onStateEnter();

		resetNeedUpdate(P_STATE);
	}
}

bool Unit::needUpdateMove()
{
	return getActive() && isMovable();
}

void Unit::updateMove(Int ms)
{
	if (needUpdateMove())
	{
		position = fxVec2Add(position, fxVec2Mul(direction, { speed * ms, speed * ms }));
		onMove(ms);
		prSetPosition(id, position);
	}
}

bool Unit::collideVec(Vector p0, Vector p1) const
{
	Rect rect;
	prGetCollideRect(id, rect);

	Float w = fxAbs(rect[RECT_RIGHT_BOTTOM_X] - rect[RECT_LEFT_TOP_X]);
	Float h = fxAbs(rect[RECT_RIGHT_BOTTOM_Y] - rect[RECT_LEFT_TOP_Y]);

	Float sz = fxMin(w, h);

	Vector v = fxVec2Sub(p1, p0);
	Float len = fxVec2Length(v);

	Int cnt = (Int)(len / sz);
	Vector step = fxVec2Div(v, { -(Float)cnt, -(Float)cnt });

	bool res = false;
	Vector p = p1;
	for (Int i = 0; i < cnt; ++i)
	{
		res = collide(p);
		if (res)
		{
			break;
		}
		p = fxVec2Add(p, step);
	}
	if (!res)
	{
		res = collide(p0);
	}
	return res;
}

bool Unit::collide(Vector pos) const
{
	return collide(pos.x, pos.y);
}

bool Unit::collide(Float x, Float y) const
{
	Rect rect;
	prGetCollideRect(id, rect);

	if (rect[RECT_LEFT_TOP_X] <= x && x <= rect[RECT_RIGHT_BOTTOM_X] &&
		rect[RECT_LEFT_TOP_Y] <= y && y <= rect[RECT_RIGHT_BOTTOM_Y])
	{
		return true;
	}

	return false;
}

void Unit::setHealth(Int h)
{ 
	if (h < 0)
	{
		health = 0;
	}
	else
	{
		health = h;
	}
	setNeedUpdate(P_HEALTH); 
}

bool Unit::hit(Int damage)
{
	if (isAlive())
	{
		setHealth(getHealth() - damage);
		return true;
	}

	//TODO
	//explode();
	//return true;
	return false;
}

void Unit::explode()
{
	
}

GameLayer Unit::getLayer() const
{
	return getLevel() ? getLevel()->getLayer() : GameLayer::LAYER_COUNT;
}
