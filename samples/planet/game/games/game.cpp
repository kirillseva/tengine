#include "games/game.h"

#include "games/game_loader.h"
#include "games/level.h"
#include "games/input.h"
#include "games/level_game.h"
#include "games/level_hud.h"
#include "games/level_loading.h"
#include "games/game_data.h"
#include "units/units_pool.h"

Game::Game(GameLoader* loader)
	: gameLoader(loader)
	, data(new GameData(this))
	, currentMS(0)
	, input(NULL)
	, signal(SG_COUNT)
{
	for (s32 i = 0; i < LAYER_COUNT; i++)
	{
		levels[i] = NULL;
	}

	signals[SG_NEW_GAME] = &Game::newGame;
	signals[SG_MAIN_MENU] = &Game::mainMenuGame;
	signals[SG_RESTART_LEVEL] = &Game::restartLevelGame;
	signals[SG_NEXT_LEVEL] = &Game::nextLevelGame;
	signals[SG_EXIT_GAME] = &Game::exitGame;
	signals[SG_PAUSE_MENU] = &Game::pauseGame;
	signals[SG_RESUME_GAME] = &Game::resumeGame;
	signals[SG_STATUS_MENU] = &Game::statusMenuGame;
	signals[SG_QUESTION_MENU] = &Game::questionMenuGame;
	signals[SG_LOAD_GAME] = &Game::loadGame;
}

Game::~Game()
{
	delete input;
	releaseLevels();
}

void Game::onActiveChangedBase()
{
	if (getActive())
	{
		setState(GameState::GS_MAIN_MENU);
	}
}

bool Game::inMainMenu() const
{
	return getActive() && getState() == GS_MAIN_MENU;
}

void Game::onStateEnterBase()
{
	setPauseEngine(FALSE);
	switch (getState())
	{
	case GS_PAUSE_MENU:
	case GS_MAIN_MENU:
	case GS_STATUS_MENU:
	case GS_QUESTION_MENU:
		setPauseEngine(TRUE);
		break;
	}

	for (Int i = 0; i < LAYER_COUNT; ++i)
	{
		if (i != LAYER_LOADING)
		{
			if (!levels[i]->getActive())
			{
				levels[i]->setActive(true);
			}
		}
	}
	for (Int i = 0; i < LAYER_COUNT; ++i)
	{
		if (i != LAYER_LOADING)
		{
			levels[i]->setState(getState());
		}
	}
}

void Game::updateState(Int ms)
{
	currentMS = ms;
	//updateInput();
}

void Game::load()
{
	for (s32 i = 0; i < LAYER_COUNT; i++)
	{
		levels[i]->load();
	}
}

void Game::levelLoad(GameLayer layer)
{
	if (levels[layer])
	{
		levels[layer]->load();
	}
}

void Game::levelRelease(GameLayer layer)
{
	if (levels[layer])
	{
		levels[layer]->release();
	}
}

void Game::init()
{
	if (!input)
	{
		input = new Input();
		input->init();
	}
	if (levels[LAYER_HUD] == NULL)
	{
		levels[LAYER_HUD] = new LevelHud(this);
		levels[LAYER_GAME] = new LevelGame(this);
		levels[LAYER_LOADING] = new LevelLoading(this);
	}
}

void Game::releaseLevels()
{
	levelRelease(LAYER_HUD);
	levelRelease(LAYER_GAME);
	levelRelease(LAYER_LOADING);

	for (s32 i = 0; i < LAYER_COUNT; i++)
	{
		if (levels[i])
		{
			delete levels[i];
			levels[i] = NULL;
		}
	}
}

Level* Game::getLevel(GameLayer layer)
{
	return levels[layer];
}

void Game::onBeginUpdate(GameLayer layer)
{
	if (layer == LAYER_GAME)
	{
		updateInput();

		if (signal != SG_COUNT)
		{
			OnGameSignalCallback callback = signals[signal];
			(this->*callback)();

			signal = SG_COUNT;
		}
	}
	SDK_ASSERT(levels[layer]);
	levels[layer]->update(currentMS);
}

void Game::onDrawBackgroundTiles(GameLayer layer)
{
	SDK_ASSERT(levels[layer]);
	levels[layer]->onDrawBackgroundTiles(currentMS);
}

void Game::onDrawObject(GameLayer layer, const u32& iId, BOOL* const opDraw)
{
	levels[layer]->onDrawObject(currentMS, iId, opDraw);
}

void Game::updateInput()
{
	if (getState() != GS_NONE)
	{
		input->update();
		const TouchPadData& tpdata = input->touchPadData();
		for (Int i = 0; i < LAYER_COUNT; ++i)
		{
			levels[i]->updateInput(tpdata);
		}
	}
}

void Game::reset(bool newGame)
{
	data->reset(newGame);
	levels[LAYER_GAME]->reset();
}

void Game::newGame()
{
	reset(true);
	setState(Game::GS_LEVEL);
}

void Game::loadGame()
{
	//TODO
	reset(true);
	gameData()->setCurrentLevel(4);
	setState(Game::GS_LEVEL);
}

void Game::mainMenuGame()
{
	reset();
	setState(Game::GS_MAIN_MENU);
}

void Game::restartLevelGame()
{
	reset();
	setState(Game::GS_LEVEL);
}

void Game::nextLevelGame()
{
	reset();
	gameData()->nextLevel();
	setState(Game::GS_LEVEL);
}

void Game::exitGame()
{
	gameLoader->exit();
}

void Game::pauseGame()
{
	setState(Game::GS_PAUSE_MENU);
}

void Game::resumeGame()
{
	setState(Game::GS_LEVEL);
}

void Game::statusMenuGame()
{
	setState(Game::GS_STATUS_MENU);
}

void Game::questionMenuGame()
{
	setState(Game::GS_QUESTION_MENU);
}
