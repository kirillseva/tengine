#include "level_game.h"
#include "units/unit.h"
#include "games/game_data.h"
#include "games/game.h"

#define FINISH_GAME_TIMER 2000
#define FINISH_GAME_TIMER2 32

LevelGame::LevelGame(Game* gm)
	: LevelWithUnits(GameLayer::LAYER_GAME, gm)
	, needReset(false)
{

}

LevelGame::~LevelGame()
{
}

void LevelGame::onActiveChangedBase()
{
	if (getActive())
	{
		reset();
	}
	LevelWithUnits::onActiveChangedBase();
}

Int LevelGame::tileSize() const
{ 
	//TODO
	return 100; 
}

void LevelGame::load()
{
	cameraStartPos = {600, 600};
	cameraMainMenuPos = cameraStartPos;
}

void LevelGame::updateState(Int ms)
{
	if (!updateActive())
	{
		return;
	}
	updateUnits(ms);
	updateLogic(ms);
}

void LevelGame::updateLogic(Int ms)
{
	updateLevel(ms);
}

void LevelGame::updateLevel(Int ms)
{
	static Int cnt = 0;
	if (cnt == 0)
	{
		cnt++;
		//createUnit(UnitClass::U_ZOMBIE, getCamera());
	}
}

void LevelGame::onUpdateHitPoint()
{
	LevelWithUnits::onUpdateHitPoint();
}

void LevelGame::onStateEnter()
{
	if (needReset)
	{
		LevelWithUnits::resetUnits();
		load();
		needReset = false;
	}

	if (getState() == Game::GS_MAIN_MENU)
	{
		setCamera(cameraMainMenuPos);
	}
	else if (getState() == Game::GS_LEVEL || getState() == Game::GS_PAUSE_MENU || getState() == Game::GS_STATUS_MENU)
	{
		setCamera(cameraStartPos);
	}
}

bool LevelGame::updateActive() const
{
	return getState() == Game::GS_LEVEL;
}

bool LevelGame::inputActive() const
{
	return updateActive() && !gameData()->levelFinishCondition();
}

void LevelGame::reset()
{
	needReset = true;
}

bool LevelGame::unitClick(Unit* unit, Int x, Int y)
{ 

	bool res = false;

	return res;
}
