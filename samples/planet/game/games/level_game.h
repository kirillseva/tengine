#pragma once

#include "level_with_units.h"

class GameData;

#define MARKER_COUNT (LAST_MARKER-FIRST_MARKER+1)

class LevelGame : public LevelWithUnits
{
public:
	LevelGame(Game* gm);
	virtual ~LevelGame() override;

	Int tileSize() const;

	virtual void reset() override;

	Vector getStartPoint(Int startPointId) const;

private:
	void updateLogic(Int ms);
	void updateLevel(Int ms);
	virtual void load() override;
	virtual void updateState(Int ms) override;
	virtual void onUpdateHitPoint() override;
	virtual bool inputActive() const override;
	virtual void onActiveChangedBase() override;
	bool updateActive() const;
	virtual void onStateEnter() override;
	virtual bool unitClick(Unit* unit, Int x, Int y) override;

	Vector cameraStartPos;
	Vector cameraMainMenuPos;
	bool needReset;
};