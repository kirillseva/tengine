#pragma once

#include "level_id.h"
#include "common/state_machine.h"

class GameLoader;
class Level;
class Input;
class UnitsPool;
class GameData;

class Game : public StateMachine
{
public:
	enum GameState
	{
		GS_NONE = 0,
		GS_LEVEL_START,
		GS_LEVEL,
		GS_MAIN_MENU,
		GS_PAUSE_MENU,
		GS_STATUS_MENU,
		GS_QUESTION_MENU,
	};

	enum GameSignal
	{
		SG_NEW_GAME,
		SG_MAIN_MENU,
		SG_RESTART_LEVEL,
		SG_NEXT_LEVEL,
		SG_EXIT_GAME,
		SG_PAUSE_MENU,
		SG_RESUME_GAME,
		SG_STATUS_MENU,
		SG_QUESTION_MENU,
		SG_LOAD_GAME,

		SG_COUNT,
	};

	Game(GameLoader* laoder);
	virtual ~Game() override;

	Level* getLevel(GameLayer layer = LAYER_GAME);

	GameData* gameData() { return data; }

	void init();

	bool inMainMenu() const;

	void updateState(Int ms);
	void onBeginUpdate(GameLayer layer);
	void onDrawBackgroundTiles(GameLayer layer);
	void onDrawObject(GameLayer layer, const u32& iId, BOOL* const opDraw);

	void setSignal(GameSignal s) { signal = s; }

//	void addUnitInfo(Int maxHealth, Int health, Vector pos);

private:
	typedef void (Game::*OnGameSignalCallback)();

	virtual void onActiveChangedBase() override;
	virtual void onStateEnterBase() override;

	void releaseLevels();

	void load();
	void levelLoad(GameLayer layer);
	void levelRelease(GameLayer layer);

	void updateInput();
	void updateSignal();

	void newGame();
	void mainMenuGame();
	void restartLevelGame();
	void nextLevelGame();
	void exitGame();
	void pauseGame();
	void resumeGame();
	void statusMenuGame();
	void questionMenuGame();
	void loadGame();


	void reset(bool newGame = false);

private:
	GameLoader* gameLoader;
	GameData* data;

	Int currentMS;

	Level* levels[LAYER_COUNT];
	Input* input;

	GameSignal signal;
	OnGameSignalCallback signals[SG_COUNT];
};