#pragma once

#include "games/game_layer.h"

class LoadingLayer : public GameLayer
{
 public:
	LoadingLayer(Game* game);
};