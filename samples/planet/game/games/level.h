#pragma once

#include "units/units_data.h"
#include "utils.h"
#include "level_id.h"
#include "common/state_machine.h"

class Unit;
class Game;
class GameData;
class UnitsPool;
struct TouchPadData;

class Level : public StateMachine
{
public:
	Level(GameLayer lr, Game* gm);
	virtual ~Level();

	Game* getGame() { return game; }
	GameData* gameData();
	const GameData* gameData() const;

	GameLayer getLayer() const { return layer; }

	virtual void reset() {}

	virtual void load() {}
	virtual void release() {}

	void update(Int ms);
	virtual void onDrawObject(const s32& ms, const s32& id, BOOL* const opDraw) { (void)ms; (void)id; (void)opDraw;	}
	virtual void onDrawBackgroundTiles(Int ms) { (void)ms; }

	virtual void updateInput(const TouchPadData& tpdata) { (void)tpdata; }

protected:
	virtual void updateState(Int ms) { (void)ms; }
	virtual void updateInPause() {}
	virtual void onStateEnter() {}

private:
	
	virtual void onStateEnterBase() override;

	Game* game;
	bool newState;
	GameLayer layer;
};