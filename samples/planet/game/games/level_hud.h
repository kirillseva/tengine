#pragma once

#include "level.h"

#define UNIT_INFO_COUNT 100

class LevelHud : public Level
{
public:
	LevelHud(Game* gm);

	//void addUnitInfo(Int maxHealth, Int health, Vector pos);

private:
	enum PauseButtonState
	{
		PB_NONE,
		PB_PAUSE,
		PB_RESUME,
	};

	struct MenuParam
	{
		float x;
		float y;
		float width;
		float height;
		float buttonHeight;
		float buttonCount;
	};

	virtual void load() override;
	virtual void release() override;
	virtual void updateState(Int ms) override;
	virtual void onDrawObject(const s32& ms, const s32& id, BOOL* const opDraw) override;
	virtual void onStateEnter() override;
	virtual void updateInput(const TouchPadData& tpdata) override;

	void init(MenuParam& param, float buttonCount = 2.0f, float buttonHeight = 120.0f, float width = 500.0f);

	void createHud();
	void drawNewGameButton();
	void drawMainMenu();
	void drawPauseMenu();
	void drawGameMenu(PauseButtonState pauseState);
	void drawStatusMenu();
	void drawQuestionMenu();

	void switchQ(Int stateQ);



	wchar aaa[16];
	wchar bbb[16];
	s32 currentFPS;

	Int stateYes;
	Int stateNo;
};