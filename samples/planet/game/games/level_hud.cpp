#include "touchpad.h"
#include "external.h"

#include "level_hud.h"
#include "constants.h"
#include "texts.h"
#include "games/game.h"
#include "games/game_data.h"

#pragma warning (push)
#pragma warning (disable : 4217)
#pragma warning (disable : 4505)
#pragma warning (disable : 4701)
#pragma warning (disable : 4127)
#pragma warning (disable : 4075)

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_DRAW_VERTEX_CUSTOM_FUNCTION
#define NK_INCLUDE_DRAW_TEXT_CUSTOM_FUNCTION
#define NK_INCLUDE_DISABLE_KEYBOARD_EDIT 
#define NK_INCLUDE_UNICODE_SUPPORT
#if defined ANDROID_NDK || defined IOS_APP
#define NK_INCLUDE_TOUCHPAD_MODE
#endif
#define NK_IMPLEMENTATION
#define NK_TENGINE_IMPLEMENTATION
#include "nuklear.h"
#include "nuklear_helper.h"

struct nk_context *main_ctx = NULL;
struct nk_user_font main_font;

static float speed_slider = 5.0f;
static float rotation_slider = 0.0f;
static float scale_slider = 1.0f;

#define PANEL_WIDTH 500.0f
#define BUTTON_HEIGHT 120.0f

LevelHud::LevelHud(Game* gm)
	: Level(GameLayer::LAYER_HUD, gm)
	, currentFPS(0)
	, stateYes(Game::GS_NONE)
	, stateNo(Game::GS_NONE)
{
	bbb[0] = L'f';
	bbb[1] = L'p';
	bbb[2] = L's';
	bbb[3] = L'=';
	bbb[4] = L'%';
	bbb[5] = L'd';
	bbb[6] = L'\0';
}

void LevelHud::onDrawObject(const s32& ms, const s32& id, BOOL* const opDraw)
{
	(void)ms;
	(void)opDraw;
	switch (id)
	{
	case M1_TXTBOX_FPS:
		if (currentFPS != getFPS())
		{
			currentFPS = getFPS();
			STD_WSnprintf(aaa, 7, bbb, currentFPS);
			txbSetDynamicTextLine(M1_TXTBOX_FPS, 0, aaa);
		}
		break;
	default:
		break;
	}
}

void LevelHud::load()
{
}

void LevelHud::createHud()
{
	if (nk_is_tengine_init() == FALSE)
	{
		main_font.userdata.id = M1_FONT_ARIAL_48; // font id
		main_ctx = nk_tengine_init(&main_font);

		nk_color trans = { 100, 100, 100, 100 };
		main_ctx->style.window.fixed_background.data.color = trans;

		main_ctx->style.button.normal = nk_style_item_color(nk_rgba(255, 165, 0, 64));
		main_ctx->style.button.hover = nk_style_item_color(nk_rgb(255, 165, 0));
		main_ctx->style.button.active = nk_style_item_color(nk_rgb(220, 10, 0));
		main_ctx->style.button.border_color = nk_rgb(255, 165, 0);
		main_ctx->style.button.text_background = nk_rgb(0, 0, 0);
		main_ctx->style.button.text_normal = nk_rgb(80, 80, 150);  //nk_rgb(255, 165, 0);
		main_ctx->style.button.text_hover = nk_rgb(80, 80, 150);
		main_ctx->style.button.text_active = nk_rgb(80, 80, 150);
		main_ctx->style.text.color = nk_rgb(80, 150, 80);
		main_ctx->style.progress.border_color = nk_rgb(255, 165, 0);
		main_ctx->style.progress.normal.type = NK_STYLE_ITEM_COLOR;
		main_ctx->style.progress.normal.data.color = nk_rgba(100, 100, 100, 64);
		main_ctx->style.progress.cursor_normal.data.color = nk_rgba(80, 150, 80, 220);
	}
}

void LevelHud::release()
{
	if (main_ctx)
	{
		nk_tengine_shutdown();
		main_ctx = NULL;
	}
}

void LevelHud::init(MenuParam& param, float buttonCount, float buttonHeight, float width)
{
	param.buttonCount = buttonCount;
	param.buttonHeight = buttonHeight;

	const float diffX = main_ctx->style.window.border * 2.0f;
	const float diffY = main_ctx->style.window.min_row_height_padding * (buttonCount - 1.0f) + main_ctx->style.window.border * 2.0f;

	param.height = param.buttonHeight * param.buttonCount + diffY;
	param.width = width;

	Vector pos = getCamera();
	pos = fxVec2Add(pos, { -getViewOffsetX(getLayer()) - param.width * 0.5f, -getViewOffsetY(getLayer()) - param.height * 0.5f });

	param.x = (float)pos.x;
	param.y = (float)pos.y;
}

void LevelHud::drawMainMenu()
{
	MenuParam panel;
	init(panel, 3.0f);

	if (nk_begin(main_ctx, NK_T("Menu"), nk_rect(panel.x, panel.y, panel.width, panel.height), NK_WINDOW_NO_SCROLLBAR))
	{
		nk_layout_row_dynamic(main_ctx, panel.buttonHeight, 1);

		drawNewGameButton();

		if (nk_button_label(main_ctx, NK_T("Load Game")))
		{
			getGame()->setSignal(Game::SG_LOAD_GAME);
		}

		if (nk_button_label(main_ctx, NK_T("Exit")))
		{
			getGame()->setSignal(Game::SG_EXIT_GAME);
		}
	}
	nk_end(main_ctx);

	nk_tengine_render(this->getLayer(), NK_ANTI_ALIASING_ON);
}

void LevelHud::drawGameMenu(PauseButtonState pauseState)
{
	const f32 button_width = 50.0f;
	const f32 button_height = 50.0f;

	const f32 panel_width = 200.0f;
	const f32 panel_height = 60.0f;

	const f32 viewOffsetX = (f32)getViewOffsetX(getLayer());
	const f32 scale = (f32)getRenderPlaneScale(getLayer());
	const f32 viewWidth = (f32)getViewWidth(getLayer());

	// WITH THE POWER OF MATH!
	Vector pos = { viewOffsetX + viewWidth, getViewOffsetY(getLayer()) };
	const f32 panel_x = (f32)pos.x / scale - panel_width - 50.0f;
	const f32 panel_y = (f32)pos.y / scale + 50.0f;

	if (nk_begin(main_ctx, NK_T("MenuGame"), nk_rect(panel_x, panel_y, panel_width, panel_height), NK_WINDOW_NO_SCROLLBAR))
	{
		nk_size points = (nk_size)gameData()->getPoints();
		nk_size maxPoints = (nk_size)gameData()->getLevelPoints();

		nk_layout_row_dynamic(main_ctx, button_height, 4);

		nk_tchar pointsText[10];
		nk_itoa(pointsText, gameData()->getPoints());

		nk_tchar levelText[10];
		nk_itoa(levelText, gameData()->getCurrentLevel());

		nk_label_colored(main_ctx, levelText, NK_TEXT_ALIGN_CENTERED, nk_rgb(150, 80, 80));

		nk_label_colored(main_ctx, pointsText, NK_TEXT_ALIGN_CENTERED, nk_rgb(80, 150, 80));

		nk_progress(main_ctx, &points, maxPoints, NK_FIXED);

		switch (pauseState)
		{
		case PB_NONE:
			if (nk_button_label(main_ctx, NK_T("-")))
			{
				//TODO: disabled button
			}
			break;
		case PB_PAUSE:
			if (nk_button_label(main_ctx, NK_T("P")))
			{
				getGame()->setSignal(Game::SG_PAUSE_MENU);
			}
			break;
		case PB_RESUME:
			if (nk_button_label(main_ctx, NK_T("R")))
			{
				getGame()->setSignal(Game::SG_RESUME_GAME);
			}
			break;
		}
	
	}
	nk_end(main_ctx);

	nk_tengine_render(this->getLayer(), NK_ANTI_ALIASING_ON);

	//drawUnitInfo();
}

void LevelHud::drawNewGameButton()
{
	if (nk_button_label(main_ctx, NK_T("New Game")))
	{
		getGame()->setSignal(Game::SG_NEW_GAME);
	}
}

void LevelHud::drawPauseMenu()
{
	stateYes = Game::GS_MAIN_MENU;
	stateNo = Game::GS_PAUSE_MENU;

	MenuParam panel;
	init(panel);

	if (nk_begin(main_ctx, NK_T("MenuPause"), nk_rect(panel.x, panel.y, panel.width, panel.height), NK_WINDOW_NO_SCROLLBAR))
	{
		nk_layout_row_dynamic(main_ctx, panel.buttonHeight, 1);

		if (nk_button_label(main_ctx, NK_T("Main Menu")))
		{
			getGame()->setSignal(Game::SG_QUESTION_MENU);
		}

		if (nk_button_label(main_ctx, NK_T("Resume")))
		{
			getGame()->setSignal(Game::SG_RESUME_GAME);
		}
	}
	nk_end(main_ctx);

	nk_tengine_render(this->getLayer(), NK_ANTI_ALIASING_ON);
}

void LevelHud::drawStatusMenu()
{
	stateYes = Game::GS_MAIN_MENU;
	stateNo = Game::GS_STATUS_MENU;

	const GameData::LevelStatus status = gameData()->getLevelStatus();

	MenuParam panel;
	init(panel, status == GameData::LS_LOSE ? 3.0f : 4.0f);

	static const wchar_t textWin[30] = L"Congratulations!";
	static const wchar_t textNext[30] = L"Next";

	if (nk_begin(main_ctx, NK_T("MenuEndLevel"), nk_rect(panel.x, panel.y, panel.width, panel.height), NK_WINDOW_NO_SCROLLBAR))
	{
		nk_layout_row_dynamic(main_ctx, panel.buttonHeight, 1);

		switch (status)
		{
		case GameData::LS_LOSE:
		{
			nk_label(main_ctx, NK_T("Oh... Try again!"), NK_TEXT_ALIGN_CENTERED | NK_TEXT_ALIGN_MIDDLE);

			if (nk_button_label(main_ctx, NK_T("Restart")))
			{
				getGame()->setSignal(Game::SG_RESTART_LEVEL);
			}

			if (nk_button_label(main_ctx, NK_T("Main Menu")))
			{
				getGame()->setSignal(Game::SG_QUESTION_MENU);
			}
		}
		break;
		case GameData::LS_WIN:
		{
			nk_color congColor = nk_rgba(255, 255, 0, 255);

			nk_label_colored(main_ctx, NK_T("Congratulations!"), NK_TEXT_ALIGN_CENTERED | NK_TEXT_ALIGN_MIDDLE, congColor);

			nk_color scoreColor = nk_rgba(255, 0, 255, 255);

			Int score = gameData()->getCurrentData().levelPoints;

			nk_tchar scoreName[20] = L"Score: ";
			nk_tchar num[20];
			nk_itoa(num, score);
			Int st = nk_strlen(scoreName);
			Int nt = nk_strlen(num);
			for (Int j = 0; j < nt; ++j)
			{
				scoreName[st + j] = num[j];
			}

			nk_label_colored(main_ctx, scoreName, NK_TEXT_ALIGN_LEFT | NK_TEXT_ALIGN_MIDDLE, scoreColor);
			nk_label_colored(main_ctx, NK_T("Prize:"), NK_TEXT_ALIGN_LEFT | NK_TEXT_ALIGN_MIDDLE, scoreColor);

			nk_layout_row_dynamic(main_ctx, panel.buttonHeight, 2);

			if (nk_button_label(main_ctx, NK_T("Main Menu")))
			{
				getGame()->setSignal(Game::SG_QUESTION_MENU);
			}

			if (nk_button_label(main_ctx, NK_T("Next")))
			{
				getGame()->setSignal(Game::SG_NEXT_LEVEL);
			}
		}
		break;
		case GameData::LS_WIN_GAME:
		{
			nk_label(main_ctx, NK_T("You killed them all!"), NK_TEXT_ALIGN_CENTERED | NK_TEXT_ALIGN_MIDDLE);

			if (nk_button_label(main_ctx, NK_T("Main Menu")))
			{
				getGame()->setSignal(Game::SG_QUESTION_MENU);
			}

			drawNewGameButton();
		}
		break;
		default:
			SDK_ASSERT(false);
			break;
		}
	}
	nk_end(main_ctx);

	nk_tengine_render(this->getLayer(), NK_ANTI_ALIASING_ON);
}

void LevelHud::switchQ(Int stateQ)
{
	switch (stateQ)
	{
	case Game::GS_MAIN_MENU:
		getGame()->setSignal(Game::SG_MAIN_MENU);
		break;
	case Game::GS_PAUSE_MENU:
		getGame()->setSignal(Game::SG_PAUSE_MENU);
		break;
	case Game::GS_STATUS_MENU:
		getGame()->setSignal(Game::SG_STATUS_MENU);
		break;
	}
}

void LevelHud::drawQuestionMenu()
{
	MenuParam panel;
	init(panel, 2.0f, BUTTON_HEIGHT);

	if (nk_begin(main_ctx, NK_T("MenuEndLevel"), nk_rect(panel.x, panel.y, panel.width, panel.height), NK_WINDOW_NO_SCROLLBAR))
	{
		nk_layout_row_dynamic(main_ctx, panel.buttonHeight, 1);

		nk_label(main_ctx, NK_T("Are you sure?"), NK_TEXT_ALIGN_CENTERED | NK_TEXT_ALIGN_MIDDLE);

		nk_layout_row_dynamic(main_ctx, panel.buttonHeight, 2);
		if (nk_button_label(main_ctx, NK_T("Yes")))
		{
			switchQ(stateYes);
		}

		if (nk_button_label(main_ctx, NK_T("No")))
		{
			switchQ(stateNo);
		}

	}
	nk_end(main_ctx);

	nk_tengine_render(this->getLayer(), NK_ANTI_ALIASING_ON);
}

void LevelHud::updateInput(const TouchPadData& tpdata)
{
	if (nk_is_tengine_init() == FALSE)
	{
		return;
	}

	if (getState() != Game::GS_NONE)
	{
		nk_input_begin(main_ctx);
		{
			s32 x, y;
			x = fx2int(FX_Div(FX32(tpdata.point[0].mX), getRenderPlaneScale(this->getLayer())));
			y = fx2int(FX_Div(FX32(tpdata.point[0].mY), getRenderPlaneScale(this->getLayer())));
			nk_input_motion(main_ctx, x, y);
			if (tpdata.point[0].mTrg)
			{
				nk_input_button(main_ctx, NK_BUTTON_LEFT, x, y, 1);
			}
			if (tpdata.point[0].mRls)
			{
				nk_input_button(main_ctx, NK_BUTTON_LEFT, x, y, 0);
			}
		}
		nk_input_end(main_ctx);
	}
}

void LevelHud::updateState(Int ms)
{
	(void)ms;

	switch (getState())
	{
	case Game::GS_MAIN_MENU:
		drawMainMenu();
		break;
	case Game::GS_PAUSE_MENU:
		drawGameMenu(PB_RESUME);
		drawPauseMenu();
		break;
	case Game::GS_STATUS_MENU:
		drawGameMenu(PB_NONE);
		drawStatusMenu();
		break;
	case Game::GS_QUESTION_MENU:
		drawGameMenu(PB_NONE);
		drawQuestionMenu();
		break;
	case Game::GS_LEVEL:
		drawGameMenu(PB_PAUSE);
		break;
	}
}

void LevelHud::onStateEnter()
{
	if (getState() != Game::GS_NONE)
	{
		createHud();
	}
}

#pragma warning (pop)
