#ifndef LEVEL_ID_H_
#define LEVEL_ID_H_

 enum GameLayer
 {
	LAYER_GAME = 0,
	LAYER_HUD = 1,
	LAYER_LOADING = 2,
	
	LAYER_COUNT 
 };

 enum GameLevelInd
 {
	 GAMELEVEL_IND_GAME = 0,
	 GAMELEVEL_IND_HUD = 1,
	 GAMELEVEL_IND_LOADING = 2,

	 GL_COUNT
 };

#endif
