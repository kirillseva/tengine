#pragma once

#include "tengine.h"

typedef u8 Uchar;
typedef s32 Int;
typedef fx32 Float;
typedef fxVec2 Vector;
typedef fxMat22 Matrix;
typedef Float Rect[RECT_SIZE];
typedef GXRgba Color;

Int randInt(Int from, Int to);
Float randFloat(Float from, Float to);
