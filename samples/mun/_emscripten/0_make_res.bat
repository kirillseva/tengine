@echo off

rem ------------------------------
rem generate map (mapeditor) files
rem ------------------------------

set CURRENT_DIR=%~dp0
..\..\..\tools\editor\MapEditor3.exe -wchar_size4 -p%CURRENT_DIR%\..\mapeditor\mun_rider2.mpf -i%CURRENT_DIR%\..\mapeditor\ -o%CURRENT_DIR%\assets\data\
rem -l%CURRENT_DIR%\log.txt 

if NOT EXIST assets\data\o (
echo ----
echo Please generate map-files with MapEditor:
echo - open ..\mapeditor\*.mpf file with ..\..\..\tools\editor\MapEditor3.exe
echo - go to main menu [File/Save map] and browse to folder assets\data
echo ----
pause
goto END
)
@echo on

rem --------------------
rem convert graphics res 
rem --------------------

for %%i in (..\rawres\*.png) do ..\..\..\tools\bmpcvtr.exe %%i -5551 -2n -fnearest -oassets\data\

@echo off

copy ..\rawres\system.fnt assets\data\

copy ..\rawres\color.vsh assets\data\
copy ..\rawres\lit.vsh assets\data\
copy ..\rawres\unlit.vsh assets\data\
copy ..\rawres\color.fsh assets\data\
copy ..\rawres\lit.fsh assets\data\
copy ..\rawres\unlit.fsh assets\data\

if NOT EXIST assets\data\color.vsh (
echo ----
echo Please copy *.fsh and *.vsh files from tengine\src\_default_shaders to ..\rawres
echo ----
pause
goto END
)

:END