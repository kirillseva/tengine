
#include "system.h"
#include "tengine.h"
#include "gamepad.h"
#include "touchpad.h"
#include "gamefield.h"
#include "constants.h"
#include "fxmath.h"

//-------------------------------------------------------------------------------------------
 
static s32 gameState;
static u32 c_c;
static s32 gleft = 0;
static s32 gtop = 0;
static s32 gwidth = 0;
static s32 gheight = 0;
static s32 end_flag = 0;
static fx32 head_posx = 0;
static fx32 head_posy = 0;
static fx32 head_posx0 = 0;
static fx32 head_posy0 = 0;
static s32 robo_a_ct = 0;
static s32 robo_a_respawn_timer;
static s32 time_to_flash;
static s32 time_disable_controls;
static fx32 cursor_radius = 0;
static fx32 cursor_angle = 0;
static fx32 cursor_radius_max = 0;
static fx32 cursor_radius_min = 0;
static s32 current_ms = 0;
static fx32 mr_off_power;
static s32 robo_shoot_id;
static s32 robo_shoot_ct;
static s32 reflect_rocket;
static fx32 mdx, mdy;
static s32 robo_a_dcount;
static s32 robo_a_d[3];
static BOOL final_cutscene_2;

#ifdef USE_OPENGL_RENDER
#define DEFAULT_SCALE 2
#else
#define DEFAULT_SCALE 1
#endif

//-------------------------------------------------------------------------------------------

static const s32 pl_vertises[] =
{
	RECT_RIGHT_TOP_X, RECT_RIGHT_TOP_Y,
	RECT_RIGHT_TOP_X, RECT_RIGHT_BOTTOM_Y,
	RECT_LEFT_TOP_X,  RECT_LEFT_BOTTOM_Y
};

static fx32 cosmicLongLines[CL_MAXLONGLINES][CL_Count];
static fx32 cosmicShortLines[CL_MAXSHORTLINES][CL_Count];

static const s32 cosmicLongLinesData_Size[3] = {5, 4, 7};
static const s32 cosmicShortLinesData_Size[3] = {11, 7, 5};

//-------------------------------------------------------------------------------------------

static void onInit();
static void load();
static void reset();

static void onEngineEvent(const struct EventCallbackData *ipData);

static void onUpdateObject(u32 layer, s32 iId, BOOL* const oDraw);
static void onUpdateBackgroundTiles(u32 layer);
static void onFinishUpdate(u32 layer);
static void onRobotForEachObject(u32 layer, s32 iIdInitiator, s32 iId);

static void rocketCollideObj(s32 pr, s32 opr);
static void drawCosmicLinesBG();
static void drawPowerLines();
static void createCosmicLine(fx32* array, BOOL infinity);

static void onLevelMapLoad();
static void game_step(s32 ms);
static void keyProcessing();

static void rotateCursor(s32 cursor_id);
static void processMUNRocket();
static void startRocket();

static BOOL rayCastingX(s32 opr, fx32 footY, fx32 faceY, enum RectType typeRect);
static fx32 getDistanceX(s32 pr, s32 opr, enum RectType typeRect);

//-------------------------------------------------------------------------------------------

void tfgInitMemory()
{
}
//-------------------------------------------------------------------------------------------

void onInit()
{
	struct InitEngineParameters ep1;
	struct RenderPlaneInitParams mp1;	
	mp1.mBGType = BGSELECT_SUB3;
	mp1.mSizes.mViewWidth = REAL_SCREEN_WIDTH * DEFAULT_SCALE;
	mp1.mSizes.mViewHeight = REAL_SCREEN_HEIGHT * DEFAULT_SCALE;
#ifdef USE_OPENGL_RENDER
	mp1.mMaxRenderObjectsOnPlane = 2000; // max objs estimation on scene
#endif
#ifdef USE_CUSTOM_RENDER
	mp1.mColorType = BMP_TYPE_DC16;
	{
		const s32 h8 = mp1.mSizes.mViewHeight; 
		s32 w8 = mp1.mSizes.mViewWidth;
  #ifdef FRAME_ALLOCATOR
		// when the screen orientation changes, it is necessary to re-create a buffer (resize)
		// if defined FRAME_ALLOCATOR, we cannot recreate new buffer for easygraphics module because of assertion)
		// so the buffer must be the same for the larger size of the screen
		w8 = (w8 > h8) ? w8 : h8; 
		mp1.mSizes.mFrameBufferWidth8 = (u16)w8;
		mp1.mSizes.mFrameBufferHeight8 = (u16)w8;
  #else //FRAME_ALLOCATOR
		mp1.mSizes.mFrameBufferWidth8 = w8;
		mp1.mSizes.mFrameBufferHeight8 = h8;
  #endif //FRAME_ALLOCATOR
	}
#endif
	mp1.mX = 0;
	mp1.mY = 0;

	ep1.layersCount = LAYER_COUNT;
	ep1.particlesPoolSize = MAX_PARTICLES;
	initEngine(&ep1);
	initRenderPlane(&mp1);

	assignLayerWithRenderPlane(GAME_LAYER, BGSELECT_SUB3, TRUE);
	//assignLayerWithRenderPlane(HUD_LAYER, BGSELECT_SUB3, FALSE);

#ifdef USE_OPENGL_RENDER
	setRenderPlaneScale(FX32(DEFAULT_SCALE), GAME_LAYER);
#endif

	//register callbacks
	setOnEvent(onEngineEvent);
	setOnDrawGameObject(onUpdateObject);
	setOnDrawBackgroundTiles(onUpdateBackgroundTiles);
	setOnFinishUpdate(onFinishUpdate);

	//begin load task
	//parameter will return with iData.initiatorId in onEngineEvent() function (EVENT_TYPE_ON_ENDASYNHLOADDATA)
	beginLoadListAsynh(111);
	{
		//load common data
		addToLoadListCommonData();
		//load text data for certain language
		// to avoid memory fragmentation load it once after common data
		addToLoadListLanguageData(LANGUAGE_DEFAULT);
	}
	//end load task
	endLoadListAsynh(FALSE);
	
	//wait for EVENT_TYPE_ON_ENDASYNHLOADDATA in onEngineEvent() function
	gameState = gstNONE;
}
//-------------------------------------------------------------------------------------------

void tfgInit()
{	
	InitGamePad();
	gameState = gstNONE; 
}
//-------------------------------------------------------------------------------------------

void tfgRelease()
{
	// to avoid memory fragmentation, releaseMapData in opposite side
	releaseResources();
	releaseMapData(GAME_LAYER);
	releaseTextData();
	releaseCommonData();
	releaseRenderPlane(BGSELECT_SUB3);
	releaseEngine();
}
//-------------------------------------------------------------------------------------------

void tfgSetWindowModeSize(s32 *const w, s32 *const h)
{
	*w = REAL_SCREEN_WIDTH * DEFAULT_SCALE;
	*h = REAL_SCREEN_HEIGHT * DEFAULT_SCALE;
}
//-------------------------------------------------------------------------------------------

void tfgResize(s32 w, s32 h)
{
	(void)w;
	(void)h;
	gameState = gstINIT_ENGINE; 
}
//-------------------------------------------------------------------------------------------

void tfgLostRenderDevice()
{
}
//-------------------------------------------------------------------------------------------

void tfgRestoreRenderDevice()
{
}
//------------------------------------------------------------------------------------

void tfgLowMemory()
{
}
//------------------------------------------------------------------------------------

void reset()
{
	// to avoid memory fragmentation, releaseMapData in opposite side
	releaseResources();
	releaseMapData(GAME_LAYER);
	gameState = gstLOADING;
}
//-------------------------------------------------------------------------------------------

void load()
{
    s32 i;
	time_disable_controls = 0; 
	robo_a_respawn_timer = 0;
	c_c = 0;

    //OS_Printf("before free newGame = %d\n", GetFreeMemorySize());
#ifdef NITRO_SDK
	GX_SetVisiblePlane(GX_PLANEMASK_NONE);
#endif
	
	for(i = 0; i < CL_MAXLONGLINES; i++)
	{
		MI_CpuClear8(cosmicLongLines[i], CL_Count * sizeof(fx32)); 
	}
	for(i = 0; i < CL_MAXSHORTLINES; i++)
	{
		MI_CpuClear8(cosmicShortLines[i], CL_Count * sizeof(fx32)); 
	}
	
	// to avoid memory fragmentation, releaseMapData in opposite side

	releaseResources();

	releaseMapData(GAME_LAYER);

	OS_Printf("mem on newGame = %u\n", GetFreeMemorySize());

	beginLoadListAsynh(222);
	{
		addToLoadListMap(GAME_LAYER, 0);
	}
	endLoadListAsynh(TRUE);
	
	//wait for EVENT_TYPE_ON_ENDASYNHLOADDATA and EVENT_TYPE_ON_ENDASYNHLOADGRAPHICS in onEngineEvent() function
	gameState = gstNONE;

}
//-------------------------------------------------------------------------------------------

void onLevelMapLoad()
{
	fx32 pr_rect[RECT_SIZE];
	struct fxVec2 pos;

	resetParticles();

	gwidth = getViewWidth(GAME_LAYER);
	gheight = getViewHeight(GAME_LAYER);
	gleft = 0;
	gtop = HEADER;

	c_c = 0;

#ifdef NITRO_SDK
    GXS_SetVisiblePlane(GX_PLANEMASK_BG3);
#endif

	prGetCollideRect(M0_MUN_HEAD, pr_rect);
	pos = prGetPosition(M0_MUN_HEAD);
	head_posy0 = head_posy = pos.y;
	head_posx0 = head_posx = pos.x;

	cursor_radius = cursor_radius_min = fxMul((pr_rect[RECT_LEFT_BOTTOM_Y] - pr_rect[RECT_LEFT_TOP_Y]), FX32(3));
    cursor_radius_max = head_posx0 - fxDiv((pr_rect[RECT_LEFT_BOTTOM_Y] - pr_rect[RECT_LEFT_TOP_Y]), FX32(2)) - getViewOffsetX(GAME_LAYER);
    cursor_angle = MUNRIDER_CURSOR_ANGLE_INIT_FX;
    time_to_flash = -1;
    rotateCursor(M0_TARGET);
    robo_a_ct = MUNRIDER_MAX_ROBOT_A;
    end_flag = 0;
	reflect_rocket = 0;
	robo_shoot_ct = -1;
	final_cutscene_2 = FALSE;
	gameState = gstGAME;
    
	time_disable_controls = 1; 

    OS_Printf("on level begin mem = %u\n", GetFreeMemorySize());
}
//------------------------------------------------------------------------------------

void tfgTick(s32 ms)
{
	c_c++;
	//UpdateSoundSystem();  

	switch(gameState)
	{
		case gstNONE:
		break;
	
		case gstINIT_ENGINE:
		{
			onInit();
		}
		break;

		case gstLOADING:
			load();
		break;

		case gstGAME:
			game_step(ms);
		break;

		case gstRESET: 
			reset();
	}
}
//-------------------------------------------------------------------------------------------

void keyProcessing()
{	
#ifdef NITRO_SDK	
	//lotcheck
	if(IsButtonPress(TYPE_L) && IsButtonPress(TYPE_R) && IsButtonPress(TYPE_SELECT) && IsButtonPress(TYPE_START))
	{
		OS_ResetSystem(0);
	}
#endif
}
//----------------------------------------------------------------------------------

void game_step(s32 ms)
{
    current_ms = ms; 
	time_disable_controls -= ms;
	if(time_disable_controls <= 0)
    {
		time_disable_controls = -1;
    }
}
//----------------------------------------------------------------------------------

void onUpdateBackgroundTiles(u32 layer)
{
	(void)layer;
	const fx32 dtscale = getCurrentDeltaTimeScale();

	UpdateGamePad();
	keyProcessing();

	if(time_disable_controls < 0)
	{
		if(IsButtonPress(TYPE_LEFT))
		{
			cursor_radius += FX_Mul(CURSOR_STEP_FX, dtscale);
			if(cursor_radius >= cursor_radius_max)
				cursor_radius = cursor_radius_max;
			rotateCursor(M0_TARGET);
		}
		else
		if(IsButtonPress(TYPE_RIGHT))
		{
			cursor_radius -= FX_Mul(CURSOR_STEP_FX, dtscale);
			if(cursor_radius <= cursor_radius_min)
				cursor_radius = cursor_radius_min;
			rotateCursor(M0_TARGET);
		}

		if(IsButtonPress(TYPE_UP))
		{
			cursor_angle += FX_Mul(CURSOR_STEP_FX, dtscale);
			if(cursor_angle >= MUNRIDER_CURSOR_ANGLE_MAX_FX)
				cursor_angle = MUNRIDER_CURSOR_ANGLE_MAX_FX;
			rotateCursor(M0_TARGET);
		}
		else
		if(IsButtonPress(TYPE_DOWN))
		{
			cursor_angle -= FX_Mul(CURSOR_STEP_FX, dtscale);
			if(cursor_angle <= MUNRIDER_CURSOR_ANGLE_MIN_FX)
				cursor_angle = MUNRIDER_CURSOR_ANGLE_MIN_FX;
			rotateCursor(M0_TARGET);
		}

		if(IsButtonDown(TYPE_A))
		{
			startRocket();
		}
	}

	if(IsButtonDown(TYPE_START))
	{
		gameState = gstRESET;
	}

	if(final_cutscene_2 == FALSE)
	{
		drawCosmicLinesBG();
		processMUNRocket();

		if(prGetProperty(M0_MUN_HEAD, PRP_SPEED) == 0)
		{
			fx32 a_rad;
			struct fxVec2 rect;
			rect = prGetPosition(M0_MUN_HEAD);
			if(end_flag == 1) // go baby to boss and finish him!
			{
				prAddXYToPosition(M0_MUN_HEAD, head_posx0 - rect.x, head_posy0 - rect.y); //set MUN to base position to synh with girl animation 
				rect.x = head_posx0;
				rect.y = head_posy0;
				end_flag = 0;
			}
			a_rad = fxMul(cursor_angle, FX_PI) / 180;
			mdx = head_posx0 + FX32(MUNRIDER_ARC_RADIUS) + (MUNRIDER_ARC_RADIUS * fxCos(a_rad)) - rect.x;
			mdy = head_posy0 + FX32(MUNRIDER_ARC_RADIUS) + (MUNRIDER_ARC_RADIUS * fxSin(a_rad)) - rect.y;
		}
		else
		{
			mdx = mdy = 0;
			time_disable_controls = MUN_DISABLE_CONTOLS_FINAL;
			prSetProperty(M0_TARGET, PRP_ENABLE, 0);
		}

		if(getCurrentZone(GAME_LAYER) == 0) // robo_a zone
		{
			robo_a_dcount = 0;
			if(prGetProperty(M0_ROBO_1, PRP_ENABLE) == 0)
			{
				robo_a_d[robo_a_dcount++] = M0_ROBO_1;
			}
			if(prGetProperty(M0_ROBO_2, PRP_ENABLE) == 0)
			{
				robo_a_d[robo_a_dcount++] = M0_ROBO_2;
			}
			if(prGetProperty(M0_ROBO_3, PRP_ENABLE) == 0)
			{
				robo_a_d[robo_a_dcount++] = M0_ROBO_3;
			}
		}
	}
}
//----------------------------------------------------------------------------------

void onUpdateObject(u32 layer, s32 pr, BOOL* const oDraw)
{
	s32 j, gr_id;
    fx32 pr_top, pr_bottom, distance;
    struct fxVec2 f_rect;
    fx32 l_rect[RECT_SIZE];
	BOOL i_see_you;
	(void)oDraw;
	(void)layer;

	gr_id = prGetGroupIdx(pr);

	switch(gr_id)
	{
        case KURSOR_BONUS:
			/*
			if(time_disable_controls > 0)
			{
				return FALSE;
			}
			*/
        break;

        case MUN:
			prAddXYToPosition(pr, mdx, mdy);
			f_rect = prGetPosition(pr);
			head_posy = f_rect.y;
			head_posx = f_rect.x;
			if(robo_shoot_id > 0)
			{
				time_disable_controls = MUN_DISABLE_CONTOLS_ROBO_TIME;
				prSetProperty(M0_TARGET, PRP_ENABLE, 0);
				prSetProperty(M0_ROCKET, PRP_ENABLE, 0);
				f_rect = prGetPosition(robo_shoot_id);
				for(j = 0; j < 3; j++)
				{
					setColor(powerLinesData[0][j]);
					drawLine(f_rect.x - FX32(j),
								f_rect.y,
								head_posx - FX32(j),
								head_posy);
				}
				robo_shoot_ct -= current_ms;

				prGetCollideRect(pr, l_rect);
				createSimpleParticles(1, COLOR_TO_1555(0x0002fafd),
										head_posx, l_rect[RECT_LEFT_TOP_Y],
										fx2int(l_rect[RECT_LEFT_BOTTOM_Y] - l_rect[RECT_LEFT_TOP_Y]),
										1, FX32(5.0f), FX32(8.0f), 100, 400, 400, NULL, NULL);

				if(robo_shoot_ct < 0)
				{
					robo_shoot_ct = -1;
					robo_shoot_id = -1;
				}
			}
			else if(time_disable_controls < 0)
			{
				s32 state;
				prSetProperty(M0_TARGET, PRP_ENABLE, 1);
				drawPowerLines();
				state = prGetState(pr);
				if(state == LOOK_UP || state == LOOK_STR || state == LOOK_DOWN ||
							state == TRANSFORM || state == LOOK_UP_DIAGONAL || state == LOOK_DOWN_DIAGONAL)
				{
					if (cursor_angle >= MUNRIDER_CURSOR_ANGLE_UP_FX)
					{
						prSetState(pr, LOOK_UP, SBM_NONE);
					}
					else if(cursor_angle >= MUNRIDER_CURSOR_ANGLE_DIAUP_FX)
					{
						prSetState(pr, LOOK_UP_DIAGONAL, SBM_NONE);
					}
					else if(cursor_angle <= MUNRIDER_CURSOR_ANGLE_DOWN_FX)
					{
						prSetState(pr, LOOK_DOWN, SBM_NONE);
					}
					else if(cursor_angle <= MUNRIDER_CURSOR_ANGLE_DIADOWN_FX)
					{
						prSetState(pr, LOOK_DOWN_DIAGONAL, SBM_NONE);
					}
					else
					{
						prSetState(pr, LOOK_STR, SBM_NONE);
					}
				}
			}
        break;
          
        case BOSS_LINZA:
			prGetViewRect(pr, l_rect);
			pr_top = l_rect[RECT_LEFT_TOP_Y];
			pr_bottom = l_rect[RECT_LEFT_BOTTOM_Y];
			i_see_you = rayCastingX(M0_ROCKET, pr_bottom, pr_top, RECTTYPE_COLLIDE);
			distance = getDistanceX(pr, M0_ROCKET, RECTTYPE_VIEW);
			if(i_see_you && distance == 0 && prGetProperty(M0_ROCKET, PRP_ENABLE) != 0 && reflect_rocket == 0)
			{
				reflect_rocket = BOSS_REFLECT_ROCKET_TICKS;
			}
			if(prGetState(pr) == SHOOT_R)
			{
				prSetProperty(M0_ROCKET, PRP_ENABLE, 0);
				prSetProperty(M0_TARGET, PRP_ENABLE, 0);
				time_disable_controls = MUN_DISABLE_CONTOLS_ROBO_TIME * 5;
				f_rect = prGetPosition(pr);
				prGetCollideRect(M0_MUN_HEAD, l_rect);

				createSimpleParticles(	1,
										COLOR_TO_1555(0x0002fafd),
										head_posx, l_rect[RECT_LEFT_TOP_Y], 
										fx2int(l_rect[RECT_LEFT_BOTTOM_Y] - l_rect[RECT_LEFT_TOP_Y]),
										10,
										FX32(0.8f), // minSpeed
										FX32(1.0f), // varSpeed
										400, // minLifeTime
										100, // varLifeTime
										30, // exploudTime
										&f_rect.x, &f_rect.y);

				if(prGetState(M0_MUN_HEAD) != TRANSFORM)
				{
					prSetState(M0_MUN_HEAD, TRANSFORM, SBM_NONE);
				}
				if(time_to_flash > 0)
				{
					if(time_to_flash == MUNRIDER_TIME_TO_FLASH)
					{
						startScript(M0_SCR_BEGIN_BOSS_BLIND, TRUE, -1, -1);
						prSetState(M0_MUN_HEAD, LOOK_DOWN, SBM_NONE);
					}
					if(time_to_flash < 5 && time_to_flash >= 0)
					{
						setColor(MUNRIDER_FLASH_COLOR);
						fillRect(FX32(gleft) + getViewOffsetX(GAME_LAYER), FX32(gtop) + getViewOffsetY(GAME_LAYER), FX32(gwidth), FX32(gheight));
					}
					--time_to_flash; 
					if(time_to_flash == 0)
					{
						time_to_flash = -1; //disable flash till to first boss node-director
					}
				}
				else if(IsButtonDown(TYPE_A) && time_to_flash == 0)
				{
					time_to_flash = MUNRIDER_TIME_TO_FLASH;
				}
			}
        break;

        case BOSS_BACK:
			//if(prGetProperty(M0_BOSS_LINZA, PRP_ENABLE) != 0)
			{
		  		f_rect.x = MUNRIDER_PARTICLES_MAX_X_FX;
				distance = c_c % MUNRIDER_BOSS_BACK_EMIT_PARTICLES_TICKS;
				if(prGetProperty(M0_BOSS_LINZA, PRP_LIFE) <= FX32(20) && distance == 0)
				{
					createParticles(M0_BOSS_SPINOMOZG, 6, 6, FX32(1.5f), FX32(2.0f), 200, 500, 80, &f_rect.x, NULL);
				}
				if(prGetProperty(M0_BOSS_LINZA, PRP_LIFE) <= FX32(10) && distance == 0)
				{
					createParticles(M0_BOSS_SPINOMOZG, 4, 4, FX32(2.0f), FX32(1.0f), 300, 600, 80, &f_rect.x, NULL);
				}
				if(prGetProperty(M0_BOSS_LINZA, PRP_LIFE) <= 0)
				{
					if(distance == 0)
					{
						createParticles(M0_BOSS_SPINOMOZG, 6, 6, FX32(2.5f), FX32(5.0f), 300, 800, 80, &f_rect.x, NULL);
						createParticles(M0_BOSS_SPINOMOZG, 5, 5, FX32(1.5f), FX32(3.0f), 300, 800, 180, &f_rect.x, NULL);
					}
					if(prGetState(M0_BOSS_LINZA) != MOVE_RIGHT)
					{
						prSetState(M0_BOSS_LINZA, MOVE_RIGHT, SBM_NONE); // force set MOVE_RIGHT state, boss must go to MUN for final scene
					}
					if(prGetProperty(M0_BOSS_LINZA, PRP_SPEED) == 0)
					{
						startScript(M0_SPT_BOSS_ACTIVATOR, TRUE, -1, -1); // force reinit boss then boss get damage on boss_stop script
					}
				}
			}
        case ROBOT_B:
        case ROBOT1:
			if(gr_id == ROBOT1 && prGetProperty(pr, PRP_PAUSE) != 0)
			{
				s32 pause = (s32)prGetProperty(pr, PRP_PAUSE) - current_ms;
				if(pause <= 0)
				{
					prSetProperty(pr, PRP_SPEED, prGetProperty(pr, PRP_AGGRESIV));
					prSetProperty(pr, PRP_PAUSE, 0);
				}
				else
				{
					prSetProperty(pr, PRP_PAUSE, (fx32)pause);
				}
			}
			callForEachGameObject(pr, onRobotForEachObject);
	}
}
//----------------------------------------------------------------------------------

static void onRobotForEachObject(u32 layer, s32 iIdInitiator, s32 iId)
{
    fx32 l_rect[RECT_SIZE];
    fx32 pr_top, pr_bottom;
	fx32 distance;
	BOOL i_see_you;
	struct fxVec2 pos;
	(void)layer;

	if(prGetProperty(iId, PRP_ENABLE) == 0 || iIdInitiator == iId)
	{
		return;
	}

	prGetCollideRect(iIdInitiator, l_rect);
	pr_top = l_rect[RECT_RIGHT_TOP_Y];
	pr_bottom = l_rect[RECT_RIGHT_BOTTOM_Y];

	i_see_you = rayCastingX(iId, pr_bottom, pr_top, RECTTYPE_COLLIDE);
	distance = getDistanceX(iIdInitiator, iId, RECTTYPE_COLLIDE);
	
	if(i_see_you && distance == 0)
	{
		switch(prGetGroupIdx(iId))
		{
			case KABOOM:
				switch(prGetGroupIdx(iIdInitiator))
				{
					case ROBOT1:
						rocketCollideObj(iIdInitiator, iId);
						prRunAssignedScript(iIdInitiator, SPT_ONCHANGENODE); //respawn
						if(robo_shoot_id == iIdInitiator)
						{
							robo_shoot_id = -1;
						}
						if(--robo_a_ct == 0)
						{
							startScript(M0_SCR_SET_ZONE_3, TRUE, -1, -1);
							end_flag = 1;
						}
					break;
								
					case ROBOT_B:
						rocketCollideObj(iIdInitiator, iId);
						startScript(M0_SCR_SET_BOSS_STATE_AGRESSIVE, TRUE, -1, -1);
					break;

					case BOSS_BACK:
						rocketCollideObj(M0_BOSS_SPINOMOZG, iId);
						if(prGetProperty(M0_BOSS_LINZA, PRP_LIFE) <= 0)
						{
							startScript(M0_SCR_SET_FINAL_ZONE4, TRUE, -1, -1);
							time_disable_controls = MUN_DISABLE_CONTOLS_FINAL;
							prSetProperty(M0_TARGET, PRP_ENABLE, 0);
						}
				}
			break;
						
			case MUN:
				prSetProperty(M0_TARGET, PRP_ENABLE, 0);
				prSetProperty(M0_ROCKET, PRP_ENABLE, 0);
				pos.x = head_posx;
				pos.y = l_rect[RECT_LEFT_TOP_Y];
				prSetPosition(M0_ROCKET, pos);
				rocketCollideObj(iIdInitiator, iIdInitiator);
				onCollide(iIdInitiator, iId);
				time_disable_controls = MUN_DISABLE_CONTOLS_ROBO_TIME * 5;
				prGetCollideRect(iId, l_rect);
				createSimpleParticles(1, COLOR_TO_1555(0x0002fafd),
										head_posx, l_rect[RECT_LEFT_TOP_Y],
										fx2int(l_rect[RECT_LEFT_BOTTOM_Y] - l_rect[RECT_LEFT_TOP_Y]),
																50, FX32(5.0f), FX32(8.0f), 400, 100, 150, NULL, NULL);
				prSetState(iId, LOOK_DOWN, SBM_NONE);
		}
	}
}
//----------------------------------------------------------------------------------

void onFinishUpdate(u32 layer)
{
	(void)layer;
	if(getCurrentZone(GAME_LAYER) == 0) // robo_a zone
	{
		if(robo_a_dcount >= 2 && robo_a_dcount > 3 - robo_a_ct)
		{
			robo_a_respawn_timer -= current_ms;
			if(robo_a_respawn_timer < 0 && mthGetRandom(MUNRIDER_RND_RESPAWN_ROBOT_A) == 1)
			{
				robo_a_respawn_timer = MUNRIDER_RESPAWN_TIME;
				prSetProperty(robo_a_d[mthGetRandom((u32)robo_a_dcount)], PRP_ENABLE, 1);
				prSetProperty(robo_a_d[mthGetRandom((u32)robo_a_dcount)], PRP_PAUSE, 0);
			}
		}
	}
	current_ms = 0;
}
//----------------------------------------------------------------------------------

void rotateCursor(s32 cursor_id)
{
	fx32 x, y, a_rad;
	struct fxVec2 rect;
    rect = prGetPosition(cursor_id);
	a_rad = fxMul(cursor_angle, FX_PI) / 180;
    x = head_posx + (fxMul(cursor_radius, fxCos(a_rad)));
    y = head_posy + (fxMul(cursor_radius, fxSin(a_rad)));
    prAddXYToPosition(cursor_id,  x - rect.x, y - rect.y);
}
//----------------------------------------------------------------------------------

BOOL rayCastingX(s32 opr, fx32 footY, fx32 faceY, enum RectType typeRect)
{
	fx32 rect[RECT_SIZE];
	switch(typeRect)
	{
		case RECTTYPE_COLLIDE:
			prGetCollideRect(opr, rect);
		break;
		case RECTTYPE_VIEW:
			prGetViewRect(opr, rect);
	}	
	return(footY >= rect[RECT_LEFT_TOP_Y] && faceY <= rect[RECT_LEFT_BOTTOM_Y]);
}
//----------------------------------------------------------------------------------

fx32 getDistanceX(s32 pr, s32 opr, enum RectType typeRect)
{
	fx32 prect[RECT_SIZE];
	fx32 orect[RECT_SIZE];
	switch(typeRect)
	{
		case RECTTYPE_COLLIDE:
			prGetCollideRect(pr, prect);
			prGetCollideRect(opr, orect);
		break;
		case RECTTYPE_VIEW:
			prGetViewRect(pr, prect);
			prGetViewRect(opr, orect);
	}
	if(prect[RECT_RIGHT_TOP_X] < orect[RECT_LEFT_TOP_X])
	{
		return orect[RECT_LEFT_TOP_X] - prect[RECT_RIGHT_TOP_X];
	}
	if(prect[RECT_LEFT_TOP_X] > orect[RECT_RIGHT_TOP_X])
	{
		return prect[RECT_LEFT_TOP_X] - orect[RECT_RIGHT_TOP_X];
	}
	return 0;
}
//----------------------------------------------------------------------------------

void rocketCollideObj(s32 pr, s32 opr)
{	
	s32 i;
	struct fxVec2 pos;
	fx32 x, y;
	x = MUNRIDER_PARTICLES_MAX_X_FX;
	pos = prGetPosition(pr);
	y = pos.y;
	if(opr > 0)
	{
		prSetProperty(opr, PRP_ENABLE, 0);
		for(i = 0; i < 2; i++)
		{
			createParticles(pr, 2, 3, FX32(2.0f), FX32(2.2f), 300, 220, 120, &x, &y);
			createParticles(pr, 2, 3, FX32(2.0f), FX32(2.2f), 600, 220, 120, &x, &y);
			createParticles(pr, 4, 4, FX32(3.0f), FX32(2.2f), 400, 320, 220, &x, &y);
			createParticles(pr, 4, 4, FX32(5.0f), FX32(2.2f), 400, 320, 220, &x, &y);
		}
		onCollide(pr, opr);
	}
	for(i = 0; i < 5; i++)
	{
		createParticles(M0_ROCKET, 4, 4, FX32(1.0f), FX32(1.0f), 400, 100, 0, NULL, NULL);
		createParticles(M0_ROCKET, 4, 4, FX32(1.0f), FX32(1.0f), 400, 100, 0, NULL, NULL);
		createParticles(M0_ROCKET, 4, 4, FX32(1.0f), FX32(1.0f), 400, 100, 0, NULL, NULL);
		createParticles(M0_ROCKET, 2, 2, FX32(1.0f), FX32(1.0f), 800, 100, 0, NULL, NULL);
		createParticles(M0_ROCKET, 2, 2, FX32(0.3f), FX32(3.0f), 500, 200, 0, NULL, NULL);
		createParticles(M0_ROCKET, 4, 4, FX32(0.3f), FX32(3.0f), 800, 100, 0, NULL, NULL);
	}
}
//----------------------------------------------------------------------------------

void drawCosmicLinesBG()
{
	fx32 *l;
	s32 len, idx, i, j;
	fx32 x1, x2, y, side2, dtscale;
	const fx32 ept_fx = FX32(gleft + gwidth - 1);
	dtscale = getCurrentDeltaTimeScale();

	for(i = 0; i < CL_MAXSHORTLINES; i++)
	{
		l = cosmicShortLines[i];
		l[CL_FXBEGIN] += FX_Mul(l[CL_FSPEED], dtscale);
		l[CL_FXEND] += FX_Mul(l[CL_FSPEED], dtscale);
		if(l[CL_FXBEGIN] >= ept_fx)
		{
			l[CL_FWIDTH] = 0;
		}
		if(fx2int(l[CL_FWIDTH]) == 0)
		{
			if(mthGetRandom(CL_MAXEMMITERRND) == 0)
			{
				createCosmicLine(l, FALSE);
			}
			else
			{
				continue;
			}
		}
		idx = fx2int(l[CL_FTYPEIDX]);
		len = cosmicShortLinesData_Size[idx];
		side2 = l[CL_FWIDTH] / len;
		y = l[CL_FY] + getViewOffsetY(GAME_LAYER);
		for(j = 0; j < len; j++)
		{
			setColor(cosmicShortLinesData[idx][j]);
			x1 = l[CL_FXBEGIN] + side2 * j + getViewOffsetX(GAME_LAYER);
			x2 = l[CL_FXEND] - side2 * j + getViewOffsetX(GAME_LAYER); 
			drawLine(x1, y, x2, y);
		}
	}

	//longlines
	for(i = 0; i < CL_MAXLONGLINES; i++)
	{
		l = cosmicLongLines[i];
		l[CL_FINFBUFFER] -= FX_Mul(FX32_ONE, dtscale);
		if(l[CL_FINFBUFFER] <= 0)
		{
			l[CL_FSIGN] = mthGetRandom(2) == 0 ? FX32_ONE : -FX32_ONE;
			l[CL_FINFBUFFER] = FX32(mthGetRandom(CL_MAXLONPATH));
			l[CL_FTYPEIDX] = FX32(mthGetRandom(CL_MAXLONGTYPES));
		}
		l[CL_FY] += FX_Mul(FX_Mul(l[CL_FSIGN], l[CL_FSPEED]), dtscale);
		if(l[CL_FY] < FX32(gtop - CL_MAXOVERBOUNDS))
		{
			l[CL_FY] = FX32(gtop - CL_MAXOVERBOUNDS);
		}
		else if(l[CL_FY] > FX32(gtop + gheight + CL_MAXOVERBOUNDS))
		{
			l[CL_FY] = FX32(gtop + gheight + CL_MAXOVERBOUNDS);
		}
		if(fx2int(l[CL_FWIDTH]) == 0)
		{
			createCosmicLine(l, TRUE);
		}
		idx = fx2int(l[CL_FTYPEIDX]);
		len = cosmicLongLinesData_Size[idx];
		side2 = FX32(len / 2 - 2);
		for(j = 0; j < len; j++)
		{
			setColor(cosmicLongLinesData[idx][j]);
			x1 = l[CL_FXBEGIN] + getViewOffsetX(GAME_LAYER); 
			x2 = l[CL_FXEND] + getViewOffsetX(GAME_LAYER);
			y = l[CL_FY] - side2 + FX32(j) + getViewOffsetY(GAME_LAYER);
			drawLine(x1, y, x2, y);
			y = l[CL_FY] + side2 - FX32(j) + getViewOffsetY(GAME_LAYER);
			drawLine(x1, y, x2, y);
		}
	}
}
//----------------------------------------------------------------------------------

void createCosmicLine(fx32* arr, BOOL infinity)
{
	arr[CL_FXEND] = arr[CL_FXBEGIN] = FX32(gleft);
	arr[CL_FY] = FX32(gtop - CL_MAXOVERBOUNDS + (s32)mthGetRandom((u32)gheight));
	if(infinity)
	{
		arr[CL_FTYPEIDX] = FX32(mthGetRandom((u32)CL_MAXLONGTYPES));
		arr[CL_FWIDTH] = arr[CL_FXEND] = FX32(gleft + gwidth);
		arr[CL_FSPEED] = FX32(CL_MINLONGSPEED + (s32)mthGetRandom(CL_MAXLONGSPEED - CL_MINLONGSPEED));
		arr[CL_FSIGN] = FX32_ONE;
	}
	else
	{
		arr[CL_FWIDTH] = FX32(CL_MINSHORTHALFWIDTH + (s32)mthGetRandom(CL_MAXSHORTHALFWIDTH - CL_MINSHORTHALFWIDTH));
		arr[CL_FSPEED] = FX32(CL_MINSHORTSPEED + (s32)mthGetRandom(CL_MAXSHORTSPEED - CL_MINSHORTSPEED));
		arr[CL_FXBEGIN] = arr[CL_FXEND] - arr[CL_FWIDTH] * 2;
		arr[CL_FTYPEIDX] = FX32(mthGetRandom(CL_MAXSHORTTYPES));
	}
}
//----------------------------------------------------------------------------------

void drawPowerLines()
{	
	struct fxVec2 pos, prev, end;
	fx32 pr_rect[RECT_SIZE];
	fx32 t, l, r, b, lr2, tb2;
	s32 i, v, c;
	i = PL_MAXLINES;
	v = 0;
	prGetCollideRect(M0_TARGET, pr_rect);
	while(--i >= 0)
	{		
		t = pr_rect[RECT_LEFT_TOP_Y] + FX_Mul(mthGetRandomFx(PL_OFFCIRCLE_FX), mthGetRandom(2) == 0 ? FX32_ONE : -FX32_ONE);
		l = pr_rect[RECT_LEFT_TOP_X] + FX_Mul(mthGetRandomFx(PL_OFFCIRCLE_FX), mthGetRandom(2) == 0 ? FX32_ONE : -FX32_ONE);
		r = pr_rect[RECT_RIGHT_TOP_X] + FX_Mul(mthGetRandomFx(PL_OFFCIRCLE_FX), mthGetRandom(2) == 0 ? FX32_ONE : -FX32_ONE);
		b = pr_rect[RECT_RIGHT_BOTTOM_Y] + FX_Mul(mthGetRandomFx(PL_OFFCIRCLE_FX), mthGetRandom(2) == 0 ? FX32_ONE : -FX32_ONE);
		lr2 = FX_Div(l + r, FX32(2));
		tb2 = FX_Div(t + b, FX32(2));
		prev = pos = prGetPosition(M0_KAPLYA_25);
		end.x = pr_rect[pl_vertises[v]];
		end.y = pr_rect[pl_vertises[v + 1]];
		c = 0;
		while(nextPointAtLine(prev, end, FX32(PL_PART_LENGTH), &pos))
		{
			if(++c == PL_PART_LENGTH)
			{
				c = PL_PART_LENGTH - 1;
			}
			pos.x += FX_Mul(mthGetRandomFx(FX32(PL_RNDSEEDX)), mthGetRandom(2) == 0 ? FX32_ONE : -FX32_ONE);
			pos.y += FX_Mul(mthGetRandomFx(FX32(PL_RNDSEEDY)), mthGetRandom(2) == 0 ? FX32_ONE : -FX32_ONE);
			setColor(powerLinesData[i % 2][c]);
			drawLine(prev.x, prev.y, pos.x, pos.y);
			prev = pos;
		}
		setColor(powerLinesData[i%2][c]);
		drawLine(prev.x, prev.y, end.x, end.y);
		if((v += 2) == 6)
		{
			v = 0;
		}
		setColor(powerLinesData[0][mthGetRandom(PL_PART_LENGTH)]);
		drawLine(l, t, lr2, t - PL_OFFCIRCLE_FX);
		drawLine(lr2, t - PL_OFFCIRCLE_FX, r, t);
		drawLine(r, t, r + PL_OFFCIRCLE_FX, tb2);
		drawLine(r + PL_OFFCIRCLE_FX, tb2, r, b);
		drawLine(r, b, lr2, b + PL_OFFCIRCLE_FX);
		drawLine(lr2, b + PL_OFFCIRCLE_FX, l, b);
		drawLine(l, b, l - PL_OFFCIRCLE_FX, tb2);
		drawLine(l - PL_OFFCIRCLE_FX, tb2, l, t);
	}
}
//----------------------------------------------------------------------------------

void processMUNRocket()
{
	if(reflect_rocket > 0)
	{
		if(--reflect_rocket == 0)
		{
			reflect_rocket = 0;
			prSetProperty(M0_ROCKET, PRP_ENABLE, 0);
			return;
		}
	}
	if(prGetProperty(M0_ROCKET, PRP_ENABLE) != 0)
	{
		fx32 speed;
		struct fxVec2 pos, p, tpos, end;
		p = pos = prGetPosition(M0_ROCKET);
		tpos = prGetPosition(M0_TARGET);
		speed = MR_SPEED_FX; 
		if(reflect_rocket > 0)
		{
			tpos.y = pos.y + FX32(mthGetRandom(20) * (mthGetRandom(2) == 0 ? 1 : -1));
			tpos.x = MUNRIDER_PARTICLES_MAX_X_FX;
			speed = MR_SPEED_FX * 2;
		}
		end.x = tpos.x - mr_off_power;
		end.y = tpos.y;
		if(!nextPointAtLine(p, end, FX_Mul(speed, getCurrentDeltaTimeScale()), &pos))
		{
			prSetProperty(M0_ROCKET, PRP_ENABLE, 0);
		}
		if((mr_off_power -= FX32(10.0f)) < 0)
		{
			mr_off_power = 0;
		}
		prAddXYToPosition(M0_ROCKET, pos.x - p.x, pos.y - p.y);
		p.x = MUNRIDER_PARTICLES_MAX_X_FX;
		p.y = FX32(mthGetRandom(20) * (mthGetRandom(2) == 0 ? 1 : -1));
		p.y = head_posy0 + ((p.y * 3) / 2) + p.y;
		createParticles(M0_ROCKET, 3, 3, FX32(1.5f), FX32(1.0f), 150, 100, 150, &p.x, &p.y);
		createParticles(M0_ROCKET, 4, 4, FX32(0.3f), FX32(2.0f), 600, 200, 200, &p.x, &p.y);
		createParticles(M0_ROCKET, 2, 2, FX32(0.3f), FX32(2.0f), 600, 200, 15, &p.x, &p.y);
	}
}
//----------------------------------------------------------------------------------

void startRocket()
{
	if(prGetProperty(M0_ROCKET, PRP_ENABLE) == 0)
	{
		startScript(M0_SCR_ROCKET_BACK_TO_MUN, TRUE, -1, -1);
		mr_off_power = MR_INITIAL_POWER_FX;
	}
}
//----------------------------------------------------------------------------------

//CALLBACK FUNCTIONS

//----------------------------------------------------------------------------------

void onEngineEvent(const struct EventCallbackData *ipData)
{
	switch(ipData->eventType)
	{
		case EVENT_TYPE_ON_SOUND:
		case EVENT_TYPE_ON_ANIMATION:
		break;

		case EVENT_TYPE_ON_END_ASYNH_LOAD_DATA:
			
			switch(ipData->eventId)
			{
				case LOAD_TYPE_ENDLOADDATATASK:
					if(ipData->initiatorId == 111)
					{
						gameState = gstLOADING;
					}
				break;
				case LOAD_TYPE_MAPDATA:
					if(ipData->initiatorId == 222)
					{
						//switch(ipData->layer)
						onLevelMapLoad();
					}
				break;
			}
		break;
		
		case EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES:
			setVisible(GAME_LAYER, TRUE);
			gameState = gstGAME;
		break;

		case EVENT_TYPE_ON_DIRECTOR:
		{
			switch(getCurrentZone(GAME_LAYER))
			{
				case 0: // zone with robo_a (see mapeditor)
					if(prGetGroupIdx(ipData->initiatorId) == ROBOT1)
					{
						const s32 st = prGetState(ipData->initiatorId); 
						if(st == SLEEP)
						{
							prSetProperty(ipData->initiatorId, PRP_PAUSE, (fx32)MUNRIDER_MAX_PAUSE_ROBOT_A_TIME);
							prSetProperty(ipData->initiatorId, PRP_AGGRESIV, prGetProperty(ipData->initiatorId, PRP_SPEED));
							prSetProperty(ipData->initiatorId, PRP_SPEED, MUNRIDER_MIN_PAUSE_ROBOT_SPEED);
							robo_shoot_id = ipData->initiatorId;
							robo_shoot_ct = MUNRIDER_ROBOT_A_SHOOT_TIME;  
						}
					}
				break;

				case 3: // boss with robo_b (see mapeditor)
					if(M0_BOSS_LINZA == ipData->initiatorId)
					{
						startScript(M0_CHECK_BOSS_DAMAGED_STATE, FALSE, M0_BOSS_LINZA, ipData->ownerId);
						startScript(M0_SCR_RETURN_BOSS, FALSE, M0_BOSS_LINZA, ipData->ownerId);
						startScript(M0_BOSS_DAMAGE_MUN, FALSE, M0_BOSS_LINZA, ipData->ownerId);
						startScript(M0_BOSS_TORPEDO_LAUNCH, FALSE, M0_BOSS_LINZA, ipData->ownerId);
						time_to_flash = 0; // set flag to allow flash against boss again
					}
				break;

				case 4:
					if(M0_NODE_35 == ipData->ownerId)
					{
						if(M0_BOSS_LINZA == ipData->initiatorId)
						{
							//final cutscene 1
							startScript(M0_SCR_BEGIN_JUMP, FALSE, M0_BOSS_LINZA, ipData->ownerId);
						}
					}
					else if(M0_NODE_END_LEVEL == ipData->ownerId)
					{
						//final cutscene 2
						final_cutscene_2 = TRUE;
						struct fxVec2 cpos = getCamera();
						cpos.y += FINAL_CUTSCENE2_Y_OFF_FX;
						setCamera(cpos);
						prSetProperty(LITTLE_BOSS, PRP_ENABLE, 1);
					}
				break;
			}
		}
		break;
	}
}
//----------------------------------------------------------------------------------
