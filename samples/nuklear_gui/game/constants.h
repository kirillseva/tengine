#ifndef _GE_CONSTANTS_H_
#define _GE_CONSTANTS_H_

enum GEConstants
{

	// Languages:
	LANGUAGE_DEFAULT = 0,

	// Game object types:
	TRIGGER = 0,
	DIRECTOR = 1,
	TEXTBOX = 2,
	OBJ = 5,

	// Properties of game object:
	PRP_SPEED = 0,

	// Map0 objects list
	M0_OBJ_3 = 0,

	// Map0 pathnodes list
	M0_NODE_2 = 0,
	M0_NODE_3 = 1,

	// Map1 fonts list
	M1_FONT_ARIAL = 0,

	// Map1 objects list
	M1_TXTBOX_2 = 0,

	// Animations: (for drawSingleFrame() function only)
	ANIM_IDLE = 0,

	// States:
	STATE_IDLE = 0,

	// Movement :
	DIR_IDLE = 0,
	DIR_UP = 2,
	DIR_LEFT = 4,
	DIR_RIGHT = 6,
	DIR_DOWN = 8,
	DIR_COUNT = 5

};

#endif
