/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef _GAMEFIELD_H_
#define _GAMEFIELD_H_

#include "system.h"

#ifdef __cplusplus
extern "C" {
#endif

void tfgInitMemory(void);

void tfgInit(void);

void tfgRelease(void);

void tfgLowMemory(void);

void tfgLostRenderDevice(void);

void tfgRestoreRenderDevice(void);

void tfgResize(s32 w, s32 h);

void tfgSetWindowModeSize(s32 *const w, s32 *const h);

void tfgTick(s32 ms);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif //_GAMEFIELD_H_

