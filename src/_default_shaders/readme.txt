﻿при включенном ключе препроцессора USE_OPENGL_2_RENDER
в папках "rawres" должны находиться файлы
color.fsh
lit.fsh
unlit.fsh
color.vsh
lit.vsh
unlit.vsh 

при сборке ресурсов не забудьте скопировать их в папку с ресурсами

(смотрите пример в проекте tengine\samples\scroll_map)