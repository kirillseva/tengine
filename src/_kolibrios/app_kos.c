/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2014 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "kos32sys.h"
#include "lib/platform_low.h"
#include "lib/tengine_low.h"
#include "lib/filesystem_low.h"
#include "lib/a_touchpad.h"
#include "lib/a_gamepad.h"
#include "lib/render.h"
#include "gamepad.h"
#include "texts.h"
#include "filesystem.h"
#include "gamefield.h"

#if defined USE_OPENGL_RENDER

// off-screen Mesa rendering
// #define USE_OSMESA
 
 #if defined USE_OSMESA
	#include "GL/osmesa.h"
 #else
	#define EGL_EGLEXT_PROTOTYPES
	#define GL_GLEXT_PROTOTYPES
	#include "EGL/egl.h"
	#include "EGL/eglext.h"
	#include "GL/gl.h"
	#include "gbm.h"
	#include "render.h"
 #endif
#endif

static const char DEFAULT_RES_FOLDERS[] = "data/";

enum SysEventTypes
{
	KOS_REDRAW_EVENT = 1,
	KOS_KEY_EVENT = 2,
	KOS_GUI_BUTTON_EVENT = 3,
	KOS_MOUSE_EVENT = 6
};

#if defined USE_OPENGL_RENDER && !defined USE_OSMESA
struct AppGlData
{
	EGLDisplay dpy;
	EGLContext context;
	EGLSurface surface;
	EGLConfig config;
	struct gbm_device *gbm;
	struct gbm_surface *gs;
    struct render *render;
};
#endif

struct ApplicationData
{
	BOOL mouseLButtonDown;
	pos_t lastValidMousePos;
	s32 lastKeyDown;
};

static struct TEnginePlatformData *gspPlatformData = NULL;
static s32 sWindowWidth = 1;
static s32 sWindowHeight = 1;
static struct ApplicationData gsAppData;
#if defined USE_OPENGL_RENDER && !defined USE_OSMESA
static struct AppGlData gsAppGlData;
static void egl_destroy(void);
static EGLConfig choose_config(void);
static int egl_initialize(EGLDisplay *dpy, EGLConfig *config, EGLContext *context);
#endif

#if defined USE_CUSTOM_RENDER
static void _transferFrameBuffer(const struct BMPImage* pFrameBuffer, s32 offX, s32 offY, s32 frameWidth, s32 frameHeight);
#endif
static void _setMaskForEvents(u32 mask);
static void _setKeysScancodeMode(void);
static void _renderWindow(s32 x, s32 y, s32 w, s32 h);
static void _process_mouse(BOOL forceCancelMouseInput);
static void _process_keyboard(BOOL forceCancelKeyInput);
static u32 _getActiveWindowSlotId(void);
static u32 _getSlotId(u32 pid);

//-----------------------------------------------------------------------

void _setMaskForEvents(u32 mask)
{
	asm volatile ("int $0x40"::"a"(40), "b"(mask));
}
//---------------------------------------------------------------

void _setKeysScancodeMode()
{
	asm volatile ("int $0x40"::"a"(66), "b"(1), "c"(1));
}
//---------------------------------------------------------------

u32 _getActiveWindowSlotId()
{
	u32 val;
	asm volatile ("int $0x40":"=a"(val):"a"(18),"b"(7));
	return val;
}
//---------------------------------------------------------------

u32 _getSlotId(u32 pid)
{
	u32 val;
	asm volatile ("int $0x40":"=a"(val):"a"(18),"b"(21),"c"(pid));
	return val;
}
//---------------------------------------------------------------

void _renderWindow(s32 x, s32 y, s32 w, s32 h)
{
	BeginDraw();
	DrawWindow(x, y, w, h, NULL, 0, 0x74);
	EndDraw();
}
//---------------------------------------------------------------

#if defined USE_CUSTOM_RENDER
void _transferFrameBuffer(const struct BMPImage* pFrameBuffer, s32 offX, s32 offY, s32 frameWidth, s32 frameHeight)
{
	SDK_ASSERT(pFrameBuffer->mType == BMP_TYPE_DC16);
	asm volatile ("nop"::"D"(0), "c"(frameWidth * 65536 + frameHeight), "d"(offX * 65536 + offY), "b"(pFrameBuffer->data.mpDataDC16));
	asm volatile ("xor %eax, %eax");
	asm volatile ("movl %eax, %ebp");
	asm volatile ("pushl $15");
	asm volatile ("popl %esi");
	asm volatile ("int $0x40"::"a"(65));
}
#endif
//-------------------------------------------------------------------------------------------

#if defined USE_OPENGL_RENDER && !defined USE_OSMESA
EGLConfig choose_config()
{
	EGLConfig config = NULL;
	EGLint config_attribs[32];
	EGLint num_configs, i;

	i = 0;
	config_attribs[i++] = EGL_RED_SIZE;
	config_attribs[i++] = 1;
	config_attribs[i++] = EGL_GREEN_SIZE;
	config_attribs[i++] = 1;
	config_attribs[i++] = EGL_BLUE_SIZE;
	config_attribs[i++] = 1;
	config_attribs[i++] = EGL_DEPTH_SIZE;
	config_attribs[i++] = 1;

	config_attribs[i++] = EGL_SURFACE_TYPE;
	config_attribs[i++] = EGL_WINDOW_BIT;

	config_attribs[i++] = EGL_RENDERABLE_TYPE;
	config_attribs[i++] = EGL_OPENGL_BIT;
	config_attribs[i] = EGL_NONE;

	eglChooseConfig(gsAppGlData.dpy, config_attribs, &config, 1, &num_configs);

	return config;
}
//-------------------------------------------------------------------------------------------

int egl_initialize(EGLDisplay *dpy, EGLConfig *config, EGLContext *context)
{
	EGLint major, minor;
	int fd;

	fd = get_service("DISPLAY");
	if(fd == 0)
		return -1;

	gsAppGlData.gbm = gbm_create_device(fd);
	if(gsAppGlData.gbm == NULL)
	{
		OS_Warning("failed to initialize GBM device\n");
		goto err_0;
	};

	*dpy = eglGetDisplay(gsAppGlData.gbm);

	if (!eglInitialize(*dpy, &major, &minor))
	{
		OS_Warning("failed to initialize EGL display\n");
		goto err_1;
	};

	OS_Printf("EGL_VERSION = %s\n", eglQueryString(*dpy, EGL_VERSION));
	OS_Printf("EGL_VENDOR = %s\n", eglQueryString(*dpy, EGL_VENDOR));
	OS_Printf("EGL_EXTENSIONS = %s\n", eglQueryString(*dpy, EGL_EXTENSIONS));
	OS_Printf("EGL_CLIENT_APIS = %s\n", eglQueryString(*dpy, EGL_CLIENT_APIS));

	*config = choose_config();
	if( *config == NULL)
	{
		OS_Warning("failed to choose a config\n");
		goto err_2;
	};

	eglBindAPI(EGL_OPENGL_API);
	*context = eglCreateContext(*dpy, *config, EGL_NO_CONTEXT, NULL);
	if (context == NULL)
	{
		OS_Warning("failed to create context\n");
		goto err_2;
	};

	if (!eglMakeCurrent(*dpy, EGL_NO_SURFACE, EGL_NO_SURFACE, *context))
	{
		OS_Warning("failed to make context current");
		goto err_3;
	};

	return 0;

err_3:
	eglDestroyContext(*dpy, *context);
err_2:
	eglTerminate(*dpy);
err_1:
	gbm_device_destroy(gsAppGlData.gbm);
	gsAppGlData.gbm = NULL;
err_0:
	return -1;
}
//-------------------------------------------------------------------------------------------

void egl_destroy()
{
	if(gsAppGlData.render != NULL)
	{
		free(gsAppGlData.render);
		gsAppGlData.render = NULL;
	}
	if(gsAppGlData.surface != NULL)
	{
		eglDestroySurface(gsAppGlData.dpy, gsAppGlData.surface);
		gsAppGlData.surface = NULL;
	}
	if(gsAppGlData.gs != NULL)
	{
		gbm_surface_destroy(gsAppGlData.gs);
		gsAppGlData.gs = NULL;
	}
	if(gsAppGlData.gbm != NULL)
	{
		eglMakeCurrent(gsAppGlData.dpy, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
		eglDestroyContext(gsAppGlData.dpy, gsAppGlData.context);
		eglTerminate(gsAppGlData.dpy);
		gbm_device_destroy(gsAppGlData.gbm);
		gsAppGlData.gbm = NULL;
	}
}
#endif
//-------------------------------------------------------------------------------------------

int main(int argc, char *argv[])
{
	char resPath[1024];
	struct InitFileSystemData fileSystemData;
#if defined USE_OPENGL_RENDER && defined USE_OSMESA
	OSMesaContext context;
	void *buffer;
#endif
	uint8_t wndState;
	u32 slotIdx;
	(void)argc;

	_setMaskForEvents(0x27); // 00000000 00000000 00000000 00100111
	_setKeysScancodeMode();

	gspPlatformData = Platform_GetPlatformData();
	gspPlatformData->renderDevice = FALSE;
	gspPlatformData->tengineInit = FALSE;
	gsAppData.mouseLButtonDown = FALSE;
	gsAppData.lastKeyDown = -1;

	get_proc_info(resPath);
	slotIdx = _getSlotId(*(u32*)(resPath + 30)); // SysFn09, dword: (PID/TID)
	
	{
		char *resPathPtr; 
		char *argv0 = argv[0];
		if(argv0[0] < 0x20) // fs_execute() UTF-8 hack
		{
			++argv0;
		}
		strncpy(resPath, argv0, MAX_FILEPATH);
		resPathPtr = strrchr(resPath, '/');
		*++resPathPtr = 0;
		strcat(resPath, DEFAULT_RES_FOLDERS);
	}
	
	tfgInitMemory();
	
#if defined USE_CUSTOM_RENDER
	egRender_Init(_transferFrameBuffer);
	tfgSetWindowModeSize(&sWindowWidth, &sWindowHeight);
#endif

#if defined USE_OPENGL_RENDER
	glRender_Init();
	tfgSetWindowModeSize(&sWindowWidth, &sWindowHeight);
 #if defined USE_OSMESA
	buffer = NULL;
	context = OSMesaCreateContextExt(OSMESA_BGRA, 16, 0, 0, NULL);
	if(context == NULL)
	{
		OS_Warning("Mesa create context error\n");
		goto err_0;
	}
	buffer = MALLOC(sWindowWidth * sWindowHeight * 4 * sizeof(GLubyte), "main::gsAppGlData.buffer");
	if(buffer == NULL)
	{
		OS_Warning("Data buffer memory allocation error\n");
		goto err_0;
	}
	if(!OSMesaMakeCurrent(context, buffer, GL_UNSIGNED_BYTE, sWindowWidth, sWindowHeight))
	{
		OS_Warning("Mesa error: MakeCurrent\n");
		goto err_0;
	}
	_renderWindow(0, 0, sWindowWidth + TYPE_3_BORDER_WIDTH * 2, sWindowHeight + get_skin_height() + TYPE_3_BORDER_WIDTH);
	Blit(buffer, TYPE_3_BORDER_WIDTH, get_skin_height(), 0, 0, sWindowWidth, sWindowHeight, sWindowWidth, sWindowHeight, sWindowWidth * 4);
 #else
	gsAppGlData.dpy = NULL;
	gsAppGlData.context = NULL;
	gsAppGlData.surface = NULL;
	gsAppGlData.config = NULL;
	gsAppGlData.gs = NULL;
	gsAppGlData.render = NULL;
	gsAppGlData.gbm = NULL;
	if(egl_initialize(&gsAppGlData.dpy, &gsAppGlData.config, &gsAppGlData.context) != 0)
	{
		OS_Warning("egl initialization error\n");
		goto err_0;
	}
	gsAppGlData.gs = gbm_surface_create(gsAppGlData.gbm, sWindowWidth, sWindowHeight, GBM_BO_FORMAT_ARGB8888, GBM_BO_USE_RENDERING);
	_renderWindow(0, 0, sWindowWidth + TYPE_3_BORDER_WIDTH * 2, sWindowHeight + get_skin_height() + TYPE_3_BORDER_WIDTH);
	gsAppGlData.surface = eglCreateWindowSurface(gsAppGlData.dpy, gsAppGlData.config, (EGLNativeWindowType)gsAppGlData.gs, NULL);
	if(gsAppGlData.surface == EGL_NO_SURFACE)
	{
		OS_Warning("%s\n", "failed to create surface");
		goto err_0;
	}
	if(!eglMakeCurrent(gsAppGlData.dpy, gsAppGlData.surface, gsAppGlData.surface, gsAppGlData.context))
	{
		OS_Warning("%s\n", "failed to make window current");
		goto err_0;
	}
 #endif
#endif

	fileSystemData.mpPath = resPath;
	InitFileSystem(&fileSystemData);

	tfgInit();

	tfgResize(sWindowWidth, sWindowHeight);
#if defined USE_OPENGL_RENDER
	glRender_Resize(sWindowWidth, sWindowHeight);
#endif
	OS_Printf("init tengine display with size: %d %d\n", sWindowWidth, sWindowHeight);

#if defined USE_CUSTOM_RENDER
	_renderWindow(0, 0, sWindowWidth + TYPE_3_BORDER_WIDTH * 2, sWindowHeight + get_skin_height() + TYPE_3_BORDER_WIDTH);
#endif
	gspPlatformData->tengineInit = TRUE;
	gspPlatformData->renderDevice = TRUE;
	
#if defined USE_OPENGL_RENDER && !defined USE_OSMESA
	gsAppGlData.render = create_render(gsAppGlData.dpy, gsAppGlData.surface, TYPE_3_BORDER_WIDTH, get_skin_height());
#endif

	Platform_InitTick();
	wndState = 0;
	while(gspPlatformData->tengineInit)
	{
		uint32_t evt;
		if(wndState & (WIN_STATE_MINIMIZED | WIN_STATE_ROLLED))
		{
			evt = get_os_event();
			switch(evt)
			{
				case KOS_REDRAW_EVENT:
				{
					s32 winx, winy, winw, winh;
					get_proc_info(resPath);
					wndState = *(uint8_t*)(resPath + 70);
					winx = *(uint32_t*)(resPath + 34);
					winy = *(uint32_t*)(resPath + 38);
					winw = *(uint32_t*)(resPath + 42) + TYPE_3_BORDER_WIDTH * 2;
					winh = *(uint32_t*)(resPath + 46) + get_skin_height() + TYPE_3_BORDER_WIDTH;
					_renderWindow(winx, winy, winw, winh);
					if(!(wndState & (WIN_STATE_MINIMIZED | WIN_STATE_ROLLED)))
					{
						Platform_restoreRenderDevice();
					}
				}
				break;
				
				case KOS_GUI_BUTTON_EVENT:
					if(get_os_button() == 1) // 1 -- [x] window button id
					{
						gspPlatformData->tengineInit = FALSE;
					}
				break;
				
				default:
				break;
			}
		}
		else
		{
			evt = wait_for_event(2); // 100 = 1 sec
			
			// avoid problem with calling KOS_REDRAW_EVENT on "Window Minimize" system event
			get_proc_info(resPath);
			
			switch(evt)
			{
				case KOS_REDRAW_EVENT:
				{
					s32 winx, winy, winw, winh;
					winx = *(uint32_t*)(resPath + 34);
					winy = *(uint32_t*)(resPath + 38);
					winw = *(uint32_t*)(resPath + 42) + TYPE_3_BORDER_WIDTH * 2;
					winh = *(uint32_t*)(resPath + 46) + get_skin_height() + TYPE_3_BORDER_WIDTH;
					_renderWindow(winx, winy, winw, winh);
				}
				break;

				case KOS_KEY_EVENT:
					_process_keyboard(_getActiveWindowSlotId() != slotIdx);
				break;
				
				case KOS_MOUSE_EVENT:
					_process_mouse(_getActiveWindowSlotId() != slotIdx);
				break;
				
				case KOS_GUI_BUTTON_EVENT:
					if(get_os_button() == 1) // 1 -- [x] window button id
					{
						gspPlatformData->tengineInit = FALSE;
					}
				break;
				
				default:
				break;
			}
			
			wndState = *(uint8_t*)(resPath + 70);
			if(wndState & (WIN_STATE_MINIMIZED | WIN_STATE_ROLLED))
			{
				Platform_lostRenderDevice();
				continue;
			}
			
			Platform_Tick();
#if defined USE_OPENGL_RENDER
 #if defined USE_OSMESA
			glFlush();
			Blit(buffer, TYPE_3_BORDER_WIDTH, get_skin_height(), 0, 0, sWindowWidth, sWindowHeight, sWindowWidth, sWindowHeight, sWindowWidth * 4);
 #else
			render_swap_and_blit(gsAppGlData.render);
 #endif
#endif
		}
	}
#if defined USE_OPENGL_RENDER
err_0:
#endif
	tfgRelease();
	ReleaseFileSystem();
#if defined USE_CUSTOM_RENDER
	egRender_Release();
#endif
#if defined USE_OPENGL_RENDER
 #if defined USE_OSMESA
	if(buffer != NULL)
	{
		FREE(buffer);
	}
	if(context != NULL)
	{
		OSMesaDestroyContext(context);
	}
 #else
	glRender_Release();
	egl_destroy();
 #endif
#endif
	gspPlatformData->tengineInit = FALSE;
	
	return 0;
}
//-----------------------------------------------------------------------

void _process_mouse(BOOL forceCancelMouseInput)
{
	const uint32_t m_bt_bits = get_mouse_buttons() & (forceCancelMouseInput ? 0x0 : 0x1);
	if((m_bt_bits & 0x1) == 0x1)
	{
		const pos_t pos = get_mouse_pos(POS_WINDOW);
		if(pos.x >= 0 && pos.y >= 0)
		{
			onTouchPadDown(0, (u16)pos.x, (u16)pos.y);
			gsAppData.mouseLButtonDown = TRUE;
			gsAppData.lastValidMousePos = pos;
		}
	}
	else
	{
		if(gsAppData.mouseLButtonDown == TRUE)
		{
			pos_t pos = get_mouse_pos(POS_WINDOW);
			if(pos.x < 0 || pos.y < 0)
			{
				pos = gsAppData.lastValidMousePos;
			}
			onTouchPadUp(0, (u16)pos.x, (u16)pos.y);
			gsAppData.mouseLButtonDown = FALSE;
		}
	}
}
//-----------------------------------------------------------------------

void _process_keyboard(BOOL forceCancelKeyInput)
{
	s32 te_key = -1;		
	oskey_t kos_key = get_key();
	//OS_Printf("kv1=%d\n", kos_key.code & 0x7F);

	if(forceCancelKeyInput == TRUE)
	{
		if(gsAppData.lastKeyDown != -1)
		{
			onKeyDown(gsAppData.lastKeyDown);
			gsAppData.lastKeyDown = -1;
		}
		return;
	}
	
	switch(kos_key.code & 0x7F)
	{
		case 28: // Enter
			te_key = PAD_BUTTON_START;
		break;
		case 72: 
			te_key = PAD_KEY_UP;
		break;
		case 80: 
			te_key = PAD_KEY_DOWN;
		break;
		case 75: 
			te_key = PAD_KEY_LEFT;
		break;
		case 77:
			te_key = PAD_KEY_RIGHT;
		break;
		case 42:
		case 54:
			te_key = PAD_BUTTON_B;
		break;
		case 29:
			te_key = PAD_BUTTON_A;
		break;
		case 1: // Esc
			gspPlatformData->tengineInit = FALSE;
		break;
	}
	if(te_key != -1)
	{
		if(kos_key.code & 0x80)
		{
			onKeyUp(te_key);
			gsAppData.lastKeyDown = -1;
		}
		else
		{
			onKeyDown(te_key);
			gsAppData.lastKeyDown = te_key;
		}
	}
}
//-----------------------------------------------------------------------
