#ifndef GUI_SPEEDBUTTON_H
#define GUI_SPEEDBUTTON_H

#ifdef __cplusplus
extern "C" {
#endif

#include "guibasebutton.h"

struct GUISpeedButton;
    

s32 GUISpeedButton_GetMapObjId(const struct GUISpeedButton *self);
    
s32 GUISpeedButton_GetTabOrder(const struct GUISpeedButton *self);

void GUISpeedButton_SetText(struct GUISpeedButton *self, const wchar *text);

void GUISpeedButton_SetHotkey(struct GUISpeedButton *self, enum KeyType key);

enum KeyType GUISpeedButton_GetHotkey(const struct GUISpeedButton *self);

void GUISpeedButton_Click(struct GUISpeedButton *self);

void GUISpeedButton_SetEnable(struct GUISpeedButton *self, BOOL value);
BOOL GUISpeedButton_IsEnable(const struct GUISpeedButton *self);

void GUISpeedButton_SetEnableInput(struct GUISpeedButton *self, BOOL val);
BOOL GUISpeedButton_IsEnableInput(const struct GUISpeedButton *self);

void GUISpeedButton_SetAction(struct GUISpeedButton *self, const struct GUIAction *action);
struct GUIAction GUISpeedButton_GetAction(const struct GUISpeedButton *self);

void GUISpeedButton_SetPosition(struct GUISpeedButton *self, fx32 x, fx32 y);
void GUISpeedButton_GetPosition(const struct GUISpeedButton *self, fx32 *x, fx32 *y);

void GUISpeedButton_GetCollideRect(const struct GUISpeedButton *self, fx32 rect[RECT_SIZE]);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
