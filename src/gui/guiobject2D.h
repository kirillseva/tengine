#ifndef GUI_OBJECT2D_H
#define GUI_OBJECT2D_H

#ifdef __cplusplus
extern "C" {
#endif

#include "guitypes.h"

struct GUIObject2D;

enum GUIControlType GUIObject2D_GetType(const struct GUIObject2D *self);

s32 GUIObject2D_GetMapObjId(const struct GUIObject2D *self);

s32 GUIObject2D_GetTabOrder(const struct GUIObject2D *self);

void GUIObject2D_SetEnable(struct GUIObject2D *self, BOOL state);

BOOL GUIObject2D_IsEnable(const struct GUIObject2D *self);

void GUIObject2D_SetPosition(struct GUIObject2D *self, fx32 x, fx32 y);

void GUIObject2D_GetPosition(const struct GUIObject2D *self, fx32 *x, fx32 *y);

void GUIObject2D_Update(const struct GUIObject2D *self, s32 ms);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif //GUI_OBJECT2D_H
