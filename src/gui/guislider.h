#ifndef GUI_SLIDER_H
#define GUI_SLIDER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "guiobject2D.h"

struct GUISlider;
    
s32 GUISlider_GetMapObjId(const struct GUISlider *self);

s32 GUISlider_GetTabOrder(const struct GUISlider *self);

void GUISlider_SetEnable(struct GUISlider *self, BOOL value);
BOOL GUISlider_IsEnable(const struct GUISlider *self);

void GUISlider_SetEnableInput(struct GUISlider *self, BOOL val);
BOOL GUISlider_IsEnableInput(const struct GUISlider *self);

void GUISlider_SetPosition(struct GUISlider *self, fx32 x, fx32 y);
void GUISlider_GetPosition(const struct GUISlider *self, fx32 *x, fx32 *y);

s32 GUISlider_GetCapacity(struct GUISlider *self);

s32 GUISlider_GetItemsCount(struct GUISlider *self);
void GUISlider_AddItem(struct GUISlider *self, const struct GUISliderItem *item);
void GUISlider_DeleteItem(struct GUISlider *self, s32 idx);
BOOL GUISlider_GetItem(struct GUISlider *self, s32 idx, struct GUISliderItem *const oitem);
BOOL GUISlider_UpdateItem(struct GUISlider *self, s32 idx, const struct GUISliderItem *const item);
s32 GUISlider_GetItemSlotMapIdIfExist(struct GUISlider *self, s32 idx);
void GUISlider_ClearItems(struct GUISlider *self);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
