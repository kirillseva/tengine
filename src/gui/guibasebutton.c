/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "lib/tengine_low.h"
#include "touchpad.h"
#include "guibasebutton.h"
#include "guicontainer.h"
#include "gui_low.h"

void _GUIBaseButton_GUIObject2D_SetFocusFlag(struct GUIObject2D *obj, BOOL val);
BOOL _GUIBaseButton_GUIObject2D_IsEnable(struct GUIObject2D *obj);

//---------------------------------------------------------------------------

void GUIBaseButton_protected_Init(struct GUIBaseButton *self)
{
	MI_CpuClear8(self, sizeof(struct GUIBaseButton));
	GUIObject2D_protected_Init(&self->guiObject);
    
    self->guiObject.mType = GUI_TYPE_NONE;
	self->guiObject.mpObject = NULL;

	self->guiObject.mpSetEnable = NULL;
	self->guiObject.mpProcessInput = NULL;
	self->guiObject.mpGetTabOrder = NULL;
	self->guiObject.mpUpdate = NULL;
	self->guiObject.mpIsEnable = _GUIBaseButton_GUIObject2D_IsEnable;
	self->guiObject.mpSetFocusFlag = _GUIBaseButton_GUIObject2D_SetFocusFlag;

	self->mpButtonPress = NULL;
	self->mpButtonRelease = NULL;

	self->mState = ST_NONE;
	self->mPointerState = PST_UP;
    self->mTouchPointerId = -1;
    self->guiObject.mMapObjID = -1;
    self->mAction.mType = AT_DO_NOTHING;
    self->mEnableInput = TRUE;
}
//---------------------------------------------------------------------------

void GUIBaseButton_protected_Release(struct GUIBaseButton *self)
{
	GUIObject2D_protected_Release(&self->guiObject);
	MI_CpuClear8(self, sizeof(struct GUIBaseButton));
}
//---------------------------------------------------------------------------

void _GUIBaseButton_GUIObject2D_SetFocusFlag(struct GUIObject2D *obj, BOOL val)
{
	(void)obj;
	(void)val;
}
//---------------------------------------------------------------------------

void GUIBaseButton_GetCollideRect(const struct GUIBaseButton *self, fx32 rect[RECT_SIZE])
{
    SDK_ASSERT(self->guiObject.mMapObjID >= 0);
	low_getCollideRect(self->guiObject.mMapObjID, rect, GUIContainer_GetLayer(self->guiObject.mpParent));
}
//---------------------------------------------------------------------------

BOOL _GUIBaseButton_GUIObject2D_IsEnable(struct GUIObject2D *obj)
{
    struct GUIBaseButton *bb = (struct GUIBaseButton*)obj;
	SDK_NULL_ASSERT(bb->mpBaseParams);
    if(bb->mpBaseParams->enabledPrpId != -1)
	{
		return prGetPropertyLayer(GUIObject2D_GetMapObjId(&bb->guiObject), bb->mpBaseParams->enabledPrpId, GUIContainer_GetLayer(bb->guiObject.mpParent)) != 0;
	}
	else
	{
		return TRUE;
	}
}
//---------------------------------------------------------------------------

BOOL GUIBaseButton_IsEnable(const struct GUIBaseButton *self)
{
	return GUIObject2D_IsEnable(&self->guiObject);
}
//---------------------------------------------------------------------------

void GUIBaseButton_SetEnable(struct GUIBaseButton *self, BOOL value)
{
	GUIObject2D_SetEnable(&self->guiObject, value);
}
//---------------------------------------------------------------------------

void GUIBaseButton_EnableInput(struct GUIBaseButton *self, BOOL val)
{
	SDK_NULL_ASSERT(self->mpSetEnableInput);
	SDK_NULL_ASSERT(self->guiObject.mpObject);
	self->mEnableInput = val;
	self->mpSetEnableInput(self, val);
	if(val == FALSE && self->mTouchPointerId > 0)
	{
		if(GUIContainer_protected_GetTrigged(self->guiObject.mpParent, self->mTouchPointerId) == &self->guiObject)
		{
			GUIContainer_protected_SetTrigged(self->guiObject.mpParent, NULL, self->mTouchPointerId);
		}	
	}
	self->mTouchPointerId = -1;
}
//---------------------------------------------------------------------------

BOOL GUIBaseButton_IsEnableInput(const struct GUIBaseButton *self)
{
	return self->mEnableInput;
}
//---------------------------------------------------------------------------

void GUIBaseButton_Click(struct GUIBaseButton *self)
{
    if(GUIBaseButton_IsEnable(self) && self->guiObject.mpParent != NULL &&
		self->guiObject.mpParent->mpEventHandler != NULL)
	{
        u32 layer;
		struct GUIEvent e;
		e.mpSender = &self->guiObject;
		e.mpAction = &self->mAction;
		e.mState = ST_CLICK;
		e.mPointerState = self->mPointerState;
		e.mPointerId = self->mTouchPointerId;
		layer = getActiveLayer();
		_setActiveLayer(GUIContainer_GetLayer(self->guiObject.mpParent));
		self->guiObject.mpParent->mpEventHandler(&e);
		_setActiveLayer(layer);
    }
}
//---------------------------------------------------------------------------

void GUIBaseButton_SetAction(struct GUIBaseButton *self, const struct GUIAction *action)
{
    self->mAction = *action;
}
//---------------------------------------------------------------------------

struct GUIAction GUIBaseButton_GetAction(const struct GUIBaseButton *self)
{
    return self->mAction;
}
//---------------------------------------------------------------------------

void GUIBaseButton_protected_ButtonPress(struct GUIBaseButton *self, BOOL sendEvent)
{
	if(GUIBaseButton_IsEnable(self))
	{
		SDK_NULL_ASSERT(self->mpButtonPress);
		SDK_NULL_ASSERT(self->guiObject.mpObject);
		
		self->mpButtonPress(self);

		self->mState = ST_PRESSED;
		if(sendEvent && self->guiObject.mpParent != NULL && self->guiObject.mpParent->mpEventHandler != NULL)
		{
			u32 layer;
			struct GUIEvent e;
			e.mpSender = &self->guiObject;
			e.mpAction = &self->mAction;
			e.mState = ST_PRESSED;
			e.mPointerState = self->mPointerState;
			e.mPointerId = self->mTouchPointerId;
			layer = getActiveLayer();
			_setActiveLayer(GUIContainer_GetLayer(self->guiObject.mpParent));
			self->guiObject.mpParent->mpEventHandler(&e);
			_setActiveLayer(layer);
		}
	}
}
//---------------------------------------------------------------------------

void GUIBaseButton_protected_ButtonRelease(struct GUIBaseButton *self, BOOL sendEvent)
{
	if(GUIBaseButton_IsEnable(self))
	{
		SDK_NULL_ASSERT(self->mpButtonRelease);
		SDK_NULL_ASSERT(self->guiObject.mpObject);
		
		self->mpButtonRelease(self);

		self->mState = ST_NONE;
		self->mPointerState = PST_UP;
		if(sendEvent && self->guiObject.mpParent != NULL && self->guiObject.mpParent->mpEventHandler != NULL)
		{
			u32 layer;
			struct GUIEvent e;
			e.mpSender = &self->guiObject;
			e.mpAction = &self->mAction;
			e.mState = ST_RELEASED;
			e.mPointerState = self->mPointerState;
			e.mPointerId = self->mTouchPointerId;
			layer = getActiveLayer();
			_setActiveLayer(GUIContainer_GetLayer(self->guiObject.mpParent));
			self->guiObject.mpParent->mpEventHandler(&e);
			_setActiveLayer(layer);
		}
	}
}
//---------------------------------------------------------------------------
