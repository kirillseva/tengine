/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "lib/tengine_low.h"
#include "texts.h"
#include "touchpad.h"
#include "guispeedbutton.h"
#include "guicontainer.h"
#include "guifactory.h"
#include "guislider.h"
#include "gui_low.h"
#include "static_allocator.h"
#include "containers/allocator_array.h"
#include "fxmath.h"

//static const s32 SLIDE_ITEM_DELAY  = 800;
//static const s32 SLIDER_ITEM_SPEED  = 200;
//static const s32 SLIDER_ITEM_FAST_SPEED = 150;

static void _GUISlider_GUIObject2D_Update(struct GUIObject2D *obj, s32 ms);
static void _GUISlider_GUIObject2D_SetFocusFlag(struct GUIObject2D *obj, BOOL state);
static void _GUISlider_GUIObject2D_ProcessInput(struct GUIObject2D *obj, const TouchPadData *tpData);
static s32 _GUISlider_GUIObject2D_GetTabOrder(struct GUIObject2D *obj);
static void _GUISlider_GUIObject2D_SetEnable(struct GUIObject2D *obj, BOOL state);
static BOOL _GUISlider_GUIObject2D_IsEnable(struct GUIObject2D *obj);

static void _GUISlider_Reset(struct GUISlider *self);

static void _GUISlider_updateAllItemsPositionsH(struct GUISlider* self);
static void _GUISlider_updateAllItemsPositionsV(struct GUISlider* self);
static fx32 _GUISlider_calculateItemPosX(struct GUISlider* self, s32 idx);
static fx32 _GUISlider_calculateItemPosY(struct GUISlider* self, s32 idx);

static void _GUISlider_OnItemEvent(const struct GUIEvent* event);
static void _GUISlider_UpdateItem(struct GUISlider *self, s32 idx, const struct GUISliderItem *const item);

//---------------------------------------------------------------------------

void GUISlider_protected_Init(struct GUISlider *self, const struct GUISliderInitParameters* params, s32 mapObjID, struct GUIContainer* parent)
{	
	s32 i;
	u32 heapSize;
	u16 maxTextItemChars;
	struct GameObjectAnimationFrameInfo goaif;

	SDK_ASSERT(mapObjID >= 0);

	MI_CpuClear8(self, sizeof(struct GUISlider));
	GUIObject2D_protected_Init(&self->guiObject);

	self->guiObject.mType = GUI_TYPE_SLIDER;
	self->guiObject.mpObject = self;

	self->mpParams = params;
	SDK_NULL_ASSERT(self->mpParams);
	SDK_ASSERT(self->mpParams->mapObjSlider[SLIDER_MAPOBJ_ID_PANEL_DEFAULT] != -1);
	SDK_ASSERT(self->mpParams->mapObjSlider[SLIDER_MAPOBJ_ID_CONTAINER] != -1);

	self->guiObject.mMapObjID = mapObjID;
	self->guiObject.mpParent = parent;
	for(i = 0; i < SLIDER_MAPOBJ_ID_COUNT; i++)
	{
		self->mMapObjId[i] = -1;
	}
	self->mMapObjId[SLIDER_MAPOBJ_ID_PANEL_DEFAULT] = mapObjID;

	self->guiObject.mpProcessInput = _GUISlider_GUIObject2D_ProcessInput;
	self->guiObject.mpGetTabOrder = _GUISlider_GUIObject2D_GetTabOrder;
	self->guiObject.mpSetEnable = _GUISlider_GUIObject2D_SetEnable;
	self->guiObject.mpIsEnable = _GUISlider_GUIObject2D_IsEnable;
	self->guiObject.mpSetFocusFlag = _GUISlider_GUIObject2D_SetFocusFlag;
	self->guiObject.mpUpdate = _GUISlider_GUIObject2D_Update;

	self->mAction.mType = AT_DO_NOTHING;

	SDK_ASSERT(self->mpParams->mapObjSliderVisibleItemsArray != NULL ||
				self->mpParams->mapObjSliderVisibleTextsItemsArray != NULL);

	if(self->mpParams->mapObjSliderVisibleTextsItemsArray == NULL)
	{
		maxTextItemChars = 0;	
	}
	else
	{
		maxTextItemChars = self->mpParams->maxTextItemChars;
	}

	if(self->mpParams->mapObjSliderVisibleItemsArray != NULL)
	{
		SDK_ASSERT(self->mpParams->mapObjSliderVisibleItemsArray[0] != -1);
		low_getCurrentAnimationFrameInfo(self->mpParams->mapObjSliderVisibleItemsArray[0],
												&goaif, GUIContainer_GetLayer(self->guiObject.mpParent));
	}
	else
	{
		SDK_ASSERT(self->mpParams->mapObjSliderVisibleTextsItemsArray[0] != -1);
		low_getCurrentAnimationFrameInfo(self->mpParams->mapObjSliderVisibleTextsItemsArray[0],
											&goaif, GUIContainer_GetLayer(self->guiObject.mpParent));
	}
	self->mItemWidth = goaif.width;
	self->mItemHeight = goaif.height;
	self->mItem_height_koef = self->mItem_width_koef = FX32_ONE;

	self->mpUpdatePosFn = (self->mpParams->orientation == GUISLIDER_ORIENTATION_HORIZONTAL) ?
									_GUISlider_updateAllItemsPositionsH : _GUISlider_updateAllItemsPositionsV;
	self->mItemCursorIndex = -1;
	
	heapSize = AllocatorArray_CalculateHeapSize(sizeof(struct GUISliderItem), self->mpParams->maxCapacity);
	for(i = 0; i < self->mpParams->maxCapacity && maxTextItemChars > 0; i++)
	{
		heapSize += AllocatorArray_CalculateHeapSize((sizeof(wchar) * (self->mpParams->maxTextItemChars + 1)), 1);
	}
	self->mpPool = (u8*)MALLOC(heapSize, "GUISliderInit::mpPool");
	self->mpPoolAllocator = (struct StaticAllocator *)MALLOC(sizeof(struct StaticAllocator), "GUISliderInit::mpPoolAllocator");
	self->mpArray = (struct AllocatorArray *)MALLOC(sizeof(struct AllocatorArray), "GUISliderInit::mpArray");
	StaticAllocator_Init(self->mpPoolAllocator, self->mpPool, heapSize);
	A_ARRAY_INIT_RESERVE(struct GUISliderItem, self->mpParams->maxCapacity, *self->mpArray, *self->mpPoolAllocator);
	self->mppItemParams = (struct GUISpeedButtonInitParameters **)MALLOC(sizeof(struct GUISpeedButtonInitParameters*) * self->mpParams->mapObjSliderVisibleItemsArraySize, "GUISliderInit::mppItemParams");
	for(i = 0; i < self->mpParams->mapObjSliderVisibleItemsArraySize; i++)
	{
		self->mppItemParams[i] = (struct GUISpeedButtonInitParameters *)MALLOC(sizeof(struct GUISpeedButtonInitParameters), "GUISliderInit::mppItemParams[i]");
	}

	GUIContainer_Init(&self->mContainer, GUIContainer_GetLayer(self->guiObject.mpParent), self->mpParams->mapObjSliderVisibleItemsArraySize);
	GUIContainer_SetGUIEventHandler(&self->mContainer, _GUISlider_OnItemEvent);
	
	for(i = 0; i < self->mpParams->mapObjSliderVisibleItemsArraySize; i++)
	{
		struct GUISpeedButton* obj;
		s32 objid, textobj;
		objid = textobj = -1;
		if(self->mpParams->mapObjSliderVisibleTextsItemsArray != NULL)
		{
			textobj = objid = self->mpParams->mapObjSliderVisibleTextsItemsArray[i];
			SDK_ASSERT(objid != -1);
			self->mppItemParams[i]->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_TEXT] = prGetGroupIdxLayer(objid, GUIContainer_GetLayer(self->guiObject.mpParent));
		}
		else
		{
			self->mppItemParams[i]->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_TEXT] = -1;
		}
		if(self->mpParams->mapObjSliderVisibleItemsArray != NULL)
		{
			objid = self->mpParams->mapObjSliderVisibleItemsArray[i];
			SDK_ASSERT(objid != -1);
			self->mppItemParams[i]->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE] = prGetGroupIdxLayer(objid, GUIContainer_GetLayer(self->guiObject.mpParent));
		}
		else
		{
			self->mppItemParams[i]->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE] = objid;
		}
		self->mppItemParams[i]->baseInitParameters.enabledPrpId = params->enabledPrpId;
		self->mppItemParams[i]->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_PRESSED] = -1;
		self->mppItemParams[i]->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_FOCUSED] = -1;
		obj = (struct GUISpeedButton *)MALLOC(sizeof(struct GUISpeedButton), "GUISliderInit::obj");
		GUISpeedButton_protected_Init(obj, self->mppItemParams[i], objid, &self->mContainer);
		GUIContainer_protected_Add(&self->mContainer, (struct GUIObject2D*)obj);
		if(textobj >= 0)
		{
			GUISpeedButton_protected_SetTextMapObjId(obj, textobj);
		}
	}
	self->mContainer.mpParent = &self->guiObject;

	prChangeTypeLayer(self->guiObject.mMapObjID, self->mpParams->mapObjSlider[SLIDER_MAPOBJ_ID_PANEL_DEFAULT], GUIContainer_GetLayer(parent));
	
	self->mEnableInput = TRUE;

	_GUISlider_Reset(self);
}
//---------------------------------------------------------------------------

void GUISlider_protected_Release(struct GUISlider *self)
{
	s32 i;
	GUIContainer_FreeAll(&self->mContainer);
	i = self->mpParams->mapObjSliderVisibleItemsArraySize;
	while(i > 0)
	{
		i--;
		FREE(self->mppItemParams[i]);
	}
	FREE(self->mppItemParams);
	FREE(self->mpArray);
	FREE(self->mpPoolAllocator);
	FREE(self->mpPool);
	MI_CpuClear8(self, sizeof(struct GUISlider));
}
//---------------------------------------------------------------------------

void _GUISlider_Reset(struct GUISlider *self)
{
	self->mReset = TRUE;
}
//---------------------------------------------------------------------------

void GUISlider_protected_SetContainerId(struct GUISlider *self, s32 id)
{
	self->mMapObjId[SLIDER_MAPOBJ_ID_CONTAINER] = id;
}
//---------------------------------------------------------------------------

void _GUISlider_GUIObject2D_SetFocusFlag(struct GUIObject2D *obj, BOOL state)
{
	struct GUISlider *self = (struct GUISlider *)obj;
	if(self->mMapObjId[SLIDER_MAPOBJ_ID_PANEL_FOCUSED] >= 0)
    {
		SDK_NULL_ASSERT(self->mpParams);
		SDK_ASSERT(self->mpParams->enabledPrpId != -1);
		prSetPropertyLayer(self->mMapObjId[SLIDER_MAPOBJ_ID_PANEL_FOCUSED], self->mpParams->enabledPrpId,
										FX32((char)state), GUIContainer_GetLayer(self->guiObject.mpParent));
    }  
}
//---------------------------------------------------------------------------

void _GUISlider_GUIObject2D_ProcessInput(struct GUIObject2D *obj, const TouchPadData *tpData)
{
	struct GUISlider *self = (struct GUISlider *)obj;
	if(self->mEnableInput == TRUE)
	{
		GUIContainer_ProcessInput(&self->mContainer, tpData);
	}
}
//---------------------------------------------------------------------------

void _GUISlider_OnItemEvent(const struct GUIEvent* event)
{
	if(GUIObject2D_GetType(event->mpSender) == GUI_TYPE_SPEEDBUTTON)
	{
		u32 layer;
		struct GUIEvent e;
		struct GUISlider *s = (struct GUISlider*)event->mpSender->mpParent->mpParent->mpObject;
		e.mpSender = &s->guiObject;
		e.mState = event->mState;
		e.mPointerState = event->mPointerState;
		e.mPointerId = event->mPointerId;
		s->mAction.mParameter1 = event->mpAction->mParameter1;
		s->mAction.mParameter2 = event->mpAction->mParameter2;
		e.mpAction = &s->mAction;
		layer = getActiveLayer();
		_setActiveLayer(GUIContainer_GetLayer(s->guiObject.mpParent));
		s->guiObject.mpParent->mpEventHandler(&e);
		_setActiveLayer(layer);
	}
}
//---------------------------------------------------------------------------

s32 _GUISlider_GUIObject2D_GetTabOrder(struct GUIObject2D *obj)
{
	struct GUISlider *self = (struct GUISlider *)obj;
	SDK_NULL_ASSERT(self->mpParams);
	if(self->mpParams->tabOrderPrpId >= 0)
	{
		return fx2int(prGetPropertyLayer(GUIObject2D_GetMapObjId(&self->guiObject), self->mpParams->tabOrderPrpId, GUIContainer_GetLayer(self->guiObject.mpParent)));
	}
	return -1;
}
//---------------------------------------------------------------------------

BOOL _GUISlider_GUIObject2D_IsEnable(struct GUIObject2D *obj)
{
	struct GUISlider *self = (struct GUISlider *)obj;
	SDK_NULL_ASSERT(self->mpParams);
    if(self->mpParams->enabledPrpId != -1)
	{
		return prGetPropertyLayer(GUIObject2D_GetMapObjId(&self->guiObject), self->mpParams->enabledPrpId, GUIContainer_GetLayer(self->guiObject.mpParent)) != 0;
	}
	else
	{
		return TRUE;
	}
}
//---------------------------------------------------------------------------

void _GUISlider_GUIObject2D_SetEnable(struct GUIObject2D *obj, BOOL state)
{
	u8 i;
	struct GUISlider *self = (struct GUISlider *)obj;
	SDK_NULL_ASSERT(self->mpParams);
	SDK_ASSERT(self->mpParams->enabledPrpId != -1);
	prSetPropertyLayer(GUIObject2D_GetMapObjId(&self->guiObject),
			self->mpParams->enabledPrpId,
			FX32((char)state),
			GUIContainer_GetLayer(self->guiObject.mpParent));
	for(i = 0; i < 3; i++)
	{
		s32 ch_idx = prGetJoinedChildLayer(GUIObject2D_GetMapObjId(&self->guiObject), i, 
										GUIContainer_GetLayer(self->guiObject.mpParent));
		if(NONE_MAP_IDX != ch_idx)
		{
			prSetPropertyLayer(ch_idx,
					self->mpParams->enabledPrpId,
					FX32((char)state),
					GUIContainer_GetLayer(self->guiObject.mpParent));
		}
	}
	for(i = 0; i < GUIContainer_GetObjectsCount(&self->mContainer); i++)
	{
		struct GUISpeedButton *sb = (struct GUISpeedButton *)GUIContainer_GetObject(&self->mContainer, i);
		GUISpeedButton_SetEnable(sb, state);
	}
	if(GUIContainer_GetFocus(self->guiObject.mpParent) == &self->guiObject)
    {
		GUIContainer_FindNextByTabOrder(self->guiObject.mpParent);
    }
}
//---------------------------------------------------------------------------

void _GUISlider_updateAllItemsPositionsH(struct GUISlider* self)
{
	s32 i, curr, itemcount;
	struct fxVec2 xy, pos;
	struct GUISpeedButton *sb;
	struct GUIAction action;
	struct GameObjectAnimationFrameInfo goaif_p;
	struct GameObjectAnimationFrameInfo goaif_i;

	SDK_ASSERT(self->mMapObjId[SLIDER_MAPOBJ_ID_CONTAINER] >= 0);
	low_getCurrentAnimationFrameInfo(self->mMapObjId[SLIDER_MAPOBJ_ID_CONTAINER], &goaif_p, GUIContainer_GetLayer(self->guiObject.mpParent));
	self->mItem_width_koef = FX_Div(goaif_p.width, (self->mItemWidth * GUIContainer_GetObjectsCount(&self->mContainer))); 

	SDK_ASSERT(GUIContainer_GetObjectsCount(&self->mContainer) >= 0);
	pos = prGetPositionLayer(GUIObject2D_GetMapObjId(GUIContainer_GetObject(&self->mContainer, 0)), GUIContainer_GetLayer(self->guiObject.mpParent));
	low_getCurrentAnimationFrameInfo(GUIObject2D_GetMapObjId(GUIContainer_GetObject(&self->mContainer, 0)),
												&goaif_i, GUIContainer_GetLayer(self->guiObject.mpParent));
	xy.y = pos.y - goaif_i.top + goaif_p.top + (goaif_p.height - goaif_i.height) / 2;

	curr = self->mItemCursorIndex;
	itemcount = (s32)A_ARRAY_SIZE(*self->mpArray);
	for(i = 0; i < GUIContainer_GetObjectsCount(&self->mContainer); i++)
	{
		xy.x = pos.x - goaif_i.left + goaif_p.left + _GUISlider_calculateItemPosX(self, i);
		sb = (struct GUISpeedButton *)GUIContainer_GetObject(&self->mContainer, i);
		prSetPositionLayer(GUISpeedButton_GetMapObjId(sb), xy, GUIContainer_GetLayer(self->guiObject.mpParent));
		action.mType = AT_DO_NOTHING;
		action.mParameter1 = -1;
		action.mParameter2 = 0;
		if(curr < itemcount && itemcount > 0)
		{
			struct GUISliderItem *item = &A_ARRAY_AT(struct GUISliderItem, *self->mpArray, curr);
			if(item->mapObjectItem >= 0)
			{
				struct fxVec2 posi;
				struct GameObjectAnimationFrameInfo goaif_ii;
				posi = prGetPositionLayer(item->mapObjectItem, GUIContainer_GetLayer(self->guiObject.mpParent));
				low_getCurrentAnimationFrameInfo(item->mapObjectItem, &goaif_ii, GUIContainer_GetLayer(self->guiObject.mpParent));
				posi.y = posi.y - goaif_ii.top + goaif_i.top + (goaif_i.height - goaif_ii.height) / 2;
				posi.x = posi.x - goaif_ii.left + xy.x - pos.x + goaif_i.left + (goaif_i.width - goaif_ii.width) / 2;
				prSetPositionLayer(item->mapObjectItem, posi, GUIContainer_GetLayer(self->guiObject.mpParent));
				action.mParameter1 = i + self->mItemCursorIndex;
				action.mParameter2 = item->eventData;
			}
			if(item->mpText != NULL)
			{
				GUISpeedButton_SetText(sb, item->mpText);
			}
			curr++;
		}
		GUISpeedButton_SetAction(sb, &action);
	}
}
//---------------------------------------------------------------------------

fx32 _GUISlider_calculateItemPosX(struct GUISlider* self, s32 idx)
{			
	/*return self->mItemWidth - FX_Mul(self->mItemWidth, self->mItem_width_koef) + self->mItemWidth / 4 + 
			FX_Mul(self->mItemWidth, self->mItem_width_koef) * idx - 
			(FX_Mul(FX_Mul(self->mSlideProcess, self->mItemWidth), self->mItem_width_koef)) -
			self->mPanelWidth / 2;*/

	return (FX_Mul(self->mItemWidth, self->mItem_width_koef) - self->mItemWidth) / 2 + FX_Mul(self->mItemWidth, self->mItem_width_koef) * idx;
}
//---------------------------------------------------------------------------

fx32 _GUISlider_calculateItemPosY(struct GUISlider* self, s32 idx)
{			
	return (FX_Mul(self->mItemHeight, self->mItem_height_koef) - self->mItemHeight) / 2 + FX_Mul(self->mItemHeight, self->mItem_height_koef) * idx;
}
//---------------------------------------------------------------------------

void _GUISlider_updateAllItemsPositionsV(struct GUISlider* self)
{
	s32 i, curr, itemcount;
	struct fxVec2 pos, xy;
	struct GUISpeedButton *sb;
	struct GUIAction action;
	struct GameObjectAnimationFrameInfo goaif_p;
	struct GameObjectAnimationFrameInfo goaif_i;

	SDK_ASSERT(self->mMapObjId[SLIDER_MAPOBJ_ID_CONTAINER] >= 0);
	low_getCurrentAnimationFrameInfo(self->mMapObjId[SLIDER_MAPOBJ_ID_CONTAINER], &goaif_p, GUIContainer_GetLayer(self->guiObject.mpParent));
	self->mItem_height_koef = FX_Div(goaif_p.height, (self->mItemHeight * GUIContainer_GetObjectsCount(&self->mContainer))); 

	SDK_ASSERT(GUIContainer_GetObjectsCount(&self->mContainer) >= 0);
	pos = prGetPositionLayer(GUIObject2D_GetMapObjId(GUIContainer_GetObject(&self->mContainer, 0)), GUIContainer_GetLayer(self->guiObject.mpParent));
	low_getCurrentAnimationFrameInfo(GUIObject2D_GetMapObjId(GUIContainer_GetObject(&self->mContainer, 0)),
												&goaif_i, GUIContainer_GetLayer(self->guiObject.mpParent));
	xy.x = pos.x - goaif_i.left + goaif_p.left + (goaif_p.width - goaif_i.width) / 2;

	curr = self->mItemCursorIndex;
	itemcount = (s32)A_ARRAY_SIZE(*self->mpArray);
	for(i = 0; i < GUIContainer_GetObjectsCount(&self->mContainer); i++)
	{
		xy.y = pos.y - goaif_i.top + goaif_p.top + _GUISlider_calculateItemPosY(self, i);
		sb = (struct GUISpeedButton *)GUIContainer_GetObject(&self->mContainer, i);
		prSetPositionLayer(GUISpeedButton_GetMapObjId(sb), xy, GUIContainer_GetLayer(self->guiObject.mpParent));
		action.mType = AT_DO_NOTHING;
		action.mParameter1 = -1;
		action.mParameter2 = 0;
		if(curr < itemcount && itemcount > 0)
		{
			struct GUISliderItem *item = &A_ARRAY_AT(struct GUISliderItem, *self->mpArray, curr);
			if(item->mapObjectItem >= 0)
			{
				struct fxVec2 posi;
				struct GameObjectAnimationFrameInfo goaif_ii;
				posi = prGetPositionLayer(item->mapObjectItem, GUIContainer_GetLayer(self->guiObject.mpParent));
				low_getCurrentAnimationFrameInfo(item->mapObjectItem, &goaif_ii, GUIContainer_GetLayer(self->guiObject.mpParent));
				posi.x = posi.x - goaif_ii.left + goaif_i.left + (goaif_i.width - goaif_ii.width) / 2;
				posi.y = posi.y - goaif_ii.top + xy.y - pos.y + goaif_i.top + (goaif_i.height - goaif_ii.height) / 2;
				prSetPositionLayer(item->mapObjectItem, posi, GUIContainer_GetLayer(self->guiObject.mpParent));
				action.mParameter1 = i + self->mItemCursorIndex;
				action.mParameter2 = item->eventData;
			}
			if(item->mpText != NULL)
			{
				GUISpeedButton_SetText(sb, item->mpText);
			}
			curr++;
		}
		GUISpeedButton_SetAction(sb, &action);
	}
}
//---------------------------------------------------------------------------

void _GUISlider_GUIObject2D_Update(struct GUIObject2D *obj, s32 ms)
{
    struct GUISlider *self = (struct GUISlider*)obj;

	//please do not perform any rotation for slider 
	SDK_ASSERT(prGetCustomRotationLayer(self->guiObject.mMapObjID, GUIContainer_GetLayer(self->guiObject.mpParent)) == 0);

	if(self->mSlideDirection != 0)
	{	
		self->mSlideProcess += FX_Mul(FX_Div(ms, self->mSpeed), self->mSlideDirection);
		if(self->mSlideProcess > FX32_ONE || self->mSlideProcess < 0)
		{
			self->mSlideProcess = 0;
			//_GUISlider_deleteItem(self, self->mSlideDirection);
			self->mSlideDirection = 0;
		}
		self->mpUpdatePosFn(self);
	}
	else
	{
		if(self->mReset)
		{
			self->mReset = FALSE;
			self->mpUpdatePosFn(self);
		}
		/*
		if(self->mCurrentButton && self->mCurrentButton.getCurrentState() == GuiButton.STATE_DOWN)
		{
			mTimer -= ms;
			if(mTimer < 0)
			{
				mSlideDidection = mCurrentButton.getUserData() as int;
				if(mSlideDidection > 0)
				{
					mSlideProcess = 0.0;
				}
				else
				{
					mSlideProcess = 1.0;	
				}
				addItem(mSlideDidection);
				if(mLongDelay)
				{
					mTimer = SLIDE_ITEM_DELAY;
					mLongDelay = false;
				}
				else
				{
					mTimer = 0;
					mSpeed = SLIDER_ITEM_FAST_SPEED;
				}
			}
		}
		*/
	}
}
//---------------------------------------------------------------------------

s32 GUISlider_GetMapObjId(const struct GUISlider *self)
{
	return GUIObject2D_GetMapObjId(&self->guiObject);
}
//---------------------------------------------------------------------------

s32 GUISlider_GetTabOrder(const struct GUISlider *self)
{
	return GUIObject2D_GetTabOrder(&self->guiObject);
}
//---------------------------------------------------------------------------

void GUISlider_SetEnable(struct GUISlider *self, BOOL value)
{
	GUIObject2D_SetEnable(&self->guiObject, value);
}
//---------------------------------------------------------------------------

BOOL GUISlider_IsEnable(const struct GUISlider *self)
{
	return GUIObject2D_IsEnable(&self->guiObject);
}
//---------------------------------------------------------------------------

void GUISlider_SetPosition(struct GUISlider *self, fx32 x, fx32 y)
{
	GUIObject2D_SetPosition(&self->guiObject, x, y);
	self->mpUpdatePosFn(self);
}
//---------------------------------------------------------------------------

void GUISlider_GetPosition(const struct GUISlider *self, fx32 *x, fx32 *y)
{
	GUIObject2D_GetPosition(&self->guiObject, x, y);
}
//---------------------------------------------------------------------------

s32 GUISlider_GetCapacity(struct GUISlider *self)
{
	return self->mpParams->maxCapacity;
}
//---------------------------------------------------------------------------

s32 GUISlider_GetItemsCount(struct GUISlider *self)
{
	return (s32)A_ARRAY_SIZE(*self->mpArray);
}
//---------------------------------------------------------------------------

BOOL GUISlider_GetItem(struct GUISlider *self, s32 idx, struct GUISliderItem *const oitem)
{
	s32 count = (s32)A_ARRAY_SIZE(*self->mpArray);
	if(idx < count && idx >= 0)
	{
		*oitem = A_ARRAY_AT(struct GUISliderItem, *self->mpArray, idx);
		return TRUE;
	}
	return FALSE;
}
//---------------------------------------------------------------------------

BOOL GUISlider_UpdateItem(struct GUISlider *self, s32 idx, const struct GUISliderItem *const item)
{
	s32 count = (s32)A_ARRAY_SIZE(*self->mpArray);
	if(idx < count && idx >= 0)
	{
		_GUISlider_UpdateItem(self, idx, item);
		self->mpUpdatePosFn(self);
		return TRUE;
	}
	return FALSE;
}
//---------------------------------------------------------------------------

s32 GUISlider_GetItemSlotMapIdIfExist(struct GUISlider *self, s32 idx)
{
	s32 count = (s32)A_ARRAY_SIZE(*self->mpArray);
	if(idx < count && idx >= 0)
	{
		s32 containerIdx = idx - self->mItemCursorIndex;
		if(containerIdx >= 0 && containerIdx < GUIContainer_GetObjectsCount(&self->mContainer))
		{
			struct GUISpeedButton *sb = (struct GUISpeedButton *)GUIContainer_GetObject(&self->mContainer, containerIdx);
			return sb->baseButton.guiObject.mMapObjID;
		}
	}
	return NONE_MAP_IDX;
}
//---------------------------------------------------------------------------

void _GUISlider_UpdateItem(struct GUISlider *self, s32 idx, const struct GUISliderItem *const item)
{
	struct GUISliderItem *curr;
	SDK_NULL_ASSERT(item);
	curr = &A_ARRAY_AT(struct GUISliderItem, *self->mpArray, idx);
	curr->eventData = item->eventData;
	curr->itemData1 = item->itemData1;
	curr->itemData2 = item->itemData2;
	curr->mapObjectItem = item->mapObjectItem;
	if(curr->mpText != NULL && item->mpText != NULL && curr->mpText != item->mpText)
	{
		u32 textSize = (u32)STD_WStrLen(item->mpText);
		if(textSize > (u32)self->mpParams->maxTextItemChars)
		{
			textSize = (u32)self->mpParams->maxTextItemChars;
		}
		MI_CpuCopy8(item->mpText, curr->mpText, textSize * sizeof(wchar));
		curr->mpText[textSize] = L'\0';
	}
}
//---------------------------------------------------------------------------

void GUISlider_AddItem(struct GUISlider *self, const struct GUISliderItem *item)
{
	struct GUISliderItem titem;
	SDK_ASSERT((s32)A_ARRAY_SIZE(*self->mpArray) < (s32)(self->mpParams->maxCapacity));
	SDK_ASSERT(self->mMapObjId[SLIDER_MAPOBJ_ID_CONTAINER] >= 0);
	MI_CpuClear8(&titem, sizeof(struct GUISliderItem));
	if(self->mpParams->maxTextItemChars > 0)
	{
		titem.mpText = (wchar*)StaticAllocator_Malloc(self->mpPoolAllocator, self->mpParams->maxTextItemChars);
		titem.mpText[0] = L'\0';
	}
	A_ARRAY_PUSH_BACK(struct GUISliderItem, *self->mpArray, titem);
	_GUISlider_UpdateItem(self, (s32)A_ARRAY_SIZE(*self->mpArray) - 1, item);
	if(item->mapObjectItem >= 0)
	{
		low_setClipRect(item->mapObjectItem, self->mMapObjId[SLIDER_MAPOBJ_ID_CONTAINER], GUIContainer_GetLayer(self->guiObject.mpParent));
	}
	if(self->mItemCursorIndex < 0)
	{
		self->mItemCursorIndex = 0;
	}
	self->mpUpdatePosFn(self);
}
//---------------------------------------------------------------------------

void GUISlider_DeleteItem(struct GUISlider *self, s32 idx)
{
	s32 count = (s32)A_ARRAY_SIZE(*self->mpArray);
	if(idx < count && idx >= 0)
	{
		struct GUISliderItem *curr = &A_ARRAY_AT(struct GUISliderItem, *self->mpArray, idx);
		if(curr->mapObjectItem >= 0)
		{
			low_setClipRect(curr->mapObjectItem, -1, GUIContainer_GetLayer(self->guiObject.mpParent));
		}
		if(curr->mpText != NULL)
		{
			StaticAllocator_Free(self->mpPoolAllocator, curr->mpText);
		}
		if(idx == self->mItemCursorIndex)
		{
			self->mItemCursorIndex--;
		}
		A_ARRAY_ERASE(struct GUISliderItem, *self->mpArray, idx);
		self->mpUpdatePosFn(self);
	}
}
//---------------------------------------------------------------------------

void GUISlider_ClearItems(struct GUISlider *self)
{
	StaticAllocator_Reset(self->mpPoolAllocator);
	A_ARRAY_INIT(struct GUISliderItem, *self->mpArray, *self->mpPoolAllocator);
	self->mItemCursorIndex = -1;
}
//---------------------------------------------------------------------------

void GUISlider_SetEnableInput(struct GUISlider *self, BOOL val)
{
	s32 i;
	self->mEnableInput = val;
	for(i = 0; i < GUIContainer_GetObjectsCount(&self->mContainer); i++)
	{
		struct GUISpeedButton *sb = (struct GUISpeedButton *)GUIContainer_GetObject(&self->mContainer, i);
		GUISpeedButton_SetEnableInput(sb, val);
	}
}
//---------------------------------------------------------------------------

BOOL GUISlider_IsEnableInput(const struct GUISlider *self)
{
	return self->mEnableInput;
}
//---------------------------------------------------------------------------
