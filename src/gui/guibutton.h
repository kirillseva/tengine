#ifndef GUI_BUTTON_H
#define GUI_BUTTON_H

#ifdef __cplusplus
extern "C" {
#endif

#include "guibasebutton.h"

struct GUIButton;

s32 GUIButton_GetMapObjId(const struct GUIButton *self);

s32 GUIButton_GetTabOrder(const struct GUIButton *self);

void GUIButton_SetText(struct GUIButton *self, const wchar *text);

void GUIButton_Click(struct GUIButton *self);

void GUIButton_SetEnable(struct GUIButton *self, BOOL value);
BOOL GUIButton_IsEnable(const struct GUIButton *self);

void GUIButton_SetEnableInput(struct GUIButton *self, BOOL val);
BOOL GUIButton_IsEnableInput(const struct GUIButton *self);

void GUIButton_SetAction(struct GUIButton *self, const struct GUIAction *action);
struct GUIAction GUIButton_GetAction(const struct GUIButton *self);

void GUIButton_SetPosition(struct GUIButton *self, fx32 x, fx32 y);
void GUIButton_GetPosition(const struct GUIButton *self, fx32 *x, fx32 *y);

void GUIButton_GetCollideRect(const struct GUIButton *self, fx32 rect[RECT_SIZE]);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
