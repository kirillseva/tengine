/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef GUI_TYPES_H
#define GUI_TYPES_H

#ifdef __cplusplus
extern "C" {
#endif

#include "platform.h"

struct GUIBaseButton;
struct GUIObject2D;
struct GUIAction;
struct GUISlider;

enum GUIControlType
{
    GUI_TYPE_NONE = 0,
    GUI_TYPE_BUTTON,
    GUI_TYPE_SPEEDBUTTON,
    GUI_TYPE_SLIDER
};

enum GUIStateType
{
	ST_NONE = 0,
	ST_PRESSED,
	ST_RELEASED,
	ST_CLICK
};

enum GUIPointerStateType
{
	PST_UP = 0,
	PST_DOWN,
	PST_ENTER,
	PST_LEAVE
};

struct GUIEvent
{
    const struct GUIObject2D* mpSender;
	const struct GUIAction* mpAction;
	enum GUIStateType mState;
	enum GUIPointerStateType mPointerState;
	s32 mPointerId;
};

enum GUIButtonMapObjectPart
{
	BUTTON_MAPOBJ_ID_IDLE,
    BUTTON_MAPOBJ_ID_PRESSED,
    BUTTON_MAPOBJ_ID_FOCUSED,
    BUTTON_MAPOBJ_ID_TEXT,
    BUTTON_MAPOBJ_ID_COUNT
};

enum GUISliderMapObjectPart
{
	SLIDER_MAPOBJ_ID_PANEL_DEFAULT,
	SLIDER_MAPOBJ_ID_PANEL_FOCUSED,
	SLIDER_MAPOBJ_ID_CONTAINER,
	SLIDER_MAPOBJ_ID_COUNT
};

struct GUIBaseButtonInitParameters
{
    s32 mapObjTypeID[BUTTON_MAPOBJ_ID_COUNT];
	s16 enabledPrpId;
};

struct GUIButtonInitParameters
{
    struct GUIBaseButtonInitParameters baseInitParameters;
	s16 tabOrderPrpId;
};

struct GUISpeedButtonInitParameters
{
    struct GUIBaseButtonInitParameters baseInitParameters;
};

enum GUISliderOrientation
{
	GUISLIDER_ORIENTATION_VERTICAL = 0,
	GUISLIDER_ORIENTATION_HORIZONTAL
};

struct GUISliderItem
{
	s32 mapObjectItem;
	s32 eventData;
	s32 itemData1;
	s32 itemData2;
	wchar *mpText; 
};

struct GUISliderInitParameters
{
	s32 mapObjSlider[SLIDER_MAPOBJ_ID_COUNT];
	const s32 *mapObjSliderVisibleItemsArray;
	const s32 *mapObjSliderVisibleTextsItemsArray;
	enum GUISliderOrientation orientation;
	u16 mapObjSliderVisibleItemsArraySize;
	u16 maxCapacity;
	u16 maxTextItemChars;
	s16 enabledPrpId;
	s16 tabOrderPrpId;
};

enum GUIActionType
{
    AT_DO_NOTHING = 0,
	AT_GOTO_LEVEL,
    AT_START_TENGINE_SCRIPT,
    AT_CUSTOM
};

struct GUIAction
{
	enum GUIActionType mType;
    s32 mParameter1;
    s32 mParameter2;
};

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif

