#ifndef GUI_FACTORY_H
#define GUI_FACTORY_H

#ifdef __cplusplus
extern "C" {
#endif

#include "guitypes.h"

struct GUIContainer;

void GUIFactory_Init(struct GUIContainer *pTargetGUIManager);

void GUIFactory_Reset(void);

void GUIFactory_ParseMapObjects(void);



void GUIFactory_AddGUIButtonType(const struct GUIButtonInitParameters *params);

void GUIFactory_AddGUISpeedButtonType(const struct GUISpeedButtonInitParameters *params);
  
void GUIFactory_AddGUISliderType(const struct GUISliderInitParameters *params);

struct GUIButtonInitParameters GUIFactory_GetButtonInitParameters(s32 idx);

struct GUISpeedButtonInitParameters GUIFactory_GetSpeedButtonInitParameters(s32 idx);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
