#ifndef _A_STAR_PATH_H_
#define _A_STAR_PATH_H_

#include "platform.h"
#include "as_heap.h"

#ifdef __cplusplus
extern "C" {
#endif

struct AStarPathInitParams
{
	const u8 *map; /*costs map (0xff -- full solid) */
	u16 map_w;
	u16 map_h;
	BOOL use_diag;
	u32 maxNodesPool;
};

struct AStarPathNode
{
	u16 x;
	u16 y;
	s32 g; /* movement cost */
	s32 h; /* heuristic cost */
	const struct AStarPathNode *parent;
};

struct AStarPath
{
	u16 orig_x;
	u16 orig_y;
	u16 dest_x;
	u16 dest_y;
	u32 pool_top;
	struct AStarHeap open;
	struct AStarHeap close;
	struct AStarPathNode **pool;
	const struct AStarPathNode *dest_node;
	struct AStarPathInitParams params;
};

void A_STAR_InitPath(struct AStarPath *path, struct AStarPathInitParams *params);
void A_STAR_ChangeCostsMap(struct AStarPath *path, const u8 *map, u16 map_w, u16 map_h);
void A_STAR_FindPath(struct AStarPath *path, u16 orig_x, u16 orig_y, u16 dest_x, u16 dest_y);
void A_STAR_ReleasePath(struct AStarPath *path);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
