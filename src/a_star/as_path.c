#include "as_heap.h"
#include "as_path.h"
#include "fxmath.h"
#include "memory.h"

//-------------------------------------------------------------------------------------------
// this code based on
// Path Finding Demo 2004 David A. Capello.
//-------------------------------------------------------------------------------------------

/* A is a best node of B */
#define _A_STAR_is_best_node(a, b) (a->g + a->h < b->g + b->h)

/* recalculate G (movement cost made) */
#define _A_STAR_recalc_node_g(a) \
	{ \
		a->g = a->parent ? a->parent->g + ((a->x != a->parent->x && a->y != a->parent->y) ? (14 * g_cost) : (10 * g_cost)) : 0; \
	}

/* recalculate H (heuristic movement cost) */
#define _A_STAR_recalc_node_h(a) \
	{ /* Manhattan distance */ \
		a->h = (MATH_ABS(a->x - path->dest_x) + MATH_ABS(a->y - path->dest_y)) * 10; \
	}

static void _A_STAR_add_node_to_path(struct AStarPath *path, u16 x, u16 y, const struct AStarPathNode *parent);
static s32 _A_STAR_cmp_nodes(const struct AStarPathNode *p1, const struct AStarPathNode *p2);
static void _A_STAR_add_adjacent_nodes_to_path(struct AStarPath *path, const struct AStarPathNode *node);
static void _A_STAR_add_adjacent_nodes_to_path_check_diag(struct AStarPath *path, const struct AStarPathNode *node);

//-------------------------------------------------------------------------------------------

void A_STAR_InitPath(struct AStarPath* path, struct AStarPathInitParams *params)
{
	u32 i;
	SDK_NULL_ASSERT(path);
	SDK_NULL_ASSERT(params);
	path->params = *params;
	A_STAR_HeapInit(&path->open, path->params.maxNodesPool, _A_STAR_cmp_nodes);
	A_STAR_HeapInit(&path->close, path->params.maxNodesPool, _A_STAR_cmp_nodes);
	path->pool = (struct AStarPathNode**)MALLOC(sizeof(struct AStarPathNode*) * path->params.maxNodesPool, "A_STAR_InitPath::heap->arr");
	for(i = 0; i < path->params.maxNodesPool; i++)
	{
		path->pool[i] = (struct AStarPathNode*)MALLOC(sizeof(struct AStarPathNode), "A_STAR_InitPath::heap->arr[i]");
	}
	path->dest_node = NULL;
	path->pool_top = 0;
}
//-------------------------------------------------------------------------------------------

void A_STAR_ReleasePath(struct AStarPath *path)
{
	SDK_NULL_ASSERT(path);
	if(path->pool != NULL)
	{
		u32 i = path->params.maxNodesPool;
		while(i > 0)
		{
			i--;
			FREE(path->pool[i]);
		}
		FREE(path->pool);
	}
	A_STAR_HeapRelease(&path->close);
	A_STAR_HeapRelease(&path->open);
	path->pool = NULL;
	path->pool_top = 0;
	path->dest_node = NULL;
}
//-------------------------------------------------------------------------------------------

void A_STAR_ChangeCostsMap(struct AStarPath *path, const u8 *map, u16 map_w, u16 map_h)
{
	SDK_NULL_ASSERT(path);
	SDK_NULL_ASSERT(map);
	path->params.map = map;
	path->params.map_h = map_h;
	path->params.map_w = map_w;
}
//-------------------------------------------------------------------------------------------

void A_STAR_FindPath(struct AStarPath *path, u16 orig_x, u16 orig_y, u16 dest_x, u16 dest_y)
{
	const struct AStarPathNode *cur;
	SDK_NULL_ASSERT(path);
	SDK_NULL_ASSERT(path->params.map);

	path->orig_x = orig_x;
	path->orig_y = orig_y;
	path->dest_x = dest_x;
	path->dest_y = dest_y;
	
	path->dest_node = NULL;
	path->pool_top = 0;
	A_STAR_HeapClear(&path->close);
	A_STAR_HeapClear(&path->open);

	_A_STAR_add_node_to_path(path, orig_x, orig_y, NULL);

	cur = A_STAR_HeapPop(&path->open);
	while(cur)
	{
		A_STAR_HeapPush(&path->close, cur);
		if(path->dest_x == cur->x && path->dest_y == cur->y)
		{
			path->dest_node = cur;
			break;
		}
		if(path->params.use_diag)
		{
			_A_STAR_add_adjacent_nodes_to_path(path, cur);
		}
		else
		{
			_A_STAR_add_adjacent_nodes_to_path_check_diag(path, cur);
		}
		cur = A_STAR_HeapPop(&path->open);
	}
}
//-------------------------------------------------------------------------------------------

static s32 _A_STAR_cmp_nodes(const struct AStarPathNode *p1, const struct AStarPathNode *p2)
{
	return _A_STAR_is_best_node(p1, p2) ? -1 : 1;
}
//-------------------------------------------------------------------------------------------

static void _A_STAR_add_node_to_path(struct AStarPath *path, u16 x, u16 y, const struct AStarPathNode *parent)
{
	u32 c;
	u8 g_cost; 
	struct AStarPathNode *node;
	
	if(path->pool_top == path->params.maxNodesPool)
	{
		SDK_ASSERT(0); // please increase pool size on A_STAR_InitPath
		return;
	}

	node = path->pool[path->pool_top];
	node->x = x;
	node->y = y;
	node->parent = parent;

	SDK_ASSERT(x < path->params.map_w && y < path->params.map_h); 

	/* can't add the node here */
	g_cost = path->params.map[node->y * path->params.map_w + node->x];
	if(g_cost == 0xff)
	{
		return;
	}

	/* is in the closed list */
	for(c = 0; c < path->close.top; c++)
	{
		if(path->close.arr[c]->x == node->x && path->close.arr[c]->y == node->y)
		{
			return;
		}
	}

	_A_STAR_recalc_node_g(node);
	_A_STAR_recalc_node_h(node);

	/* is in the opened list? */
	for(c = 0; c < path->open.top; c++)
	{
		if(path->open.arr[c]->x == node->x && path->open.arr[c]->y == node->y)
		{
			/* if the new node is a best path */
			if(_A_STAR_is_best_node(node, path->open.arr[c]))
			{
				/* replace the parent of the node that is already in the
					opened list, and recalculate the movement cost */
				path->open.arr[c]->parent = node->parent;
				_A_STAR_recalc_node_g(path->open.arr[c]);
				A_STAR_HeapRearrange(&path->open, c);
				return;
			}
			else 
			{
				return;
			}
		}
	}
	path->pool_top++;
	A_STAR_HeapPush(&path->open, node);
}
//-------------------------------------------------------------------------------------------

#define _A_STAR_ADD_NODE(dx, dy) \
	_A_STAR_add_node_to_path(path, node->x + dx, node->y + dy, node)

//-------------------------------------------------------------------------------------------

static void _A_STAR_add_adjacent_nodes_to_path(struct AStarPath *path, const struct AStarPathNode *node)
{
	if(node->y > 0)
	{
		if(node->x > 0)
		{
			_A_STAR_ADD_NODE(-1, -1);
		}
		_A_STAR_ADD_NODE(0, -1);
		if(node->x < path->params.map_w - 1)
		{
			_A_STAR_ADD_NODE(+1, -1);
		}
	}

	if(node->x > 0)
	{
		_A_STAR_ADD_NODE(-1, 0);
	}
	if(node->x < path->params.map_w - 1)
	{
		_A_STAR_ADD_NODE(+1, 0);
	}

	if(node->y < path->params.map_h - 1)
	{
		if(node->x > 0)
		{
			_A_STAR_ADD_NODE(-1, +1);
		}
		_A_STAR_ADD_NODE(0, +1);
		if(node->x < path->params.map_w - 1)
		{
			_A_STAR_ADD_NODE(+1, +1);
		}
	}
}
//-------------------------------------------------------------------------------------------

static void _A_STAR_add_adjacent_nodes_to_path_check_diag(struct AStarPath *path, const struct AStarPathNode *node)
{
	if(node->y > 0)
	{
		_A_STAR_ADD_NODE(0, -1);
		if(path->params.map[(node->y - 1) * path->params.map_w + node->x] != 0xff)
		{
			if(node->x > 0 && path->params.map[node->y * path->params.map_w + node->x - 1] != 0xff)
			{
				_A_STAR_ADD_NODE(-1, -1);
			}
			if(node->x < path->params.map_w - 1 && path->params.map[node->y * path->params.map_w + node->x + 1] != 0xff)
			{
				_A_STAR_ADD_NODE(+1, -1);
			}
		}
	}

	if(node->x > 0)
	{
		_A_STAR_ADD_NODE(-1, 0);
	}
	if(node->x < path->params.map_w - 1)
	{
		_A_STAR_ADD_NODE(+1, 0);
	}

	if(node->y < path->params.map_h - 1)
	{
		_A_STAR_ADD_NODE(0, +1);
		if(path->params.map[(node->y + 1) * path->params.map_w + node->x] != 0xff)
		{
			if(node->x > 0 && path->params.map[node->y * path->params.map_w + node->x - 1] != 0xff)
			{
				_A_STAR_ADD_NODE(-1, +1);
			}
			if(node->x < path->params.map_w - 1 && path->params.map[node->y * path->params.map_w + node->x + 1] != 0xff)
			{
				_A_STAR_ADD_NODE(+1, +1);
			}
		}
	}
}
//-------------------------------------------------------------------------------------------
