#include "memory.h"
#include "as_path.h"
#include "as_heap.h"

//-------------------------------------------------------------------------------------------
// this code based on
// Path Finding Demo 2004 David A. Capello.
//-------------------------------------------------------------------------------------------

#define AS_SWAP_ITEMS(x, y) \
	t = heap->arr[(x)]; \
	heap->arr[(x)] = heap->arr[(y)]; \
	heap->arr[(y)] = t; \
	(x) = (y);

//-------------------------------------------------------------------------------------------

void A_STAR_HeapInit(struct AStarHeap *heap, u32 maxSize, s32 (*cmpfn)(const struct AStarPathNode *p1, const struct AStarPathNode *p2))
{
	SDK_NULL_ASSERT(heap);
	SDK_NULL_ASSERT(cmpfn);
	heap->top = 0;
	heap->cmpfn = cmpfn;
	heap->size = maxSize;
	heap->arr = (struct AStarPathNode**)MALLOC(sizeof(struct AStarPathNode*) * maxSize, "A_STAR_HeapInit::heap->arr");
}
//-------------------------------------------------------------------------------------------

void A_STAR_HeapRelease(struct AStarHeap *heap)
{
	SDK_NULL_ASSERT(heap);
	if(heap->arr)
	{
		FREE(heap->arr);
	}
	heap->size = 0;
	heap->arr = NULL;
	heap->top = 0;
}
//-------------------------------------------------------------------------------------------

void A_STAR_HeapClear(struct AStarHeap *heap)
{
	heap->top = 0;
}
//-------------------------------------------------------------------------------------------

void A_STAR_HeapPush(struct AStarHeap *heap, const struct AStarPathNode *ptr)
{
	s32 c, p;
	struct AStarPathNode *t;
	SDK_NULL_ASSERT(heap);
	SDK_NULL_ASSERT(ptr);
	if(heap->top == heap->size)
	{
		SDK_ASSERT(0); // please increase heap size on A_STAR_HeapInit
		return;
	}
	heap->arr[c = heap->top++] = (struct AStarPathNode *)ptr;
	while(c > 0 && (*heap->cmpfn)(heap->arr[c], heap->arr[p = (c + 1) / 2 - 1]) < 0)
	{
		AS_SWAP_ITEMS(c, p);
	}
}
//-------------------------------------------------------------------------------------------

void A_STAR_HeapRearrange(struct AStarHeap *heap, s32 c)
{
	s32 p;
	struct AStarPathNode *t;
	SDK_NULL_ASSERT(heap);
	while(c > 0 && (*heap->cmpfn)(heap->arr[c], heap->arr[p = (c + 1) / 2 - 1]) < 0)
	{
		AS_SWAP_ITEMS(c, p);
	}
}
//-------------------------------------------------------------------------------------------

const struct AStarPathNode* A_STAR_HeapPop(struct AStarHeap *heap)
{ 
	SDK_NULL_ASSERT(heap);
	if(heap->top > 0)
	{
		u32 c, ch1, ch2;
		struct AStarPathNode *t;
		const struct AStarPathNode *r = heap->arr[0];
		heap->arr[c = 0] = heap->arr[--heap->top];
		for(;;)
		{
			ch1 = (c + 1) * 2 - 1;
			ch2 = ch1 + 1;
			if(ch1 < heap->top)
			{
				if(ch2 < heap->top) 
				{
					if((*heap->cmpfn)(heap->arr[ch1], heap->arr[ch2]) < 0)
					{
						if((*heap->cmpfn)(heap->arr[c], heap->arr[ch1]) > 0)
						{
							AS_SWAP_ITEMS(c, ch1);
						}
						else
						{
							break;
						}
					}
					else
					{
						if ((*heap->cmpfn)(heap->arr[c], heap->arr[ch2]) > 0)
						{
							AS_SWAP_ITEMS(c, ch2);
						}
						else
						{
							break;
						}
					}
				}
				else if ((*heap->cmpfn) (heap->arr[c], heap->arr[ch1]) > 0)
				{
					AS_SWAP_ITEMS(c, ch1);
				}
				else
				{
					break;
				}
			}
			else
			{
				break;
			}
		}
		return r;
	}
	return NULL;
}
//-------------------------------------------------------------------------------------------
