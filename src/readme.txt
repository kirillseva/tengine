Начало работы
-------------


1. Настройка переменной окружения PATH (для пользователей windows):
-------------------------------------------------------------------

win32 ветка
-----------
Настройка переменных окружений не требуется

android ветка
-------------
Для работы с android веткой нужно скачать android SDK (http://developer.android.com/sdk/index.html) и 
NDK (http://developer.android.com/tools/sdk/ndk/index.html), JRE и JDK (http://www.oracle.com/technetwork/java/javase/downloads/index.html)
- Создать переменную окружения ANDROID_HOME, прописать путь к корневой папке android_sdk
  (пример d:\Android\android_sdk\)
- Создать переменную окружения ANDROID_NDK, прописать путь к корневой папке android_ndk
  (пример d:\Android\android_ndk\)
- Создать переменную окружения JAVA_HOME, прописать путь к корневой папке jdk
  (пример c:\Program Files (x86)\Java\jdk1.7\)
- В переменную окружения PATH прописать путь к android_ndk

опционально:
ADT (http://developer.android.com/sdk/index.html) или eclipse standart (http://www.eclipse.org/downloads),
cygwin (http://www.cygwin.com). В переменную окружения PATH прописать путь к cygwin/bin.

web (emscripten) ветка
----------------------
- Установить Emscripten-SDK (https://github.com/kripken/emscripten/wiki/Emscripten-SDK)
- Создать переменную окружения EMSCRIPTEN_HOME, прописать путь к корневой папке Emscripten
  (пример d:\Emscripten\)
- Создать переменную окружения EMSCRIPTEN, прописать путь к emcc
  (пример d:\Emscripten\emscripten\1.27.0)
  
kolibrios ветка
---------------
- Установить toolchain, libc, [Qemu] по инструкции tengine\samples\scroll_map\_kolibrios\readme.txt
- Создать переменную окружения KOLIBRIOS_HOME, прописать путь в unix формате к корневой папке SDK
  (пример /d/kolibri)
  
2. Настройка среды разработки:
------------------------------
- добавить в includes путь к папке tengine\src\tengine\  
Здесь находятся файлы, необходимые для работы библиотеки tengine

- добавить в includes путь к папке tengine\src\gui\
Здесь находятся файлы gui-надстройки для tengine, использование этой надстройки не обязательно

- добавить в includes путь к папке tengine\src\gamefield\
Здесь находится файл gamefield.h, необходимый для работы приложения на базе tengine,
содержит интерфейсные функции точка входа приложения. Реализация этих функций является необходимым условием для работы приложения.

- добавить в includes путь к папке tengine\src\_win32 (или, в зависимости от платформы,
соответственно tengine\src\_android, tengine\src\_ios, tengine\src\_emscripten)
Здесь находится платформо-зависимый код

- по умолчанию tengine настроен на использование статической библиотеки pthread.
Для win32 среды используется pthread-win32 библиотека, которая находится по пути tengine\lib\win32\pthreads\ (*.h файлы и pthread.lib).
Более новые версии библиотеки можно скачать по адресу http://www.sourceware.org/pthreads-win32. Для отключения мультипоточности
можно установить ключ препроцессора JOBS_IN_SINGLE_THREAD.

- по умолчанию tengine настроен на использование библиотеки openAl.
Для win32 среды используется openAl, которая находится по пути tengine\lib\win32\openAL\ 
(include\AL\*.h, Win32\OpenAL32.lib, Win32\OpenAL32.dll). Более новые версии библиотеки можно скачать по адресу 
http://connect.creativelabs.com/openal/default.aspx или http://kcat.strangesoft.net/openal.html#download.
Внимание: в win32 без установленной OpenAL32.dll многие примеры могут не запускаться и выдавать ошибку.
		Не забудьте скопировать OpenAL32.dll в папку windows\system32\

- для сборки android-ветки используется инструментарий Ant, находится по пути tengine\tools\ant\. 
Более новые версии Ant можно скачать по адресу http://ant.apache.org

- с помощью утилиты android_sdk\SDK Manager.exe скачать Tools, Android 2.3.3 (api10) и Extras


3. Настройка среды разработки, ключи препроцессора:
---------------------------------------------------
JOBS_IN_SINGLE_THREAD
JOBS_IN_SEPARATE_THREAD

по умолчанию включен tengine JOBS_IN_SEPARATE_THREAD, который определяет выполнение загрузки стримминговой 
(постоянно подгружаемой) анимации и фоновой музыки. В будущем планируется развивать функциональные задачи, исполняемые в отдельном потоке. 

SDK_DEBUG

по умолчанию tengine не использует SDK_DEBUG, определяет включение дебажного функционала, 
функций SDK_ASSERT и SDK_NULL_ASSERT, а так же работу функций OS_Printf и OS_Warning (вывод дебажных сообщений)

FRAME_ALLOCATOR

по умолчанию tengine не использует FRAME_ALLOCATOR, работает только при включенном SDK_DEBUG. 
Не обязательный, но крайне рекомендованный параметр, позволяющий не допустить утечку и фрагментацию 
оперативной памяти. Включает функционал, контролирующий принцип FRAME_ALLOCATOR, вся выделенная 
динамическая память должна освобождаться в строго обратном порядке. При несоблюдении принципа, 
при использовании оператора FREE будет выдаваться assert. Пример кода можно посмотреть в примере 
scroll_map (tengine\samples\scroll_map\game\gamefield.c)

USE_STATIC_MEMORY

по умолчанию tengine не использует USE_STATIC_MEMORY, определяет работу функций MALLOC и FREE, 
переключает их в режим использования указанного статического пула. При использовании данного ключа в 
функцию инициализации памяти InitMemoryAllocator() обязательными параметрами указываются указатель на 
статический пул и его размер в байтах

NITRO_SDK
ANDROID_NDK
IOS_APP
WINDOWS_APP
NIX_APP
EMSCRIPTEN_APP

по умолчанию tengine не использует ни один их перечисленных параметром, но установка одно их этих трех параметров обязательна

DRAW_DEBUG_COLLIDERECTS
DRAW_DEBUG_VIEWRECTS

по умолчанию tengine не использует ни один их перечисленных параметров, установка их включает отладочное 
отображение зон "столкновений" и зон "видимости" объектов

USE_OPENAL_SOUND
USE_SLES_SOUND
USE_NO_SOUND

по умолчанию tengine использует ключ USE_OPENAL_SOUND для всех платформ, 
кроме ANDROID_NDK. Для ANDROID_NDK автоматически устанавливается ключ USE_SLES_SOUND.
USE_NO_SOUND отключает использование звуковых библиотек

USE_FX32_AS_FLOAT
USE_FX32_AS_FIXED

по умолчанию tengine использует ключ USE_FX32_AS_FLOAT, определяет тип и размерность типа данных fx32.
При включенном USE_FX32_AS_FLOAT тип fx32 является double, стандартным типом хранения значений с плавающей точкой.
Ключ USE_FX32_AS_FIXED определяет тип fx32 как fixed, в этом случае способ представления действительных чисел происходит
в формате int, все операции над fx32 являются целочисленными и не используют FPU (сопроцессор)


4. Типы данных и их особенности 
-----------------------------------
Так как tengine представляет собой мультиплатформенное решение:

все целочисленные типы переопределены:

u8, u16, s16, u32, s64, u64, s32
префиксы u - беззнаковое, s - знаковое


булевый тип:

BOOL, принимает значение FALSE или TRUE


для операций над числами с плавающей точкой применяется "фиксированный" тип
(тип float использовать не рекомендуется, так как возможны проблемы с переносимостью кода)

fx32

функции для работы с fx32:
FX32(x) - преобразует float в fx32, например FX32(0.2f);
FX_Mul(x, y) - умножить fx32
FX_Div(x, y) - разделить fx32

двумерный вектор из fixed:
fxVec2 

функции для работы с fxVec2:

fxVec2 fxVec2Create(fx32 x, fx32 y)
fxVec2 fxVec2Add(fxVec2 const v1, fxVec2 const v2)
fxVec2 fxVec2Sub(fxVec2 const v1, fxVec2 const v2)
fxVec2 fxVec2Mul(fxVec2 const v1, fxVec2 const v2)
fxVec2 fxVec2MulFx(fxVec2 const v1, fx32 const val)
fxVec2 fxVec2DivFx(fxVec2 const v1, fx32 const val)
fx32 fxVec2Dot(fxVec2 const v1, fxVec2 const v2)
fx32 fxVec2Cross(fxVec2 const v1, fxVec2 const v2)

строковый тип:

char

функции для работы с char:
STD_StrLen()
STD_StrCpy()
STD_StrCmp()
STD_StrCmp()
STD_StrCat()

для работы с текстами tengine использует юникод, потому в проекте используется тип
(тип wchar_t использовать не рекомендуется, так как возможны проблемы с переносимостью кода, например под android)

wchar

функции для работы с wchar:
STD_WStrLen()
STD_WSprintf()
STD_WStrCmp()
STD_WStrCpy()
STD_WStrNCmp()
STD_WStrStr()


для работы с цветом используют типы
(tengine использует 16-битный цвет формата 5551)

GXRgba
GXRgb

функция для работы с GXRgba и GXRgb 
GX_RGBA(r, g, b, a) r, g, b - в пределах 0..31, a - в пределах 0..1

для работы с указателями tengine использует функции:

MI_CpuFill8()
MI_CpuFill16()
MI_CpuClear8()
MI_CpuClear32()
MI_CpuClearFast()
MI_CpuCopy8()
MI_CpuCopy16()
MI_CpuCopy32()
MI_CpuCopyFast()

для дебажных исключений используются:

SDK_ASSERT()
SDK_NULL_ASSERT()

для дебажных сообщений tengine использует функции:

OS_Printf()
OS_Warning()

Все типы и основные операции с ними описаны в файле tengine\src\tengine\platform.h


5. Создаем файл gamefield.c (или gamefield.cpp)
-----------------------------------------------
gamefield.c необходим для работы приложения, в нем нужно реализовать функции gamefield.h

#include "tengine.h"
#include "gamefield.h"

// вызывается в момент создания приложения для инициализации памяти,
// в зависимости от установленного ключа препроцессора в этой функции нужно
// проинициализировать системную память настройками по умолчанию или указанным статическиим пулом   
void tfgInitMemory()
{
}

// вызывается в момент создания приложения, после инициализации файловой системы и рендеринга
void tfgInit()
{
}

// вызывается при завершении приложения, перед деинициализацией файловой системы и рендеринга
void tfgRelease()
{
}

// вызывается в момент инициации размеров окна приложения
// платформеннозависимая функция, операционные системы win32, web(emscripten), kolibrios
void tfgSetWindowModeSize(s32 *const w, s32 *const h)
{
}

// вызывается в момент изменения размеров физического экрана а также в момент создания, после init()
void tfgResize(s32 w, s32 h)
{
}

// вызывается в момент потери физического рендер-устройства (например, "потухания" экрана телефона)
// внимание: после вызова этой функции система автоматически высвобождает графические ресурсы
//                    код в этой функции не должен быть громоздким или долго исполняться
void tfgLostRenderDevice()
{
}

// вызывается в момент обретения физического рендер-устройства
// внимание: до вызова этой функции система автоматически загружает освобожденные графические ресурсы
void tfgRestoreRenderDevice()
{
}

// вызывается в момент, когда операционная система устройства требует освободить память под более 
// приоритетные задачи (платформеннозависимая функция)
void tfgLowMemory()
{
}

// вызывается постоянно, является mainloop приложения,
// ms - значение в микросекундах между вызовами фукции
void tfgTick(s32 ms)
{
}


6. Документация по API находится в tengine\src\tengine\manual папке
-------------------------------------------------------------------
Также читайте туториал по созданию небольшой игры в tengine\samples\space_invaders\how_to_make_a_game_tutorial.txt