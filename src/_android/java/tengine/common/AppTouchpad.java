/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

package tengine.common;

import android.view.MotionEvent;

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------

class point_item
{
	public float x;
	public float y;
	public boolean check = false;
	public int id = -1;
};
//-----------------------------------------------------------------------

public class AppTouchpad
{
	private static final int MAX_POINTS = 10;
	
	private static native void nativeTouchPadDown(int ptid, float x, float y);
    private static native void nativeTouchPadUp(int ptid, float x, float y);
    private static native void nativeTouchPadMove(int ptid, float x, float y);
    
    private static point_item[] mPoints = null;
    
//-----------------------------------------------------------------------
    
    public static void init()
    {
    	if(mPoints == null)
    	{
    		mPoints = new point_item[MAX_POINTS];
	    	for(int p = 0; p < MAX_POINTS; p++)
			{
	    		mPoints[p] = new point_item(); 
			}
    	}
    }
//-----------------------------------------------------------------------

    public static boolean onTouchEventV2(MotionEvent mCurrentEvent)
    {
    	int a, p, ptid;
    	final int pointerCount = mCurrentEvent.getPointerCount();
    	final int action = mCurrentEvent.getAction();
    	switch(action & MotionEvent.ACTION_MASK)
		{
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_POINTER_UP:
			case MotionEvent.ACTION_CANCEL:
			{
				a = (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
	    		final int ptid_u = mCurrentEvent.getPointerId(a);
		    	for (a = 0; a < pointerCount; a++)
		        {
			    	ptid = mCurrentEvent.getPointerId(a);
		        	for(p = 0; p < MAX_POINTS; p++)
					{
						if(ptid == mPoints[p].id && ptid_u != ptid)
						{
							mPoints[p].check = true;		        
						}
					}
		        }
			}
			break;
			default:
		    	for (a = 0; a < pointerCount; a++)
		        {
		        	ptid = mCurrentEvent.getPointerId(a);
					float x = mCurrentEvent.getX(a);
					float y = mCurrentEvent.getY(a);
		        	boolean add = true;
		        	for(p = 0; p < MAX_POINTS; p++)
					{
						if(mPoints[p].id == ptid)
						{
							add = false;
							mPoints[p].x = x;
							mPoints[p].y = y;
							mPoints[p].check = true;
							nativeTouchPadMove(p, x, y);		        
							//System.out.println(" ACTION_MOVE " + p + " x = " + x + " y =" + y);
						}
					}
					for(p = 0; p < MAX_POINTS && add; p++)
					{
						if(mPoints[p].id == -1)
						{
							mPoints[p].id = ptid;
							mPoints[p].x = x;
							mPoints[p].y = y;
							mPoints[p].check = true;
							nativeTouchPadDown(p, x, y);
							//System.out.println(" ACTION_DOWN " + ptid + ", " + p + " x = " + x + " y =" + y);
							break;
						}
					}
		        }	
		}
		for(p = 0; p < MAX_POINTS; p++)
		{
			if(mPoints[p].id != -1 && mPoints[p].check == false)
			{
				nativeTouchPadUp(p, mPoints[p].x, mPoints[p].y);
				mPoints[p].id = -1;
				//System.out.println(" ACTION_UP " + p + " x = " + mPoints[p].x + " y =" + mPoints[p].y);
			}
			mPoints[p].check = false;
		}
    	return true;
    }
}

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------