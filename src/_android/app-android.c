/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include <jni.h>
#include <sys/time.h>
#include <time.h>
#include <android/log.h>
#include <android/asset_manager_jni.h>
#include <stdint.h>

#include "lib/platform_low.h"
#include "lib/filesystem_low.h"
#include "gamefield.h"
#include "filesystem.h"
#include "lib/tengine_low.h"
#include "lib/render.h"
#include "lib/a_touchpad.h"
#include "profiler/profile.h"
#include "texts.h"
#ifdef PROFILER_ENABLE
#include "static_allocator.h"
#endif
#ifdef NDK_NATIVE_API10
#include "android_native_app_glue.h"
#endif

struct AAssetManager *gpAssetManager = NULL;
char gInternalPath[MAX_INTERNALFILEPATH] = {0};
static struct TEnginePlatformData* gspPlatformData = NULL;

#ifdef PROFILER_ENABLE
#define PROFILER_STATIC_ALLOCATOR_HEAP_SIZE (1024 * 1024 * 4)
static struct StaticAllocator sProfilerStaticAllocator;
static u8 sStaticAllocatorHeap[PROFILER_STATIC_ALLOCATOR_HEAP_SIZE];
#endif

#ifdef __cplusplus
extern "C" {
#endif
 JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeInit(JNIEnv* env, jobject o, jobject assetManager, jstring internalDataPath);
 JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeRelease(JNIEnv* env, jobject o, jint is_finish);
 JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativePause(JNIEnv* env);
 JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeResume(JNIEnv* env);
 JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeStop(JNIEnv* env);
 JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeLowMemory(JNIEnv* env);

 JNIEXPORT void JNICALL Java_tengine_common_AppTouchpad_nativeTouchPadDown(JNIEnv* env, jobject o, jint ptid, jfloat x, jfloat y);
 JNIEXPORT void JNICALL Java_tengine_common_AppTouchpad_nativeTouchPadUp(JNIEnv* env, jobject o, jint ptid, jfloat x, jfloat y);
 JNIEXPORT void JNICALL Java_tengine_common_AppTouchpad_nativeTouchPadMove(JNIEnv* env, jobject o, jint ptid, jfloat x, jfloat y);
 JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeKeyDown(JNIEnv* env, jobject o, jint keyCode);
 JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeKeyUp(JNIEnv* env, jobject o, jint keyCode);
 
 JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeCreateRenderDevice(JNIEnv* env);
 JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeResize(JNIEnv* env, jobject o, jint w, jint h);
 JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeTick(JNIEnv* env);
#ifdef __cplusplus
} /* extern "C" */
#endif

//-----------------------------------------------------------------------

#ifdef NDK_NATIVE_API10
void android_main(struct android_app* app)
{
	(void)app;
}
#endif
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeInit(JNIEnv* env, jobject o, jobject assetManager, jstring internalDataPath)
{
	(void)o;
	const char* nativeString = (*env)->GetStringUTFChars(env, internalDataPath, NULL);
	STD_StrCpy(gInternalPath, nativeString);
	(*env)->ReleaseStringUTFChars(env, internalDataPath, nativeString);
	if(gspPlatformData == NULL)
	{
#ifdef PROFILER_ENABLE
		StaticAllocator_Init(&sProfilerStaticAllocator, sStaticAllocatorHeap, PROFILER_STATIC_ALLOCATOR_HEAP_SIZE);
		PROFILE_INIT(sProfilerStaticAllocator);
#endif
		gpAssetManager = AAssetManager_fromJava(env, assetManager);
		gspPlatformData = Platform_GetPlatformData();
		gspPlatformData->renderDevice = FALSE;
		gspPlatformData->tengineInit = FALSE;
		Platform_InitTick();
	}
	else if(gspPlatformData->tengineInit)
	{
		struct InitFileSystemData fileSystemData;
		gpAssetManager = AAssetManager_fromJava(env, assetManager);
		fileSystemData.mpAssetManager = gpAssetManager;
		InitFileSystem(&fileSystemData);
	}
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeCreateRenderDevice(JNIEnv* env)
{
	(void)env;
	OS_Printf("restore render device");
	Platform_restoreRenderDevice();
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeStop(JNIEnv* env)
{
	(void)env;
	OS_Printf("lost render device");
	Platform_lostRenderDevice();
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeLowMemory(JNIEnv* env)
{
	(void)env;
	OS_Printf("app low memory signal");
	tfgLowMemory();
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeRelease(JNIEnv* env, jobject o, jint is_finish)
{
	(void)env;
	(void)o;
	if(is_finish != 0)
	{
		if(gspPlatformData->tengineInit == TRUE)
		{
			tfgRelease();
			ReleaseFileSystem();
			glRender_Release();
		}
		gspPlatformData->tengineInit = FALSE;
	}
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativePause(JNIEnv* env)
{
	Java_tengine_common_AppGLSurfaceView_nativeStop(env);
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeResume(JNIEnv* env)
{
	(void)env;
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeResize(JNIEnv* env, jobject o, jint w, jint h)
{
	(void)env;
	(void)o;
	if(gspPlatformData->tengineInit)
	{
		tfgResize(w, h);
		glRender_Resize(w, h);
	}
	else
	{
		if(gspPlatformData->tengineInit == FALSE)
		{
			struct InitFileSystemData fileSystemData;
			
			tfgInitMemory();
			glRender_Init();

			fileSystemData.mpAssetManager = gpAssetManager;
			InitFileSystem(&fileSystemData);

			tfgInit();
			tfgResize(w, h);
			OS_Printf("init tengine display with size: %d %d", w, h);
			glRender_Resize(w, h);

			gspPlatformData->tengineInit = TRUE;
		}
	}
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppTouchpad_nativeTouchPadDown(JNIEnv* env, jobject o, jint ptid, jfloat x, jfloat y)
{
	(void)env;
	(void)o;
	onTouchPadDown((u8)ptid, (s32)x, (s32)y);
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppTouchpad_nativeTouchPadUp(JNIEnv* env, jobject o, jint ptid, jfloat x, jfloat y)
{
	(void)env;
	(void)o;
	onTouchPadUp((u8)ptid, (s32)x, (s32)y);
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppTouchpad_nativeTouchPadMove(JNIEnv* env, jobject o, jint ptid, jfloat x, jfloat y)
{
	(void)env;
	(void)o;
	onTouchPadMove((u8)ptid, (s32)x, (s32)y);
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeKeyDown(JNIEnv* env, jobject o, jint keyCode)
{
	(void)env;
	(void)o;
	(void)keyCode;
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeKeyUp(JNIEnv* env, jobject o, jint keyCode)
{
	(void)env;
	(void)o;
	(void)keyCode;
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeTick(JNIEnv* env)
{
	(void)env;
	PROFILE_START;
	Platform_Tick();
	PROFILE_FINISH;
}
//-----------------------------------------------------------------------
