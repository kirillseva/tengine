/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef _MEMORY_H_
#define _MEMORY_H_

#include "platform.h"

#ifdef __cplusplus
#include <new>
extern "C" {
#endif

//----------------------------------------------------------------------------------------------------------------

#ifdef SDK_DEBUG 
#ifdef FRAME_ALLOCATOR 
#define PTCOLLECTOR_SIZE 8000
#define PTCOLLECTOR_MARK_SIZE 64
#else
#define PTCOLLECTOR_MARK_SIZE 1
#endif
#else
#define PTCOLLECTOR_MARK_SIZE 1
#endif
#ifdef USE_STATIC_MEMORY
struct StaticAllocator;
#endif

#ifdef USE_STATIC_MEMORY
void InitMemoryAllocator(struct StaticAllocator *allocator);
#else
void InitMemoryAllocator(void);
#endif

void* MALLOC(u32 iSizeBytes, const char iMarkName[PTCOLLECTOR_MARK_SIZE]);

void  FREE(void* pPointer);

#ifdef STATIC_HEAP_MEM_DEBUG_DEEP
void DebugMemoryCheck();
#define DEBUG_MEMORY_CHECK DebugMemoryCheck()
#else
#define DEBUG_MEMORY_CHECK {}
#endif

u32 GetFreeMemorySize(void);

#if defined SDK_DEBUG && defined FRAME_ALLOCATOR
u32 GetTotalMALLOCSizeDbg(void);
#endif

//----------------------------------------------------------------------------------------------------------------

#ifndef NITRO_SDK

 void MI_CpuFill8(void *dest, u8 val, s32 sz);
 
 void MI_CpuFill16(void *dest, s16 val, s32 sz);
 
 void MI_CpuClear8(void *dest, s32 sz);
 
 void MI_CpuClear32(void *dest, s32 sz);
 
 void MI_CpuClearFast(void *dest, s32 sz);
 
 void MI_CpuCopy8(const void *src, void *dest, u32 sz);
 
 void MI_CpuCopy16(const void *src, void *dest, u32 sz);
 
 void MI_CpuCopy32(const void *src, void *dest, u32 sz);

 void MI_CpuCopyFast(const void *src, void *dest, u32 sz);

#endif

#ifdef __cplusplus
} /* extern "C" */
#ifndef IOS_APP
	//I don't know how to isolate overloaded operators new/delete from objective-c
#if defined NIX_APP || defined EMSCRIPTEN_APP || defined NITRO_SDK || defined KOLIBRIOS_APP
	#define THROW_BAD_ALLOC throw(std::bad_alloc)
	#define THROW_EMPTY throw()
#else
	//Android doesn't fully support exceptions, so its <new> header
	//has operators that don't specify throw() at all. Also include MSVC
	//to suppress build warning spam
	#define THROW_BAD_ALLOC

#if defined ANDROID_NDK || defined __BORLANDC__
	#define THROW_EMPTY _NOEXCEPT
#else
	#define THROW_EMPTY
#endif

	void* operator new(std::size_t isize, const std::nothrow_t&) THROW_EMPTY;

    void operator delete(void* ptr, const std::nothrow_t&) THROW_EMPTY;

    void* operator new[](std::size_t isize, const std::nothrow_t&) THROW_EMPTY;

    void operator delete[](void* ptr, const std::nothrow_t&) THROW_EMPTY;

#endif

void* operator new(std::size_t isize) THROW_BAD_ALLOC;

void operator delete(void* ptr) THROW_EMPTY;

void* operator new[](std::size_t isize) THROW_BAD_ALLOC;

void operator delete[](void* ptr) THROW_EMPTY;

#undef THROW_BAD_ALLOC
#undef THROW_EMPTY
#endif
#endif
//---------------------------------------------------------------------------
#endif //_MEMORY_H_
