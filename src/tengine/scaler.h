#ifndef __SCALER__H__
#define __SCALER__H__

#include "tengine.h"
#include "static_allocator.h"
#include "containers/allocator_list.h"

#ifndef SCALER_ALLOC_SIZE
#define SCALER_ALLOC_SIZE 100000
#endif

enum ScalerAlignHorisontal
{
	ScalerAlignHCenter = 0,
	ScalerAlignLeft,
	ScalerAlignRight
};

enum ScalerAlignVertical
{
	ScalerAlignVCenter = 0,
	ScalerAlignTop,
	ScalerAlignBottom
};

enum ScalerAnchorHorizontal
{
	ScalerAnchorHCenter = 0,
	ScalerAnchorLeft,
	ScalerAnchorRight
};

enum ScalerAnchorVertical
{
	ScalerAnchorVCenter = 0,
	ScalerAnchorTop,
	ScalerAnchorBottom
};

enum ScalerScaleType
{
	ScalerScaleNone,
	ScalerScaleHorizontal,
	ScalerScaleVertical,
	ScalerScaleBoth
};

struct scaler_s
{
	BOOL isInited;

	BOOL isNeedRecalc;
	BOOL isNeedRecalcAll;

	struct fxVec2 realSize;
	struct fxVec2 absSize;

	struct StaticAllocator allocator;
	struct AllocatorList items;

	u8 allocBuffer[SCALER_ALLOC_SIZE];
};

struct scaler_obj_s
{
	BOOL isInited;
	s32 objId;

	BOOL isNeedRecalc;

	struct fxVec2 realPos;
	struct fxVec2 realSize;
	struct fxVec2 realScale;

	enum ScalerAlignHorisontal horAlign;
	enum ScalerAlignVertical verAlign;
	enum ScalerAnchorHorizontal horAnchor;
	enum ScalerAnchorVertical verAnchor;

	enum ScalerScaleType scaleType;

	struct fxVec2 pos;
	struct fxVec2 size;
	struct fxVec2 scale;
};

typedef struct scaler_s* scaler_P;
typedef struct scaler_obj_s* scaler_obj_P;


void scaler_init(scaler_P scaler, s32 objMax);
void scaler_set_abs_size(scaler_P scaler, struct fxVec2);
void scaler_resize(scaler_P scaler, struct fxVec2 size);
scaler_obj_P scaler_add(scaler_P scaler, s32 objId);
s32 scaler_remove(scaler_P scaler, scaler_obj_P sobj);
void scaler_set_pos(scaler_P scaler, scaler_obj_P sobj, struct fxVec2 pos);
void scaler_set_size(scaler_P scaler, scaler_obj_P sobj, struct fxVec2 size);
void scaler_set_scale(scaler_P scaler, scaler_obj_P sobj, struct fxVec2 scale);

void scaler_set_anchor(scaler_P scaler, scaler_obj_P sobj, enum ScalerAnchorHorizontal hAnchor, enum ScalerAnchorVertical vAnchor);
void scaler_set_align(scaler_P scaler, scaler_obj_P sobj, enum ScalerAlignHorisontal hAlign, enum ScalerAlignVertical vAlign);
void scaler_set_scale_type(scaler_P scaler, scaler_obj_P sobj, enum ScalerScaleType scaleType);

s32 scaler_get_objid(scaler_P scaler, scaler_obj_P sobj);

void scaler_update(scaler_P scaler);

#endif
