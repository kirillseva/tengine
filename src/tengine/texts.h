/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef TEXTS_H
#define TEXTS_H

#include "platform.h"

#ifdef __cplusplus
extern "C" {
#endif

struct TERFont;

enum TERFontInfoField
{
	TERF_IS_SMOOTH = 0,
	TERF_IS_UNICODE = 1,
	TERF_IS_ITALIC = 4,
	TERF_IS_BOLD = 8,
	TERF_IS_FIXEDHEIGHT = 16
};

//----------------------------------------------------------------------------------------------------------------

#ifndef NITRO_SDK

s32 STD_StrLen(const char *str);

char* STD_StrCpy(char *dest, const char *src);

s32 STD_StrCmp(const char *a, const char *b);

s32 STD_StrCmp(const char *a, const char *b);

char *STD_StrCat(char *str1, const char *str2);

#endif

void STD_NumToString(u16 num, char *buf, u16 bufSize);

s32 STD_WStrLen(const wchar *str);

/* extra specifier 'F' character defines the type fx32 */
s32 STD_WSnprintf(wchar *buf, s32 buf_size, const wchar *fmt, ...);

s32 STD_WStrCmp(const wchar *cs, const wchar *ct);

s32 STD_WStrNCmp(const wchar *s1, const wchar *s2, s32 n);

wchar* STD_WStrStr(wchar *s, const wchar *find);

wchar* STD_WStrCpy(wchar *str1, const wchar *str2);

wchar* STD_WItoA(wchar *s, s32 n);

s32 STD_WStrtoI(const wchar *str, wchar **endptr);

const char* Font_GetName(const struct TERFont* font);

u16 Font_GetSize(const struct TERFont* font);

/* bit oparations with TERFontInfoField enum */
u16 Font_GetInfoBitField(const struct TERFont* font);

//----------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif //TEXTS_H
