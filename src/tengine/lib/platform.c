/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "gamefield.h"
#include "platform_low.h"
#include "tengine_low.h"
#include "sound_low.h"
#include "filesystem_low.h"
#include "texts.h"
#include "render.h"
#include "fxmath.h"
#include "jobs.h"
#include "jobs_low.h"
#include "loadtdata.h"
#include "touchpad.h"
#include "gamepad.h"
#if defined ANDROID_NDK || defined EMSCRIPTEN_APP || defined IOS_APP
#include <time.h>
#include <sys/time.h>
#endif
#ifdef KOLIBRIOS_APP
#include "kos32sys.h"
#endif
#ifdef EMSCRIPTEN_APP
#include <GLFW/glfw3.h>
#endif

static void (*_tickFn)(void) = NULL;
static void _tickInitial(void);
static void _tickMain(void);
#ifndef DISABLE_FPS_COUNTER
static void _processFPSCounter(fx32 ms_fx);
#endif

extern struct TEngineCommonData cd;
struct TEnginePlatformData gsEnginePlatformData = {0};

#if defined USE_FX32_AS_FLOAT
static const s32 FIXED_TIMESTEP_MS = (s32)FIXED_TIMESTEP_MS_FX;
#else
static const s32 FIXED_TIMESTEP_MS = FIXED_TIMESTEP_MS_FX >> FX32_SHIFT;
#endif

u32 Platform_GetTime()
{
#ifdef WINDOWS_APP
	LARGE_INTEGER s_frequency;
	const BOOL use_qpf = QueryPerformanceFrequency(&s_frequency);
	if(use_qpf)
	{
		LARGE_INTEGER now;
		QueryPerformanceCounter(&now);
		return (u32)((1000LL * now.QuadPart) / s_frequency.QuadPart);
	}
	else
	{
		return (u32)GetTickCount();
	}
#elif defined ANDROID_NDK || defined EMSCRIPTEN_APP || defined IOS_APP
    struct timeval now;
    gettimeofday(&now, NULL);
    return (u32)(now.tv_sec * 1000 + now.tv_usec / 1000);
#elif defined KOLIBRIOS_APP
	return (u32)(get_ns_count() / 1000000);
#else
	return 0;
#endif
}
//---------------------------------------------------------------------------

struct TEnginePlatformData* Platform_GetPlatformData()
{
	return &gsEnginePlatformData;
}
//-------------------------------------------------------------------------------------------

void Platform_lostRenderDevice()
{
	if(gsEnginePlatformData.renderDevice == TRUE)
	{
		low_SetDummyDrawFunctions();
		gsEnginePlatformData.renderDevice = FALSE;
		tfgLostRenderDevice();
		RENDERFN(LostDevice)();
		cd.lost_render_device_is_jobs_active = jobIsActive();
		jobSetActive(FALSE);
		_freeRes(&cd);
		if(sndIsSoundSystemInit())
		{
			sndLostDevice();
		}
		if(FILESYSTEMFN(IsFileSystemInit)())
		{
			FILESYSTEMFN(lostDevice)();
		}
		ResetGamePad();
		ResetTouchPad();
	}
}
//-------------------------------------------------------------------------------------------

void Platform_restoreRenderDevice()
{
	if(gsEnginePlatformData.renderDevice == FALSE)
	{
		gsEnginePlatformData.renderDevice = TRUE;
		if(sndIsSoundSystemInit())
		{
			sndRestoreDevice();
		}
		cd.ag_st = TRUE;
		jobSetActive(cd.lost_render_device_is_jobs_active);
		RENDERFN(RestoreDevice)();
		tfgRestoreRenderDevice();
	}
}
//-------------------------------------------------------------------------------------------

void Platform_InitTick()
{
	gsEnginePlatformData.tickPreviousTime = Platform_GetTime();
	_tickFn = _tickInitial;
}
//-------------------------------------------------------------------------------------------

void _tickInitial()
{
	gsEnginePlatformData.tickPreviousTime = Platform_GetTime();
	tfgTick(FIXED_TIMESTEP_MS);
	if(cd.instances != NULL)
	{
		_tickFn = _tickMain;
	}
}
//-------------------------------------------------------------------------------------------

void _tickMain()
{
	u32 var = Platform_GetTime();
	const fx32 ms = FX32(var - gsEnginePlatformData.tickPreviousTime); 
	gsEnginePlatformData.timeAccumulatorMSFx += ms; 
	gsEnginePlatformData.tickPreviousTime = var;
	if(DELAY_LIMIT_MS_FX < gsEnginePlatformData.timeAccumulatorMSFx || ms < 0)
	{
		gsEnginePlatformData.timeAccumulatorMSFx = DELAY_LIMIT_MS_FX;
	}
#ifndef DISABLE_FPS_COUNTER
	_processFPSCounter(ms);
#endif
#if defined ANDROID_NDK || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP
	if(gsEnginePlatformData.renderDevice && gsEnginePlatformData.tengineInit)
#else
	if(gsEnginePlatformData.renderDevice)
#endif
	{	
		if(cd.ld_st == ALS_STATE_LOAD_DATA || cd.ag_st)
		{
			_updateLoadingProcess();
#ifndef JOBS_IN_SEPARATE_THREAD 
			jobUpdateFileStreamTasks();
#endif
		}
#ifndef JOBS_IN_SEPARATE_THREAD
			jobUpdateAudioStreamTasks();
#endif
		var = 0;
		low_RestoreDrawFunctions();
		while(gsEnginePlatformData.timeAccumulatorMSFx >= FIXED_TIMESTEP_MS_FX)
		{
			u32 layer = 0;
			gsEnginePlatformData.timeAccumulatorMSFx -= FIXED_TIMESTEP_MS_FX;
			tfgTick(FIXED_TIMESTEP_MS);
			if(var == 0)
			{
				++var;
				for(; layer < cd.initParams.layersCount; layer++)
				{
					struct TEngineInstance* ei = cd.instances[layer];
					cd._updateEngineObjFn[(ei->ueof_state & ei->visible_mask) << cd.pause_engine](ei, layer, FIXED_TIMESTEP_MS);
				}
			}
			else
			{
				low_SetDummyDrawFunctions();
				for(; layer < cd.initParams.layersCount; layer++)
				{
					struct TEngineInstance* ei = cd.instances[layer];
					cd._updateEngineObjFn[(ei->ueof_state & ei->t_visible_mask) << cd.pause_engine](ei, layer, FIXED_TIMESTEP_MS);
				}
			}
		}

#ifdef USE_CUSTOM_RENDER
		if(egRender_IsTransferDataVRAMChanged())
		{
			egRender_TransferDataToVRAM();
		}
#elif defined USE_OPENGL_RENDER
#ifdef IOS_APP
        //nope
#else
		glRender_DrawFrame();
#endif
#endif
#ifdef WINDOWS_APP
		SwapBuffers(gsEnginePlatformData.hDC);
#elif defined EMSCRIPTEN_APP
		glfwSwapBuffers(gsEnginePlatformData.glWnd);
#elif defined ANDROID_NDK || defined KOLIBRIOS_APP || defined IOS_APP
//		nope
#else
		SDK_ASSERT(0);
#endif
	}
	else
	{
		tfgTick(FIXED_TIMESTEP_MS);
	}
}
//-------------------------------------------------------------------------------------------

void Platform_Tick()
{
	SDK_NULL_ASSERT(_tickFn);
	_tickFn();
}
//-------------------------------------------------------------------------------------------

#ifndef DISABLE_FPS_COUNTER
void _processFPSCounter(fx32 ms_fx)
{
	cd.fps_timer_fx += ms_fx;
	++cd.fps_tick_ct;
	if(cd.fps_timer_fx >= ONE_SECOND_MS_FX)
	{
		cd.last_fps_tick_ct = cd.fps_tick_ct;
		cd.fps_tick_ct = 0; 
		cd.fps_timer_fx -= ONE_SECOND_MS_FX;
	}
}
#endif
//-------------------------------------------------------------------------------------------
