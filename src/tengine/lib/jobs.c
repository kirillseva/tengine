/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "platform_low.h"
#include "system.h"
#include "jobs_low.h"
#include "sound_low.h"
#include "render.h"
#include "jobs.h"
#include "sound.h"
#include "filesystem_low.h"
#include "texts.h"
#include "loadhelpers.h"
#ifdef JOBS_IN_SEPARATE_THREAD
#ifdef KOLIBRIOS_APP
#include "libsync.h"
#else
#include "pthread.h"
#endif
#endif

//-------------------------------------------------------------------------------------------

enum
{
	JOB_TASK_TYPE_VIDEOSTREAM = 0,
	JOB_TASK_TYPE_AUDIOSTREAM = 1,
	JOB_TASK_TYPE_FILESTREAM = 2,
	JOB_TASK_TYPE_COUNT,

	JOB_TASK_VIDEOSTREAM_MAX = 64,
	JOB_TASK_FILESTREAM_MAX = 1,
	JOB_TASK_AUDIOSTREAM_MAX = 2
};

struct JOBTask
{
#ifdef SDK_DEBUG
	s32 mType;
#endif
	union TaskData
	{
		void *mpPointer;
		struct JOBAnimStreamTask *mpAnimStreamData;
		struct JOBFileStreamTask *mpFileStreamData;
	}mTaskData;
};

static const s32 LOAD_TEXTURES_PER_TICK_MAX 
#ifdef JOBS_IN_SEPARATE_THREAD
												= 1;
#else
												= 3;
#endif

static struct JOBTask *gspJobTask[JOB_TASK_TYPE_COUNT] = {NULL};
static u32 gsTaskCount[JOB_TASK_TYPE_COUNT] = {0};
static BOOL gsJobsActive = FALSE;
static BOOL gsInit = FALSE;
#ifdef SDK_DEBUG
static s32 sgTextureLoad = 0;
#endif

#ifdef JOBS_IN_SEPARATE_THREAD
#define THREAD_IDLE_LIFE_MS 300
static BOOL gsThreadLoopActive = FALSE;
static BOOL gsThreadUsed = FALSE;
#ifndef KOLIBRIOS_APP
static pthread_t sgThread;
static pthread_attr_t sgThreadAttr;
static pthread_mutex_t gsMutex = PTHREAD_MUTEX_INITIALIZER;
#else
static mutex_t gsMutex;
#endif
#endif

static s32 _findTask(const void *data, s32 type);

// AnimStream

typedef void (*processASTaskFn)(struct AnimStreamData *asd, s32 *var, BOOL *exit_process);
typedef BOOL (*conditionASFn)(struct AnimStreamData *asd, s32 *var, BOOL *exit_process);

static BOOL _conditionASFn_LoadTexture(struct AnimStreamData *asd, s32 *var, BOOL *exit_process);

static void _processTaskASFn_LoadTexture(struct AnimStreamData *asd, s32 *var, BOOL *exit_process);

#ifdef JOBS_IN_SEPARATE_THREAD
 static BOOL _conditionASFn_Default(struct AnimStreamData *asd, s32 *var, BOOL *exit_process);
#ifdef USE_OPENGL_RENDER
 static void _processTaskASFn_BindTexture(struct AnimStreamData *asd, s32 *var, BOOL *exit_process);
 static BOOL _conditionASFn_BindTexture(struct AnimStreamData *asd, s32 *var, BOOL *exit_process);
#endif
 static void _processTaskASFn_Lock(struct AnimStreamData *asd, s32 *var, BOOL *exit_process);
 static BOOL _jobTrySetMutexForAnimStreamTasks(void);
#endif

static void _processASTask(s32 depth, processASTaskFn wfn, conditionASFn cfn, s32* var);

// FileStream

typedef void (*processFSTaskFn)(struct FileStreamData *fsd, s32 *var);
typedef BOOL (*conditionFSFn)(struct FileStreamData *fsd);

static BOOL _conditionFSFn_Default(struct FileStreamData *fsd);
static void _processTaskFSFn_LoadFile(struct FileStreamData *fsd, s32 *var);
#ifdef JOBS_IN_SEPARATE_THREAD
static void _processTaskFSFn_Lock(struct FileStreamData *fsd, s32 *var);
static BOOL _jobTrySetMutexForFileSystemTasks(void);

static void _startThread(void);
static void _stopThread(void);
#ifndef KOLIBRIOS_APP
static void* _threadLoop(void *t);
#else
int create_thread(int (*proc)(void *param), void *param, int stack_size);
static s32 _threadLoop(void *t);
#endif
#endif
static void _processFSTask(processFSTaskFn wfn, conditionFSFn cfn, s32* var);

// AudioStream

#ifdef JOBS_IN_SEPARATE_THREAD
BOOL _jobTrySetMutexForAudioStreamTasks(void);
#endif

//-------------------------------------------------------------------------------------------

void jobInit(void)
{
	if(!gsInit)
	{
		SDK_ASSERT(gsTaskCount[JOB_TASK_TYPE_VIDEOSTREAM] == 0);
		SDK_ASSERT(gsTaskCount[JOB_TASK_TYPE_AUDIOSTREAM] == 0);
		SDK_ASSERT(gsTaskCount[JOB_TASK_TYPE_FILESTREAM] == 0);
#ifdef SDK_DEBUG
		sgTextureLoad = 0;
#endif
		gsTaskCount[JOB_TASK_TYPE_VIDEOSTREAM] = 0;
		gsTaskCount[JOB_TASK_TYPE_AUDIOSTREAM] = 0;
		gsTaskCount[JOB_TASK_TYPE_FILESTREAM] = 0;
		gsJobsActive = TRUE;
#ifdef JOBS_IN_SEPARATE_THREAD
		gsThreadUsed = FALSE;
#ifndef KOLIBRIOS_APP
		pthread_attr_init(&sgThreadAttr);
		pthread_attr_setdetachstate(&sgThreadAttr, PTHREAD_CREATE_JOINABLE);
		pthread_mutex_init(&gsMutex, NULL);
#else
		mutex_init(&gsMutex);
#endif
#endif
		gspJobTask[JOB_TASK_TYPE_VIDEOSTREAM] = (struct JOBTask*)MALLOC(JOB_TASK_VIDEOSTREAM_MAX * sizeof(struct JOBTask), "jobInit:gspJobTask[0]");
		gspJobTask[JOB_TASK_TYPE_AUDIOSTREAM] = NULL; // 1
		gspJobTask[JOB_TASK_TYPE_FILESTREAM] = (struct JOBTask*)MALLOC(JOB_TASK_FILESTREAM_MAX * sizeof(struct JOBTask), "jobInit:gspJobTask[2]");
		gsInit = TRUE;
	}
}
//-------------------------------------------------------------------------------------------

void jobRelease(void)
{
	SDK_ASSERT(gsTaskCount[JOB_TASK_TYPE_VIDEOSTREAM] == 0);
	SDK_ASSERT(gsTaskCount[JOB_TASK_TYPE_AUDIOSTREAM] == 0);
	SDK_ASSERT(gsTaskCount[JOB_TASK_TYPE_FILESTREAM] == 0);
	gsJobsActive = FALSE;
	if(gspJobTask[JOB_TASK_TYPE_FILESTREAM])
	{
		FREE(gspJobTask[JOB_TASK_TYPE_FILESTREAM]);
	}
	if(gspJobTask[JOB_TASK_TYPE_AUDIOSTREAM])
	{
		FREE(gspJobTask[JOB_TASK_TYPE_AUDIOSTREAM]);
	}
	if(gspJobTask[JOB_TASK_TYPE_VIDEOSTREAM])
	{
		FREE(gspJobTask[JOB_TASK_TYPE_VIDEOSTREAM]);	
	}
	gspJobTask[JOB_TASK_TYPE_VIDEOSTREAM] =
	gspJobTask[JOB_TASK_TYPE_AUDIOSTREAM] = 
	gspJobTask[JOB_TASK_TYPE_FILESTREAM] = NULL;
#ifdef JOBS_IN_SEPARATE_THREAD
	if(gsInit)	
	{
		_stopThread();
#ifndef KOLIBRIOS_APP
		pthread_mutex_destroy(&gsMutex);
		pthread_attr_destroy(&sgThreadAttr);
#else
		mutex_destroy(&gsMutex);
#endif		
		gsThreadUsed = FALSE;
	}
#endif
	gsInit = FALSE;
}
//-------------------------------------------------------------------------------------------

BOOL jobHasTasks(void)
{
	return gsTaskCount[JOB_TASK_TYPE_VIDEOSTREAM] > 0 ||
			gsTaskCount[JOB_TASK_TYPE_AUDIOSTREAM] > 0 ||
			gsTaskCount[JOB_TASK_TYPE_FILESTREAM] > 0;
}
//-------------------------------------------------------------------------------------------

void jobSetActive(BOOL val)
{
	gsJobsActive = val;
#ifdef JOBS_IN_SEPARATE_THREAD
	if(gsJobsActive == TRUE && jobHasTasks() == TRUE)
	{
		_startThread();
	}
#endif
}
//-------------------------------------------------------------------------------------------

BOOL jobIsActive(void)
{
	return gsJobsActive;
}
//-------------------------------------------------------------------------------------------

#ifdef JOBS_IN_SEPARATE_THREAD
void _startThread(void)
{
	if(gsThreadLoopActive == TRUE)
	{
		return;
	}
	_stopThread();
	if(jobHasTasks() == TRUE)
	{
#ifndef KOLIBRIOS_APP
		const s32 rc = pthread_create(&sgThread, &sgThreadAttr, _threadLoop, NULL);
		if(rc)
		{
			OS_Warning("error: return code from pthread_create() is %d\n", rc);
			SDK_ASSERT(0);
		}
#else
		create_thread(_threadLoop, 0, 163840);
#endif	
		// OS_Printf("start thread\n");
	}
}
//-------------------------------------------------------------------------------------------

void _stopThread(void)
{
#ifndef KOLIBRIOS_APP
	if(gsThreadUsed == TRUE)
	{
		const s32 rc = pthread_join(sgThread, NULL);
		if(rc)
		{
			OS_Warning("error: return code from pthread_join() is %d\n", rc);
			SDK_ASSERT(0);
		}
	}
#endif
}
//-------------------------------------------------------------------------------------------
#ifndef KOLIBRIOS_APP
void* _threadLoop(void *t)
#else
s32 _threadLoop(void *t)
#endif
{
	u32 timer, tickPreviousTime;
	gsThreadLoopActive = TRUE;
	gsThreadUsed = TRUE;
	tickPreviousTime = Platform_GetTime();
	timer = 0;
	(void)t;
	while(gsJobsActive == TRUE)
	{
		const u32 var = Platform_GetTime();
		timer += var - tickPreviousTime;
		tickPreviousTime = var;
		if(jobHasTasks() == TRUE)
		{
			timer = 0;
			//load files
			if(_jobTrySetMutexForFileSystemTasks()) // <-- jobCriticalSectionBegin()
			{
				jobUpdateFileStreamTasks();
				jobCriticalSectionEnd();
			}
			// steaming video
			if(_jobTrySetMutexForAnimStreamTasks()) // <-- jobCriticalSectionBegin()
			{
				jobUpdateAnimStreamTasksAndLoadTextures();
				jobCriticalSectionEnd();
			}
			// process audio
			if(_jobTrySetMutexForAudioStreamTasks()) // <-- jobCriticalSectionBegin()
			{
				jobUpdateAudioStreamTasks();
				jobCriticalSectionEnd();
			}
		}
		else if (timer > THREAD_IDLE_LIFE_MS)
		{
			break;
		}
#ifndef KOLIBRIOS_APP
		sched_yield();
#else
		// sched_yield() kolibrios substitute
		asm volatile ("int $0x40"::"a"(68), "b"(1));
#endif
	}
	// OS_Printf("stop thread\n");
	gsThreadLoopActive = FALSE;
#ifndef KOLIBRIOS_APP
	return NULL;
#else
	return 0;
#endif
}
//-------------------------------------------------------------------------------------------

BOOL jobCriticalSectionBegin(void)
{
	if(gsInit)
	{
#ifndef KOLIBRIOS_APP
		pthread_mutex_lock(&gsMutex);
#else
		mutex_lock(&gsMutex);
#endif
		return TRUE;
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------

BOOL jobCriticalSectionEnd(void)
{
	if(gsInit)
	{
#ifndef KOLIBRIOS_APP
		pthread_mutex_unlock(&gsMutex);
#else
		mutex_unlock(&gsMutex);
#endif
		return TRUE;
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------
#endif

s32 _findTask(const void *data, s32 type)
{
	u32 i;
	for(i = 0; i < gsTaskCount[type]; i++)
	{
		if(gspJobTask[type][i].mTaskData.mpPointer == data)
		{
			return (s32)i;
		}
	}
	return -1;
}
//-------------------------------------------------------------------------------------------

void jobResetTasks(void)
{
	u32 i, j, k;
#ifdef SDK_DEBUG
	sgTextureLoad = 0;
#endif
	for(k = 0; k < JOB_TASK_TYPE_COUNT; k++)
	{
		for(i = 0; i < gsTaskCount[k]; i++)
		{
			switch(k)
			{
				case JOB_TASK_TYPE_VIDEOSTREAM:
				{
					for(j = 0; j < JOB_ANIM_TASK_BUFFER_MAX; j++)
					{
						gspJobTask[k][i].mTaskData.mpAnimStreamData->mData[j].mBufferStatus = FALSE;
						gspJobTask[k][i].mTaskData.mpAnimStreamData->mData[j].mBindStatus = FALSE;
						gspJobTask[k][i].mTaskData.mpAnimStreamData->mData[j].mpImage = NULL;
					}
				}
				break;

				case JOB_TASK_TYPE_FILESTREAM:
					gspJobTask[k][i].mTaskData.mpFileStreamData->mData.mStatus = FALSE;
			}
		}
		gsTaskCount[k] = 0;
	}
}
//-------------------------------------------------------------------------------------------

void jobAddStreamVideoTask(struct JOBAnimStreamTask *data)
{
	SDK_ASSERT(gsInit);
	if(_findTask(data, JOB_TASK_TYPE_VIDEOSTREAM) < 0)
	{
		gspJobTask[JOB_TASK_TYPE_VIDEOSTREAM][gsTaskCount[JOB_TASK_TYPE_VIDEOSTREAM]].mTaskData.mpAnimStreamData = data;
#ifdef SDK_DEBUG
		gspJobTask[JOB_TASK_TYPE_VIDEOSTREAM][gsTaskCount[JOB_TASK_TYPE_VIDEOSTREAM]].mType = JOB_TASK_TYPE_VIDEOSTREAM;
#endif
		gsTaskCount[JOB_TASK_TYPE_VIDEOSTREAM]++;
		SDK_ASSERT(JOB_TASK_VIDEOSTREAM_MAX >= gsTaskCount[JOB_TASK_TYPE_VIDEOSTREAM]);
#ifdef JOBS_IN_SEPARATE_THREAD
		_startThread();
#endif
	}
}
//-------------------------------------------------------------------------------------------

void jobRemoveStreamVideoTask(const struct JOBAnimStreamTask *data)
{
	if(gsInit)
	{
		const s32 i = _findTask(data, JOB_TASK_TYPE_VIDEOSTREAM);
		if(i >= 0)
		{
			gsTaskCount[JOB_TASK_TYPE_VIDEOSTREAM]--;
			gspJobTask[JOB_TASK_TYPE_VIDEOSTREAM][i] = gspJobTask[JOB_TASK_TYPE_VIDEOSTREAM][gsTaskCount[JOB_TASK_TYPE_VIDEOSTREAM]];
		}
	}
}
//-------------------------------------------------------------------------------------------

void jobAddStreamFileTask(struct JOBFileStreamTask *data)
{
	SDK_ASSERT(gsInit);
	if(_findTask(data, JOB_TASK_TYPE_FILESTREAM) < 0)
	{
		gspJobTask[JOB_TASK_TYPE_FILESTREAM][gsTaskCount[JOB_TASK_TYPE_FILESTREAM]].mTaskData.mpFileStreamData = data;
#ifdef SDK_DEBUG
		gspJobTask[JOB_TASK_TYPE_FILESTREAM][gsTaskCount[JOB_TASK_TYPE_FILESTREAM]].mType = JOB_TASK_TYPE_FILESTREAM;
#endif
		gsTaskCount[JOB_TASK_TYPE_FILESTREAM]++;
		SDK_ASSERT(JOB_TASK_FILESTREAM_MAX >= gsTaskCount[JOB_TASK_TYPE_FILESTREAM]);
#ifdef JOBS_IN_SEPARATE_THREAD
		_startThread();
#endif
	}
}
//-------------------------------------------------------------------------------------------

void jobRemoveStreamFileTask(const struct JOBFileStreamTask *data)
{
	if(gsInit)
	{
		const s32 i = _findTask(data, JOB_TASK_TYPE_FILESTREAM);
		if(i >= 0)
		{
			gsTaskCount[JOB_TASK_TYPE_FILESTREAM]--;
			gspJobTask[JOB_TASK_TYPE_FILESTREAM][i] = gspJobTask[JOB_TASK_TYPE_FILESTREAM][gsTaskCount[JOB_TASK_TYPE_FILESTREAM]];
		}
	}
}
//-------------------------------------------------------------------------------------------

void jobAddStreamAudioTask(void)
{
	SDK_ASSERT(gsInit);
	gsTaskCount[JOB_TASK_TYPE_AUDIOSTREAM]++;
	SDK_ASSERT(JOB_TASK_AUDIOSTREAM_MAX >= gsTaskCount[JOB_TASK_TYPE_AUDIOSTREAM]);
#ifdef JOBS_IN_SEPARATE_THREAD
	_startThread();
#endif
}
//-------------------------------------------------------------------------------------------

void jobRemoveStreamAudioTask(void)
{
	if(gsInit)
	{
		if(gsTaskCount[JOB_TASK_TYPE_AUDIOSTREAM] > 0)
		{
			gsTaskCount[JOB_TASK_TYPE_AUDIOSTREAM]--;
		}
	}
}
//-------------------------------------------------------------------------------------------

// video stream

//-------------------------------------------------------------------------------------------
#ifdef JOBS_IN_SEPARATE_THREAD
BOOL _jobTrySetMutexForAnimStreamTasks(void)
{
	s32 var = 0;
	_processASTask(JOB_ANIM_TASK_BUFFER_MAX, _processTaskASFn_Lock, _conditionASFn_Default, &var);
	return var == 1;
}
//-------------------------------------------------------------------------------------------

void _processTaskASFn_Lock(struct AnimStreamData *asd, s32 *var, BOOL *exit_process)
{
	(void)asd;
	*exit_process = TRUE;
	*var = jobCriticalSectionBegin();
}
//-------------------------------------------------------------------------------------------

#ifdef JOBS_IN_SEPARATE_THREAD
#ifdef USE_OPENGL_RENDER
void _processTaskASFn_BindTexture(struct AnimStreamData *asd, s32 *var, BOOL *exit_process)
{
	(void)var;
	(void)exit_process;
	glRender_LoadTextureToVRAM(asd->mpImage, asd->mpImage->mOpaqType);
	asd->mBindStatus = TRUE;
}
#endif
#endif
//-------------------------------------------------------------------------------------------

BOOL jobBindTexturesToVRAM(void)
{
#ifdef USE_OPENGL_RENDER
	s32 var = 1;
	_processASTask(1, _processTaskASFn_BindTexture, _conditionASFn_BindTexture, &var);
	return var;
#else
	return FALSE;
#endif
}
#endif
//-------------------------------------------------------------------------------------------

void jobUpdateAnimStreamTasksAndLoadTextures(void)
{
	s32 var = 0;
	_processASTask(
#ifdef JOBS_IN_SEPARATE_THREAD
		JOB_ANIM_TASK_BUFFER_MAX,
#else
		JOB_ANIM_TASK_BUFFER_MAX - 1,
#endif
		_processTaskASFn_LoadTexture, _conditionASFn_LoadTexture, &var);
#ifdef SDK_DEBUG
	sgTextureLoad += var;
#endif
}
//-------------------------------------------------------------------------------------------

#ifdef SDK_DEBUG
void jobResetTextureCounter(void){ sgTextureLoad = 0; }
s32 jobGetTextureCounter(void){ return sgTextureLoad; }
#endif
//-------------------------------------------------------------------------------------------

void _processTaskASFn_LoadTexture(struct AnimStreamData *asd, s32 *var, BOOL *exit_process)
{
#ifdef USE_OPENGL_RENDER
	u16 glidx;
	char fullPath[MAX_FILEPATH];
	STD_StrCpy(fullPath, asd->mpFilename);
	STD_StrCat(fullPath, FILERES_EXTENTION);
	glidx = asd->mpImage->mOpaqType;
	LoadBMPImage(fullPath, &asd->mpImage, TRUE);
	asd->mpImage->mOpaqType = glidx;
#ifndef JOBS_IN_SEPARATE_THREAD
	glRender_LoadTextureToVRAM(asd->mpImage, glidx);
#endif
#endif
#ifdef USE_CUSTOM_RENDER
	char fullPath[MAX_FILEPATH];
	STD_StrCpy(fullPath, asd->mpFilename);
	STD_StrCat(fullPath, FILERES_EXTENTION);
	LoadBMPImage(fullPath, &asd->mpImage, TRUE);
#endif
	asd->mBufferStatus = TRUE;
	asd->mBindStatus = FALSE;
	++(*var);
	(void)exit_process;
}
//-------------------------------------------------------------------------------------------

#ifdef JOBS_IN_SEPARATE_THREAD
BOOL _conditionASFn_Default(struct AnimStreamData *asd, s32 *var, BOOL *exit_process)
{
	(void)var;
	*exit_process = FALSE;
	return asd->mBufferStatus == FALSE && asd->mpImage != NULL;
}
#endif
//-------------------------------------------------------------------------------------------

BOOL _conditionASFn_LoadTexture(struct AnimStreamData *asd, s32 *var, BOOL *exit_process)
{
	*exit_process = *var >= LOAD_TEXTURES_PER_TICK_MAX;
	return asd->mBufferStatus == FALSE && asd->mpImage != NULL && !*exit_process;
}
//-------------------------------------------------------------------------------------------

#ifdef JOBS_IN_SEPARATE_THREAD
#ifdef USE_OPENGL_RENDER
BOOL _conditionASFn_BindTexture(struct AnimStreamData *asd, s32 *var, BOOL *exit_process)
{
	(void)exit_process;
	if(asd->mBufferStatus == FALSE)
	{
		*var = 0;
	}
	return asd->mBufferStatus == TRUE && asd->mBindStatus == FALSE && asd->mpImage != NULL; 
}
#endif
#endif
//-------------------------------------------------------------------------------------------

void _processASTask(s32 depth, processASTaskFn wfn, conditionASFn cfn, s32* var)
{
	if(gsJobsActive && gsTaskCount[JOB_TASK_TYPE_VIDEOSTREAM])
	{
		s32 i;
		u32 j;		
		for(i = 0; i < depth; i++)
		{
			for(j = 0; j < gsTaskCount[JOB_TASK_TYPE_VIDEOSTREAM]; j++)
			{
				struct AnimStreamData *asd;
				BOOL exit_process;
#ifdef SDK_DEBUG
				SDK_ASSERT(gspJobTask[JOB_TASK_TYPE_VIDEOSTREAM][j].mType == JOB_TASK_TYPE_VIDEOSTREAM);
#endif
				asd = &gspJobTask[JOB_TASK_TYPE_VIDEOSTREAM][j].mTaskData.mpAnimStreamData->mData[i]; 
				exit_process = FALSE;
				if(cfn(asd, var, &exit_process))
				{
					wfn(asd, var, &exit_process);
				}
				if(exit_process)
				{
					return;
				}
			}
		}
	}
}
//-------------------------------------------------------------------------------------------

// file stream

//-------------------------------------------------------------------------------------------

#ifdef JOBS_IN_SEPARATE_THREAD
BOOL _jobTrySetMutexForFileSystemTasks(void)
{
	s32 var = 0;
	_processFSTask(_processTaskFSFn_Lock, _conditionFSFn_Default, &var);
	return var == 1;
}
//-------------------------------------------------------------------------------------------

void _processTaskFSFn_Lock(struct FileStreamData *fsd, s32 *var)
{
	(void)fsd;
	*var = jobCriticalSectionBegin();
}
//-------------------------------------------------------------------------------------------
#endif

void jobUpdateFileStreamTasks(void)
{
	s32 var = 0;
	_processFSTask(_processTaskFSFn_LoadFile, _conditionFSFn_Default, &var);
}
//-------------------------------------------------------------------------------------------

static BOOL _conditionFSFn_Default(struct FileStreamData *fsd)
{
	return fsd->mStatus == FALSE && fsd->mMode != 0;
}
//-------------------------------------------------------------------------------------------

static void _processTaskFSFn_LoadFile(struct FileStreamData *fsd, s32 *var)
{
	switch(fsd->mMode)
	{
		case JFSDM_MODE_IMG:
			LoadBMPImage(fsd->mpFilename, &fsd->mpImage, FALSE);
			fsd->mStatus = TRUE;
		break;
		case JFSDM_MODE_FILE:
			fsd->mpFile = LoadFile(fsd->mpFilename, FALSE);
			fsd->mStatus = TRUE;
		break;
		case JFSDM_MODE_SND:
			fsd->mpFile = (u8*)sndLoadWavData(fsd->mpFilename, fsd->mIdx, fsd->mValue == 1); //sfx
			fsd->mStatus = TRUE;
	}
	++(*var);
}
//-------------------------------------------------------------------------------------------

void _processFSTask(processFSTaskFn wfn, conditionFSFn cfn, s32* var)
{
	if(gsJobsActive && gsTaskCount[JOB_TASK_TYPE_FILESTREAM])
	{
		u32 j;		
		for(j = 0; j < gsTaskCount[JOB_TASK_TYPE_FILESTREAM]; j++)
		{
			struct FileStreamData *fsd;
#ifdef SDK_DEBUG
			SDK_ASSERT(gspJobTask[JOB_TASK_TYPE_FILESTREAM][j].mType == JOB_TASK_TYPE_FILESTREAM);
#endif
			fsd = &gspJobTask[JOB_TASK_TYPE_FILESTREAM][j].mTaskData.mpFileStreamData->mData; 
			if(cfn(fsd))
			{
				wfn(fsd, var);
			}
		}
	}
}
//-------------------------------------------------------------------------------------------

// audio stream

//-------------------------------------------------------------------------------------------
#ifdef JOBS_IN_SEPARATE_THREAD
BOOL _jobTrySetMutexForAudioStreamTasks(void)
{
	if(gsJobsActive && sndIsBGMPlaying())
	{
		return jobCriticalSectionBegin();
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------
#endif

void jobUpdateAudioStreamTasks(void)
{
	if(gsJobsActive && gsTaskCount[JOB_TASK_TYPE_AUDIOSTREAM])
	{
		sndUpdateSoundStream();
	}
}
//-------------------------------------------------------------------------------------------
