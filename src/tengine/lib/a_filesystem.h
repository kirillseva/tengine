#ifndef ANDROID_FILESYSTEM_H
#define ANDROID_FILESYSTEM_H

#ifdef ANDROID_NDK

#include "platform.h"

#ifdef __cplusplus
extern "C" {
#endif

struct InitFileSystemData;

#define FILESYSTEMFN(name) androidFS_##name

void androidFS_InitFileSystem(struct InitFileSystemData* data);
void androidFS_ReleaseFileSystem(void);
BOOL androidFS_IsFileSystemInit(void);

s16 androidFS_fopen(const char* filename);
s32 androidFS_fsize(s16 id);
BOOL androidFS_fseek(u32 off, s16 id);
s32 androidFS_fread(unsigned char* buf, u32 size, s16 id);
void androidFS_fclose(s16 id);

void androidFS_lostDevice(void);

#ifdef __cplusplus
}
#endif
#endif
#endif // ANDROID_FILESYSTEM_H
