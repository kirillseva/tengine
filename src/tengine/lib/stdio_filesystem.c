/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#if defined  WINDOWS_APP || defined NIX_APP || defined IOS_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP

#include "system.h"
#include "filesystem.h"
#include "filesystem_low.h"
#include "stdio_filesystem.h"
#include "texts.h"

//-------------------------------------------------------------------------------------------

static char gSysFSPath[MAX_FILEPATH] = {0};
static s32 gSysFSPathLenght = 0;
static FILE *gspFileHandler[MAX_OPENFILES];
static BOOL gsFileHandlerState[MAX_OPENFILES] = {0};

extern BOOL gsFilesystemError;

//-------------------------------------------------------------------------------------------

void stdioFS_InitFileSystem(struct InitFileSystemData* data)
{
	SDK_ASSERT(gSysFSPathLenght == 0);
	SDK_NULL_ASSERT(data->mpPath);
	STD_StrCpy(gSysFSPath, data->mpPath);
	gSysFSPathLenght = STD_StrLen(gSysFSPath); 
	gsFilesystemError = FALSE;
}
//-------------------------------------------------------------------------------------------

void stdioFS_ReleaseFileSystem()
{
	stdioFS_lostDevice();
	gSysFSPathLenght = 0;
	gSysFSPath[0] = 0;
}
//-------------------------------------------------------------------------------------------

void stdioFS_lostDevice()
{
	s16 i = MAX_OPENFILES;
	while(i > 0)
	{
		i--;
		if(gsFileHandlerState[i])
		{
			fclose(gspFileHandler[i]);
			gsFileHandlerState[i] = FALSE;
		}
	}
}
//-------------------------------------------------------------------------------------------

BOOL stdioFS_IsFileSystemInit()
{
	return gSysFSPathLenght != 0;	
}
//-------------------------------------------------------------------------------------------

s16 stdioFS_fopen(const char* filename)
{
	s16 i;
	for(i = 0; i < MAX_OPENFILES; i++)
	{
		if(gsFileHandlerState[i] == FALSE)
		{
			char fullPath[MAX_FILEPATH];
			STD_StrCpy(fullPath, gSysFSPath);
			STD_StrCpy(fullPath + gSysFSPathLenght, filename);
			gspFileHandler[i] = fopen(fullPath, "rb");
			if(gspFileHandler[i] != NULL)
			{
				gsFileHandlerState[i] = TRUE;
				return i;
			}
			else
			{
				return -1;
			}
		}
	}
	return -1;
}
//-------------------------------------------------------------------------------------------

s32 stdioFS_fsize(s16 id)
{
	s32 size;
	SDK_ASSERT(id < MAX_OPENFILES);
	SDK_ASSERT(gsFileHandlerState[id] == TRUE);
	fseek(gspFileHandler[id], 0, SEEK_END);
	size = (s32)ftell(gspFileHandler[id]);
	fseek(gspFileHandler[id], 0, SEEK_SET);
	return size;
}
//-------------------------------------------------------------------------------------------

BOOL stdioFS_fseek(u32 off, s16 id)
{
	SDK_ASSERT(id < MAX_OPENFILES);
	SDK_ASSERT(gsFileHandlerState[id] == TRUE);
	fseek(gspFileHandler[id], off, SEEK_SET);
	return TRUE;
}
//---------------------------------------------------------------------------

s32 stdioFS_fread(unsigned char* buf, u32 size, s16 id)
{
	SDK_ASSERT(id < MAX_OPENFILES);
	SDK_ASSERT(gsFileHandlerState[id] == TRUE);
	return (s32)fread(buf, 1, size, gspFileHandler[id]);
}
//-------------------------------------------------------------------------------------------

void stdioFS_fclose(s16 id)
{
	SDK_ASSERT(id < MAX_OPENFILES && id >= 0);
	if(gsFileHandlerState[id] == TRUE)
	{
		fclose(gspFileHandler[id]);
		gsFileHandlerState[id] = FALSE;
	}
}
//-------------------------------------------------------------------------------------------

#endif
