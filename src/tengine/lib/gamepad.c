/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "gamepad.h"
#if defined ANDROID_NDK || defined WINDOWS_APP || defined NIX_APP || defined IOS_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP
#include "lib/a_gamepad.h"
#endif
//----------------------------------------------------------------------------------------------

typedef s32 KeyTriggerState;

#if defined ANDROID_NDK || defined  WINDOWS_APP || defined NIX_APP || defined IOS_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP
static s32 key_data = 0;
#endif

enum  KeyTriggerStateEnum
{
    KEYTRIGGER_NONE  = 0,
    KEYTRIGGER_DOWN  = 1,
    KEYTRIGGER_PRESS = 2,
    KEYTRIGGER_UP	 = 4
};

static KeyTriggerState _gsKeyState[KEY_TYPE_COUNT];

//----------------------------------------------------------------------------------------------

void _getKeyResult(s32 iPadReadResult, s32 iPadButtonID, KeyType iNGButtonID);
KeyTriggerState GetButtonState(KeyType iButton);

//----------------------------------------------------------------------------------------------

void InitGamePad()
{
    ResetGamePad();
}
//----------------------------------------------------------------------------------------------

BOOL IsButtonDown(KeyType iButton)
{
    return _gsKeyState[iButton] & KEYTRIGGER_DOWN;
}
//----------------------------------------------------------------------------------------------
  
BOOL IsButtonUp(KeyType iButton)
{
    return _gsKeyState[iButton] & KEYTRIGGER_UP;
}
//----------------------------------------------------------------------------------------------
 
BOOL IsButtonPress(KeyType iButton)
{
    return _gsKeyState[iButton] & KEYTRIGGER_PRESS;
}
//----------------------------------------------------------------------------------------------
  
KeyTriggerState GetButtonState(KeyType iButton)
{
    return _gsKeyState[iButton];
}
//----------------------------------------------------------------------------------------------
 
void ResetGamePad()
{
	u32 i;
    for(i = 0; i < KEY_TYPE_COUNT; i++)
    {
        _gsKeyState[i] = KEYTRIGGER_NONE;    	
    }
#if defined ANDROID_NDK || defined  WINDOWS_APP || defined NIX_APP || defined IOS_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP
	key_data = 0;
#endif
}
//----------------------------------------------------------------------------------------------

void _getKeyResult(s32 iPadReadResult, s32 iPadButtonID, KeyType iNGButtonID)
{
	if(iPadButtonID & iPadReadResult || 
	  (iPadReadResult != 0 && iNGButtonID == TYPE_ANY_KEY)) // for any key
	{    	    
        switch(_gsKeyState[iNGButtonID])
        {
            case KEYTRIGGER_UP:
            case KEYTRIGGER_NONE:
                _gsKeyState[iNGButtonID] = KEYTRIGGER_DOWN;
            break;
            case KEYTRIGGER_DOWN:
                _gsKeyState[iNGButtonID] = KEYTRIGGER_PRESS;  
        }
    }
    else
	switch(_gsKeyState[iNGButtonID])
	{
	    case KEYTRIGGER_PRESS:
	    case KEYTRIGGER_DOWN:
	        _gsKeyState[iNGButtonID] = KEYTRIGGER_UP;
	    break;
	    default:
	        _gsKeyState[iNGButtonID] = KEYTRIGGER_NONE;
	}     
}
//----------------------------------------------------------------------------------------------

void UpdateGamePad()
{
	const s32 pressed = PAD_Read();

	_getKeyResult(pressed, PAD_BUTTON_A, TYPE_A);			
    _getKeyResult(pressed, PAD_BUTTON_B, TYPE_B);
    
    _getKeyResult(pressed, PAD_BUTTON_SELECT, TYPE_SELECT);
    _getKeyResult(pressed, PAD_BUTTON_START, TYPE_START);
    
    _getKeyResult(pressed, PAD_KEY_RIGHT, TYPE_RIGHT);
    _getKeyResult(pressed, PAD_KEY_LEFT, TYPE_LEFT);
    _getKeyResult(pressed, PAD_KEY_UP, TYPE_UP);
    _getKeyResult(pressed, PAD_KEY_DOWN, TYPE_DOWN);
    
    _getKeyResult(pressed, PAD_BUTTON_R, TYPE_R);
    _getKeyResult(pressed, PAD_BUTTON_L, TYPE_L);
    
    _getKeyResult(pressed, PAD_BUTTON_X, TYPE_X);    
    _getKeyResult(pressed, PAD_BUTTON_Y, TYPE_Y);
    
    _getKeyResult(pressed, PAD_BUTTON_DEBUG, TYPE_D);

    _getKeyResult(pressed, 0, TYPE_ANY_KEY);
}
//----------------------------------------------------------------------------------------------

#if defined ANDROID_NDK || defined WINDOWS_APP || defined NIX_APP || defined IOS_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP
s32 PAD_Read()
{
	return key_data; 	
}
//----------------------------------------------------------------------------------------------

void onKeyDown(s32 key)
{
	key_data |= key;
}
//----------------------------------------------------------------------------------------------

void onKeyUp(s32 key)
{
	key_data &= ~key;
}
//----------------------------------------------------------------------------------------------
#endif
