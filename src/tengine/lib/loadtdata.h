#ifndef TENGINE_LOAD_DATA_H
#define TENGINE_LOAD_DATA_H
//---------------------------------------------------------------------------

#include "platform.h"

#ifdef __cplusplus
extern "C" {
#endif

struct TEngineInstance;
struct TEngineCommonData;

void _releaseScriptData(struct TEngineInstance * i);
void _releaseTrigData(struct TEngineInstance * i);
void _freeRes(struct TEngineCommonData *cd);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /*TENGINE_LOAD_DATA_H*/
