/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "gx_helpers.h"
#include "fxmath.h"

s32 gxHelper_preDrawImage(s32 iScreenW, s32 iScreenH, s32 *iScreenX, s32 *iScreenY,
                    const struct BMPImage* pSrcData, s32 *iSrcX, s32 *iSrcY, s32 *iSrcW, s32 *iSrcH)
{
    if(pSrcData == NULL || *iScreenX >= iScreenW || *iScreenY >= iScreenH)
    {
       return FALSE;
    }

    if(*iSrcX < 0)
    {
       *iSrcW += *iSrcX;
       *iSrcX = 0;
    	if(*iSrcW > pSrcData->mWidth)
    	{
    		*iSrcW = pSrcData->mWidth;
    	}
    }
    else
    {
    	if(*iSrcX + *iSrcW > pSrcData->mWidth)
    	{
    		*iSrcW = pSrcData->mWidth - *iSrcX;
    	}
    }

    if(*iScreenX < 0)
    {
    	*iSrcW += *iScreenX;
    	*iSrcX -= *iScreenX;
    	*iScreenX = 0;
    	if(*iSrcW >= iScreenW)
    	{
    		*iSrcW = iScreenW;
    	}
    }
    else
    {
    	if(*iScreenX + *iSrcW >= iScreenW)
    	{
    		*iSrcW = iScreenW - *iScreenX;
    	}
    }

    if(*iSrcW <= 0 || *iScreenX + *iSrcW < 0)
    {
        return FALSE;
    }

    if(*iSrcY < 0)
    {
        *iSrcH += *iSrcY;
        *iSrcY = 0;
        if(*iSrcH > pSrcData->mHeight)
        {
        	*iSrcH = pSrcData->mHeight;
        }
    }
    else
    {
       if(*iSrcY + *iSrcH > pSrcData->mHeight)
       {
           *iSrcH = pSrcData->mHeight - *iSrcY;
       }
    }

    if(*iScreenY < 0)
    {
    	*iSrcH += *iScreenY;
    	*iSrcY -= *iScreenY;
    	*iScreenY = 0;
    	if(*iSrcH >= iScreenH)
    	{
    		*iSrcH = iScreenH;
    	}
    }
    else
    {
    	if(*iScreenY + *iSrcH >= iScreenH)
    	{
    		*iSrcH = iScreenH - *iScreenY;
    	}
    }
    
    if(*iSrcH <= 0 || *iScreenY + *iSrcH < 0)
    {
        return FALSE;
    }
    return TRUE;
}
//----------------------------------------------------------------------------------------------

//koen-sazerland algorithm
s32 gxHelper_preDrawLine(fx32 iDestW, fx32 iDestH, fx32 *x0, fx32 *y0, fx32 *x1, fx32 *y1)
{
   s32 cn, ck, s, v, ct;            
   fx32 dxdy, dydx, dx, dy, r;

   ck = 0;
   if(*x1 < 0)
   {
        ck += 1; //0001
   }
#ifdef USE_CUSTOM_RENDER
   else if(*x1 >= iDestW)
#else
   else if(*x1 > iDestW)
#endif
   {
        ck += 2; //0010
   }
   if(*y1 < 0)
   {
        ck += 8; //1000
   }
#ifdef USE_CUSTOM_RENDER
   else if(*y1 >= iDestH)
#else 
   else if(*y1 > iDestH)
#endif
   {
        ck += 4; //0100
   }
   
   cn = 0;
   if(*x0 < 0)
   {
        cn += 1; //0001
   }
#ifdef USE_CUSTOM_RENDER
   else if(*x0 >= iDestW)
#else
   else if(*x0 > iDestW)
#endif
   {
        cn += 2; //0010
   }
   if(*y0 < 0)
   {
        cn += 8; //1000
   }
#ifdef USE_CUSTOM_RENDER
   else if(*y0 >= iDestH)
#else
   else if(*y0 > iDestH)
#endif
   {
        cn += 4; //0100
   }

   dx = *x1 - *x0;
   dy = *y1 - *y0;
   
   if(dx != 0)
   {
		dydx = fxDiv(dy, dx);
   }
   else
   {
	  if(dy == 0) 
      {
         if(cn == 0 && ck == 0)
         {
            return 0;
         }
         else
         {
            return 0;
         }
      }
      dydx = 0;
   }
   
   if(dy != 0)
   {
   	  dxdy = fxDiv(dx, dy);
   }
   else
   {
	   dxdy = 0;
   }
   
   v = 0; // visible  
   ct = 4; // counter
   
   do 
   {
       if(cn & ck)
       {
         break;
       }
      
       if(cn == 0 && ck == 0)
       { 
         ++v;
         break;
       }
      
       if(!cn)
       {
         s = cn; 
         cn = ck;
         ck = s;
         r = *x0;
         *x0 = *x1;
         *x1 = r;
         r = *y0;
         *y0 = *y1;
         *y1 = r;
       }

       if(cn & 1)
       {
         *y0 -= fxMul(*x0, dydx);
         *x0 = 0;
       }
       else if(cn & 2)
       {
         *y0 += fxMul(dydx, (iDestW - FX32_ONE - *x0));
         *x0 = iDestW - FX32_ONE;
       }
       else if(cn & 4)
       {
         *x0 += fxMul(dxdy, (iDestH - FX32_ONE - *y0));
         *y0 = iDestH - FX32_ONE;
       }
       else if(cn & 8)
       {
         *x0 -= fxMul(*y0, dxdy);
         *y0 = 0;
       }
      
       cn = 0;
       if(*x0 < 0)
       {
            cn += 1; //0001;
       }
#ifdef USE_CUSTOM_RENDER
       else if(*x0 >= iDestW)
#else
       else if(*x0 > iDestW)
#endif
       {
            cn += 2; //0010;
       }
       if(*y0 < 0)
       {
            cn += 8; //1000;
       }
#ifdef USE_CUSTOM_RENDER
       else if(*y0 >= iDestH)
#else
       else if(*y0 > iDestH)
#endif
       {
            cn += 4; //0100;
       }
   }
   while(--ct >= 0);
   return v;
}
//----------------------------------------------------------------------------------------------

s32 gxHelper_preDrawSquare(fx32 iDestW, fx32 iDestH, fx32 *iX, fx32 *iY, fx32 *iWidth, fx32 *iHeight)
{
     if(*iX >= iDestW || *iY >= iDestH)
     {
          return FALSE;
     }
     
     if(*iX < 0)
     {     
          *iWidth += *iX;     
          *iX = 0;
          if(*iWidth >= iDestW)
          {
            *iWidth = iDestW;
          }
     }
     else if(*iX + *iWidth >= iDestW)
     {
          *iWidth = iDestW - *iX;
     }

	 if(*iWidth <= 0 || *iX + *iWidth < 0)
	 {
		return FALSE;
	 }

     if(*iY < 0)
     {
          *iHeight += *iY;
          *iY = 0;
          if(*iHeight >= iDestH)
          {
               *iHeight = iDestH;
          }
     }
     else if(*iY + *iHeight >= iDestH)
     {
          *iHeight = iDestH - *iY;
     }

     if(*iHeight <= 0 || *iY + *iHeight < 0)
        return FALSE;
        
     return TRUE;
}
//----------------------------------------------------------------------------------------------
