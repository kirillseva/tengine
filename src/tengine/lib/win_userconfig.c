/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#if defined  WINDOWS_APP

#include <windows.h>
#include <io.h>
#include <direct.h>
#include <shlobj_core.h>

#include "system.h"
#include "userconfig.h"
#include "texts.h"

//-------------------------------------------------------------------------------------------

static char gLocalLowPath[MAX_PATH] = {0};
static BOOL gsUserConfigError = FALSE;
static const char gTengineFolderName[] = "tengine";
static const char gPathSlash[] = "\\";
static const char gTengineConfigFileName[] = "config.bin";
static const char gHeaderMark[] = "CB";

struct UserConfigHeader
{
    char a[2];
    u32 size;
};

//-------------------------------------------------------------------------------------------

BOOL UserConfig_FolderExists(const char* folderName)
{
    if (_access(folderName, 0) == -1)
    {
        return FALSE;
    }
    DWORD attr = GetFileAttributes((LPCSTR)folderName);
    if (!(attr & FILE_ATTRIBUTE_DIRECTORY))
    {
        return FALSE;
    }
    return TRUE;
}
//-------------------------------------------------------------------------------------------

static BOOL UserConfig_CreateFolders(const char* appName)
{
    char path1[MAX_PATH];
    char path2[MAX_PATH];
    char path3[MAX_PATH];
    char* p[3];
    s32 i;

    STD_StrCpy(path1, gLocalLowPath);
    STD_StrCat(path1, gPathSlash);
    
    STD_StrCpy(path2, path1);
    STD_StrCat(path2, gTengineFolderName);
    STD_StrCat(path2, gPathSlash);

    STD_StrCpy(path3, path2);
    STD_StrCat(path3, appName);
    STD_StrCat(path3, gPathSlash);

    i = 2;
    p[0] = path1;
    p[1] = path2;
    p[2] = path3;

    while (i >= 0)
    {
        if (UserConfig_FolderExists(p[i]))
        {
            break;
        }
        i--;
    }
    if (i < 0)
    {
        return FALSE;
    }
    for (; i < 2; i++)
    {
        if (CreateDirectoryA(p[i + 1], NULL) == 0)
        {
            return FALSE;
        }
    }
    return TRUE;
}
//-------------------------------------------------------------------------------------------

s32 InitUserConfig(const char *appName)
{
	PWSTR path_tmp;
    size_t converted;
    SDK_ASSERT(appName);
    path_tmp = NULL;
	if (SHGetKnownFolderPath(&FOLDERID_LocalAppDataLow, KF_FLAG_DEFAULT, NULL, &path_tmp) != S_OK)
	{
        if (path_tmp != NULL)
        {
            CoTaskMemFree(path_tmp);
        }
		gsUserConfigError = TRUE;
		return UCR_INIT_ERROR;
	}
#if defined __BORLANDC__
	wcstombs(gLocalLowPath, path_tmp, MAX_PATH);
#else
	wcstombs_s(&converted, gLocalLowPath, MAX_PATH, path_tmp, MAX_PATH);
#endif
	CoTaskMemFree(path_tmp);

    if (UserConfig_CreateFolders(appName))
    {
        STD_StrCat(gLocalLowPath, gPathSlash);
        STD_StrCat(gLocalLowPath, gTengineFolderName);
        STD_StrCat(gLocalLowPath, gPathSlash);
        STD_StrCat(gLocalLowPath, appName);
        STD_StrCat(gLocalLowPath, gPathSlash);
        STD_StrCat(gLocalLowPath, gTengineConfigFileName);
        return UCR_SUCCESS;
    }
    gsUserConfigError = TRUE;
    return UCR_INIT_ERROR;
}
//-------------------------------------------------------------------------------------------

s32 SaveUserConfig(const void* pdata, u32 size)
{
    FILE* f;
    size_t write_bytes;
    struct UserConfigHeader uch;
    if (gsUserConfigError)
    {
        return UCR_INIT_ERROR;
    }
    if (fopen_s(&f, gLocalLowPath, "wb") != 0)
    {
        return UCR_WRITE_ERROR;
    }
    if (f == NULL)
    {
        return UCR_WRITE_ERROR;
    }
    uch.a[0] = gHeaderMark[0];
    uch.a[1] = gHeaderMark[1];
    uch.size = size;
    fwrite(&uch, sizeof(struct UserConfigHeader), 1, f);
    write_bytes = fwrite(pdata, 1, size, f);
    if (size != (u32)write_bytes)
    {
        fclose(f);
        return UCR_WRITE_ERROR;
    }
    fclose(f);
    return UCR_SUCCESS;
}
//-------------------------------------------------------------------------------------------

s32 LoadUserConfig(void* pdata, u32 size)
{
    FILE* f;
    size_t read_bytes;
    struct UserConfigHeader uch;
    if (gsUserConfigError)
    {
        return UCR_INIT_ERROR;
    }
    if (fopen_s(&f, gLocalLowPath, "rb") != 0)
    {
        return UCR_CONFIG_NOT_EXIST;
    }
    if (f == NULL)
    {
        return UCR_READ_ERROR;
	}
#if defined __BORLANDC__
	fread(&uch, sizeof(struct UserConfigHeader), 1, f);
#else
	fread_s(&uch, sizeof(struct UserConfigHeader), sizeof(struct UserConfigHeader), 1, f);
#endif
    if (uch.a[0] != gHeaderMark[0] || uch.a[1] != gHeaderMark[1])
    {
        fclose(f);
        return UCR_WRONG_FILE_STRUCTURE;
    }
    if (uch.size != size)
    {
        fclose(f);
        return UCR_WRONG_DATA_SIZE;
	}
#if defined __BORLANDC__
	read_bytes = fread(pdata, 1, size, f);
#else
	read_bytes = fread_s(pdata, size, 1, size, f);
#endif
    if (size != (u32)read_bytes)
    {
        fclose(f);
        return UCR_READ_ERROR;
    }
    fclose(f);
    return UCR_SUCCESS;
}
//-------------------------------------------------------------------------------------------

#endif
