/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "pool_list.h"
#include "memory.h"

struct PoolListItem
{
	s32 mArrIdx;
	u8 *mpData;
	struct PoolListItem *mpNext;
	struct PoolListItem *mpPrev;
};

static const struct PoolListItem* _PoolList_DeleteItem(struct PoolList *self, struct PoolListItem *curr);

//---------------------------------------------------------------------------

void PoolList_Init(struct PoolList *self, u32 sizeofData, u32 maxPool)
{
	s32 i;
	SDK_NULL_ASSERT(self);
	SDK_ASSERT(sizeofData > 0);
	SDK_ASSERT(maxPool > 0);
	self->mppItemsPool = (struct PoolListItem **)MALLOC(sizeof(struct PoolListItem*) * maxPool, "PoolList_Init::mppItemsPool");
	self->mMaxPool = (s32)maxPool;
	self->mSizeOfData = (s32)sizeofData;
	for(i = 0; i < self->mMaxPool; i++)
	{
		self->mppItemsPool[i] = (struct PoolListItem *)MALLOC(sizeof(struct PoolListItem), "PoolList_Init::itemsPool[i]");
		self->mppItemsPool[i]->mpData = (u8 *)MALLOC(sizeofData, "PoolList_Init::itemsPool[i]->mpData");
	}
}
//---------------------------------------------------------------------------

void PoolList_Release(struct PoolList *self)
{
	s32 i = self->mMaxPool;
	while(i > 0)
	{
		i--;
		FREE(self->mppItemsPool[i]->mpData);
		FREE(self->mppItemsPool[i]);
	}
	FREE(self->mppItemsPool);
	MI_CpuClear8(self, sizeof(struct PoolList));
}
//---------------------------------------------------------------------------

const struct PoolListItem* PoolList_Add(struct PoolList *self, const void *data)
{
	SDK_ASSERT(self->mItemsCount < self->mMaxPool);
	self->mppItemsPool[self->mItemsCount]->mpPrev = self->mpItemLast;
	if(self->mpItemLast != NULL)
	{
		self->mpItemLast	->mpNext = self->mppItemsPool[self->mItemsCount];
	}
	MI_CpuCopy8(data, self->mppItemsPool[self->mItemsCount]->mpData, self->mSizeOfData);
	self->mpItemLast = self->mppItemsPool[self->mItemsCount];
	self->mpItemLast->mpNext = NULL;
	self->mpItemLast->mArrIdx = self->mItemsCount;
	if(self->mpItemFirst == NULL)
	{
		self->mpItemFirst = self->mpItemLast;
	}
	self->mItemsCount++;
	return self->mpItemLast;
}
//---------------------------------------------------------------------------

const struct PoolListItem* PoolList_DeleteByIndex(struct PoolList *self, s32 idx)
{
	if(idx >= 0 && idx < self->mItemsCount)
	{
		s32 ct;
		struct PoolListItem *curr;
		if(idx < self->mItemsCount / 2)
		{
			ct = 0;
			curr = self->mpItemFirst;
			while(curr != NULL && ct != idx)
			{
				curr = curr->mpNext;
				++ct;
			}
		}
		else
		{
			ct = self->mItemsCount - 1;
			curr = self->mpItemLast;
			while(curr != NULL && ct != idx)
			{
				curr = curr->mpPrev;
				--ct;
			}
		}
		return _PoolList_DeleteItem(self, curr);
	}
	return NULL;
}
//---------------------------------------------------------------------------

const struct PoolListItem* PoolList_Delete(struct PoolList *self, const struct PoolListItem* item)
{
	return _PoolList_DeleteItem(self, (struct PoolListItem *)item);
}
//---------------------------------------------------------------------------

const struct PoolListItem* _PoolList_DeleteItem(struct PoolList *self, struct PoolListItem *curr)
{
	if(curr != NULL)
	{
		s32 arrIdx = curr->mArrIdx;
		MI_CpuClear8(curr->mpData, self->mSizeOfData);
		if(self->mItemsCount > 1)
		{
			if(curr == self->mpItemFirst)
			{
				self->mpItemFirst = curr->mpNext;
			}
			if(curr->mpPrev != NULL)
			{
				curr->mpPrev->mpNext = curr->mpNext;
			}
			if(curr->mpNext != NULL)
			{
				curr->mpNext->mpPrev = curr->mpPrev;
			}
			if(curr == self->mpItemLast)
			{
				self->mpItemLast = curr->mpPrev;
			}
			--self->mItemsCount;
			if(curr != self->mppItemsPool[self->mItemsCount])
			{
				curr = self->mppItemsPool[arrIdx];
				*curr = *self->mppItemsPool[self->mItemsCount];
				curr->mArrIdx = arrIdx;
				if(curr->mpNext != NULL)
				{
					curr->mpNext->mpPrev = curr;
				}
				if(curr->mpPrev != NULL)
				{
					curr->mpPrev->mpNext = curr;
				}
				if(self->mppItemsPool[self->mItemsCount] == self->mpItemFirst)
				{
					self->mpItemFirst = curr;
				}
				if(self->mppItemsPool[self->mItemsCount] == self->mpItemLast)
				{
					self->mpItemLast = curr;
				}
				return curr;
			}
		}
		else
		{
			self->mpItemLast = self->mpItemFirst = NULL;
			self->mItemsCount = 0;
		}
	}
	return NULL;
}
//---------------------------------------------------------------------------

void PoolList_Clear(struct PoolList *self)
{
	self->mpItemLast = self->mpItemFirst = NULL;
	self->mItemsCount = 0;
}
//---------------------------------------------------------------------------

const void *PoolList_GetItemData(const struct PoolListItem* item)
{
	return item->mpData;
}
//---------------------------------------------------------------------------

s32 PoolList_GetItemsCount(struct PoolList *self)
{
	return self->mItemsCount;
}
//---------------------------------------------------------------------------
