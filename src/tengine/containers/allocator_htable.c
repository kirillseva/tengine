/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "allocator_htable.h"
#include "static_allocator.h"
#include "allocator_list.h"
#include "memory.h"

#define HTABLE_HASH_PRIME 31

//----------------------------------------------------------------------------------------------

u32 AllocatorHTable_CalculateHeapSize(u32 max_items)
{
	return AllocatorList_CalculateHeapSize(sizeof(struct HTableItem), max_items);
}
//----------------------------------------------------------------------------------------------

void AllocatorHTable_Init(struct AllocatorHTable* phtable, const u32 max_items, struct StaticAllocator* allocator)
{
	u32 idx;
	SDK_NULL_ASSERT(phtable);
	SDK_NULL_ASSERT(allocator);
	phtable->item_count = 0;
	phtable->max_items = max_items;
	phtable->htable = (struct AllocatorList**)MALLOC(max_items * sizeof(struct AllocatorList*), "AllocatorHTable_Init::htable");
	for (idx = 0; idx < max_items; idx++)
	{
		phtable->htable[idx] = (struct AllocatorList*)MALLOC(sizeof(struct AllocatorList), "AllocatorHTable_Init::htable[idx]");
		AllocatorList_Init(phtable->htable[idx], sizeof(struct HTableItem), allocator);
	}
}
//----------------------------------------------------------------------------------------------

void AllocatorHTable_Release(struct AllocatorHTable* phtable)
{
	u32 idx;
	SDK_NULL_ASSERT(phtable);
	idx = phtable->max_items;
	while (idx != 0)
	{
		idx--;
		FREE(phtable->htable[idx]);
		phtable->htable[idx] = NULL;
	}
	if (phtable->htable != NULL)
	{
		FREE(phtable->htable);
		phtable->htable = NULL;
	}
	phtable->max_items = 0;
	phtable->item_count = 0;
}
//----------------------------------------------------------------------------------------------

void AllocatorHTable_Clear(struct AllocatorHTable* phtable)
{
	u32 idx;
	SDK_NULL_ASSERT(phtable);
	for (idx = 0; idx < phtable->max_items; idx++)
	{
		AllocatorList_Clear(phtable->htable[idx]);
	}
	phtable->item_count = 0;
}
//----------------------------------------------------------------------------------------------

static u32 _AllocatorHTable_Hash(const struct AllocatorHTable* phtable, u32 key)
{
	char ch;
	u32 hashval = 0;
	ch = (key >> 24) & 0xff;
	hashval = ch + HTABLE_HASH_PRIME * hashval;
	ch = (key >> 16) & 0xff;
	hashval = ch + HTABLE_HASH_PRIME * hashval;
	ch = (key >> 8) & 0xff;
	hashval = ch + HTABLE_HASH_PRIME * hashval;
	ch = key & 0xff;
	hashval = ch + HTABLE_HASH_PRIME * hashval;
	return hashval % phtable->max_items;
}
//----------------------------------------------------------------------------------------------

static struct HTableItem* _AllocatorHTable_Find(const struct AllocatorHTable* phtable, struct AllocatorList** outlist, const u32 key)
{
	u32 hashval;
	struct ListItem* it;
	struct HTableItem* keyval_it;
	SDK_NULL_ASSERT(phtable);
	hashval = _AllocatorHTable_Hash(phtable, key);
	keyval_it = NULL;
	*outlist = phtable->htable[hashval /*& (phtable->max_items - 1)*/];
	it = AllocatorList_Begin(*outlist);
	while (it != AllocatorList_End(*outlist))
	{
		keyval_it = (struct HTableItem*)AllocatorList_Val(*outlist, it);
		if (keyval_it->key == key)
		{
			return keyval_it;
		}
		it = AllocatorList_Next(*outlist, it);
	}
	return NULL;
}
//----------------------------------------------------------------------------------------------

void AllocatorHTable_Push(struct AllocatorHTable* phtable, const struct HTableItem* keyvalue)
{
	struct AllocatorList* plist;
	struct HTableItem* keyval_it;
	SDK_NULL_ASSERT(phtable);
	SDK_NULL_ASSERT(keyvalue);
	keyval_it = _AllocatorHTable_Find(phtable, &plist, keyvalue->key);
	if (keyval_it != NULL)
	{
		SDK_NULL_ASSERT(keyval_it->key == keyvalue->key);
		keyval_it->value = keyvalue->value;
		return;
	}
	AllocatorList_PushBack(plist, keyvalue);
	phtable->item_count++;
}
//----------------------------------------------------------------------------------------------

const struct HTableItem* AllocatorHTable_Find(const struct AllocatorHTable* phtable, const u32 key)
{
	struct AllocatorList* plist;
	SDK_NULL_ASSERT(phtable);
	return _AllocatorHTable_Find(phtable, &plist, key);
}
//----------------------------------------------------------------------------------------------

u32 AllocatorHTable_Size(const struct AllocatorHTable* phtable)
{
	return phtable->item_count;
}
//----------------------------------------------------------------------------------------------
