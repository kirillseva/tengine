/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef ALLOCATOR_HTABLE_H
#define ALLOCATOR_HTABLE_H

#include "tengine.h"

#ifdef __cplusplus
extern "C" {
#endif

struct AllocatorList;
struct StaticAllocator;

struct HTableItem
{
	u32 key;
	const void* value;
};

struct AllocatorHTable
{
	u32 item_count;
	u32 max_items;
	struct AllocatorList** htable;
};

u32 AllocatorHTable_CalculateHeapSize(const u32 max_items);
void AllocatorHTable_Init(struct AllocatorHTable* phtable, const u32 max_items, struct StaticAllocator* allocator);
void AllocatorHTable_Release(struct AllocatorHTable* phtable);
void AllocatorHTable_Clear(struct AllocatorHTable* phtable);
void AllocatorHTable_Push(struct AllocatorHTable* phtable, const struct HTableItem* keyvalue);
const struct HTableItem* AllocatorHTable_Find(const struct AllocatorHTable* phtable, const u32 key);
u32 AllocatorHTable_Size(const struct AllocatorHTable* phtable);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
