/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef FILESYSTEM_H_
#define FILESYSTEM_H_

#include "platform.h"

#ifdef __cplusplus
extern "C" {
#endif

//----------------------------------------------------------------------------------------------

struct BMPImage;
struct BMPPalette;

//----------------------------------------------------------------------------------------------

BOOL IsFileSystemError(void);

u8* LoadFile(const char* pFname, u32* oFileSize);
u32 FileSize(const char* pFname);
void LoadFileToSpecificMemory(const char* pFname, u8* pPointer, u32 iSize);

//----------------------------------------------------------------------------------------------

void InitBMPImage(struct BMPImage *const img);
void DeleteBMPImage(struct BMPImage *const img);
void LoadBMPImage(const char* pFname, struct BMPImage** img, BOOL updateImage);
u8* LoadBMPPalette(struct BMPPalette** ppPalette, const char* pFname, u32* oFileSize);
#ifdef NITRO_SDK
u8* LoadNCER(struct NNSG2dCellDataBank** ppCellBank, const char* pFname);
u8* LoadNCGR(struct NNSG2dCharacterData** ppCharData, const char* pFname);
u8* LoadNCLR(struct NNSG2dPaletteData** ppPltData, const char* pFname);
u8* LoadNCBR(struct NNSG2dCharacterData** ppCharData, const char* pFname);
u8* LoadNCGRforBG(struct NNSG2dCharacterData** ppCharData, const char* pFname);
#endif

//----------------------------------------------------------------------------------------------

s32 _NBytesToInt(const u8* m, s32 N, s32 off);

#ifdef NITRO_SDK
void TransferPaletteToMainVRAM(const struct BMPPalette* pPalette);
void TransferPaletteToSubVRAM(const struct BMPPalette* pPalette);
void ChangePaletteColor(struct BMPPalette* pPalette, u8 iIndex, GXRgb iColor);
void ChangePaletteColorMainVRAM(u8 iIndex, GXRgb iColor);
void ChangePaletteColorSubVRAM(u8 iIndex, GXRgb iColor);
void GetPalette256MainVRAM(struct BMPPalette *opPalette);
void GetPalette256SubVRAM(struct BMPPalette *opPalette);
#endif

//----------------------------------------------------------------------------------------------

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif //FILESYSTEM_H_
