/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include <emscripten/emscripten.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "lib/platform_low.h"
#include "lib/tengine_low.h"
#include "lib/a_touchpad.h"
#include "lib/a_gamepad.h"
#include "lib/render.h"
#include "gamepad.h"
#include "texts.h"
#include "filesystem.h"
#include "gamefield.h"
#include <GLFW/glfw3.h>

static const char* RES_PATH = "data/";
//do not change values here, use your IDE debug command line instead
// for example -w1024 -h728
#define DEFAULT_CLIENT_SIZE_W 512
#define DEFAULT_CLIENT_SIZE_H 512

//-----------------------------------------------------------------------

static struct TEnginePlatformData *gspPlatformData = NULL;

static void _resize(struct GLFWwindow *wnd, int w, int h);
static void _mouseMove(struct GLFWwindow *wnd, double x, double y);
static void _mouseClick(struct GLFWwindow *wnd, int button, int action, int mods);
static void _KeyPressed(struct GLFWwindow *wnd, int key, int scancode, int action, int mods);
static void _stop(struct GLFWwindow *wnd);

//-----------------------------------------------------------------------

int main()
{
	s32 width = DEFAULT_CLIENT_SIZE_W;
	s32 height = DEFAULT_CLIENT_SIZE_H;
	
	gspPlatformData = Platform_GetPlatformData();
	gspPlatformData->renderDevice = FALSE;
	gspPlatformData->tengineInit = FALSE;
			
    if (glfwInit() == GL_TRUE)
	{
		tfgInitMemory();
	    glRender_Init();
		tfgSetWindowModeSize(&width, &height);
		
		glfwWindowHint(GLFW_SAMPLES, 8);
		gspPlatformData->glWnd = glfwCreateWindow(width, height, "", NULL, NULL);
		glfwGetWindowSize(gspPlatformData->glWnd, &width, &height);
		
		if(gspPlatformData->glWnd != NULL)
		{
			struct InitFileSystemData fileSystemData;
		
			glfwMakeContextCurrent(gspPlatformData->glWnd);

			glfwSetKeyCallback(gspPlatformData->glWnd, _KeyPressed);
			glfwSetWindowCloseCallback(gspPlatformData->glWnd, _stop);
			glfwSetWindowSizeCallback(gspPlatformData->glWnd, _resize);
			glfwSetCursorPosCallback(gspPlatformData->glWnd, _mouseMove);
			glfwSetMouseButtonCallback(gspPlatformData->glWnd, _mouseClick);
		
			fileSystemData.mpPath = RES_PATH;
			InitFileSystem(&fileSystemData);
			tfgInit();
			gspPlatformData->tengineInit = TRUE;

			tfgResize(width, height);
			glRender_Resize(width, height);
			OS_Printf("init tengine display with size: %d %d\n", width, height);
			gspPlatformData->renderDevice = TRUE;
			
			Platform_InitTick();
			emscripten_set_main_loop(Platform_Tick, 0, 1);
		}
		else
		{
			OS_Printf("glfwOpenWindow() failed\n");
		}
    
		if(gspPlatformData->tengineInit)
		{
			tfgRelease();
			ReleaseFileSystem();
		}
		glRender_Release();
		
		if(gspPlatformData->glWnd != NULL)
		{
			glfwSetKeyCallback(gspPlatformData->glWnd, NULL);
			glfwSetWindowCloseCallback(gspPlatformData->glWnd, NULL);
			glfwSetWindowSizeCallback(gspPlatformData->glWnd, NULL);
			glfwSetCursorPosCallback(gspPlatformData->glWnd, NULL);
			glfwSetMouseButtonCallback(gspPlatformData->glWnd, NULL);
			glfwDestroyWindow(gspPlatformData->glWnd);
		}
		glfwTerminate();
	}
	else
	{
		OS_Printf("glfwInit() failed\n");
	}
	
	gspPlatformData->tengineInit = FALSE;
    gspPlatformData->glWnd = NULL;
	return 0;
}
//-----------------------------------------------------------------------

void _stop(struct GLFWwindow *wnd)
{
	(void)wnd;
	if(gspPlatformData->tengineInit)
	{
		Platform_lostRenderDevice();
	}
}
//-----------------------------------------------------------------------

void _resize(struct GLFWwindow *wnd, int w, int h)
{
	(void)wnd;
	if(gspPlatformData->tengineInit)
	{
		tfgResize(w, h);
		glRender_Resize(w, h);
	}
}
//-----------------------------------------------------------------------

void _mouseClick(struct GLFWwindow *wnd, int button, int action, int mods)
{
	(void)wnd;
	(void)mods;
	if(button == GLFW_MOUSE_BUTTON_LEFT)
	{
		double x, y;
		glfwGetCursorPos(wnd, &x, &y);
		if(action == GLFW_PRESS)
		{
			onTouchPadDown(0, (s32)x, (s32)y);
		}
		else if (action == GLFW_RELEASE)
		{
			onTouchPadUp(0, (s32)x, (s32)y);
		}
	}
}
//-----------------------------------------------------------------------

void _mouseMove(struct GLFWwindow *wnd, double x, double y)
{
	(void)wnd;
	onTouchPadMove(0, (s32)x, (s32)y);
}
//-----------------------------------------------------------------------

void _KeyPressed(struct GLFWwindow *wnd, int key, int scancode, int action, int mods)
{
	s32 te_key = -1;
	(void)wnd;
	(void)mods;
	(void)scancode;
	switch(key)
	{
		case GLFW_KEY_ENTER:
		case GLFW_KEY_KP_ENTER:
			te_key = PAD_BUTTON_START;
		break;
		case GLFW_KEY_UP: 
			te_key = PAD_KEY_UP;
			break;
		case GLFW_KEY_DOWN: 
			te_key = PAD_KEY_DOWN;
			break;
		case GLFW_KEY_LEFT: 
			te_key = PAD_KEY_LEFT;
			break;
		case GLFW_KEY_RIGHT:
			te_key = PAD_KEY_RIGHT;
			break;
		case GLFW_KEY_LEFT_SHIFT:
		case GLFW_KEY_RIGHT_SHIFT:
			te_key = PAD_BUTTON_B;
			break;
		case GLFW_KEY_LEFT_CONTROL:
		case GLFW_KEY_RIGHT_CONTROL:
			te_key = PAD_BUTTON_A;
	}
	if(te_key != -1)
	{
		switch(action)
		{
			case GLFW_RELEASE:
				onKeyUp(te_key);
			break;
			case GLFW_PRESS:
				onKeyDown(te_key);
		}
	}
}
//-----------------------------------------------------------------------
